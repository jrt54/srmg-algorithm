---------------------------------------------------------
Petsc Release Version 3.7.6, unknown  Tue Oct 17 21:10:08 2017
./SRMGtree on a linux-gn, 1 proc. with options:
---------------------------------------------------------
************************************************************************************************************************
***             WIDEN YOUR WINDOW TO 120 CHARACTERS.  Use 'enscript -r -fCourier9' to print this document            ***
************************************************************************************************************************

---------------------------------------------- PETSc Performance Summary: ----------------------------------------------

./SRMGtree on a linux-gnu-c-debug named jrt54-Blade with 1 processor, by jrt54 Tue Oct 17 21:15:20 2017
Using Petsc Release Version 3.7.6, unknown 

                         Max       Max/Min        Avg      Total 
Time (sec):           3.117e+02      1.00000   3.117e+02
Objects:              1.395e+04      1.00000   1.395e+04
Flops:                1.118e+11      1.00000   1.118e+11  1.118e+11
Flops/sec:            3.587e+08      1.00000   3.587e+08  3.587e+08
Memory:               1.761e+07      1.00000              1.761e+07
MPI Messages:         0.000e+00      0.00000   0.000e+00  0.000e+00
MPI Message Lengths:  0.000e+00      0.00000   0.000e+00  0.000e+00
MPI Reductions:       0.000e+00      0.00000

Flop counting convention: 1 flop = 1 real number operation of type (multiply/divide/add/subtract)
                            e.g., VecAXPY() for real vectors of length N --> 2N flops
                            and VecAXPY() for complex vectors of length N --> 8N flops

Summary of Stages:   ----- Time ------  ----- Flops -----  --- Messages ---  -- Message Lengths --  -- Reductions --
                        Avg     %Total     Avg     %Total   counts   %Total     Avg         %Total   counts   %Total 
 0:      Main Stage: 3.1172e+02 100.0%  1.1180e+11 100.0%  0.000e+00   0.0%  0.000e+00        0.0%  0.000e+00   0.0% 

------------------------------------------------------------------------------------------------------------------------
See the 'Profiling' chapter of the users' manual for details on interpreting output.
Phase summary info:
   Count: number of times phase was executed
   Time and Flops: Max - maximum over all processors
                   Ratio - ratio of maximum to minimum over all processors
   Mess: number of messages sent
   Avg. len: average message length (bytes)
   Reduct: number of global reductions
   Global: entire computation
   Stage: stages of a computation. Set stages with PetscLogStagePush() and PetscLogStagePop().
      %T - percent time in this phase         %F - percent flops in this phase
      %M - percent messages in this phase     %L - percent message lengths in this phase
      %R - percent reductions in this phase
   Total Mflop/s: 10e-6 * (sum of flops over all processors)/(max time over all processors)
------------------------------------------------------------------------------------------------------------------------


      ##########################################################
      #                                                        #
      #                          WARNING!!!                    #
      #                                                        #
      #   This code was compiled with a debugging option,      #
      #   To get timing results run ./configure                #
      #   using --with-debugging=no, the performance will      #
      #   be generally two or three times faster.              #
      #                                                        #
      ##########################################################


Event                Count      Time (sec)     Flops                             --- Global ---  --- Stage ---   Total
                   Max Ratio  Max     Ratio   Max  Ratio  Mess   Avg len Reduct  %T %F %M %L %R  %T %F %M %L %R Mflop/s
------------------------------------------------------------------------------------------------------------------------

--- Event Stage 0: Main Stage

coarsesolve            1 1.0 3.0257e-01 1.0 1.07e+08 1.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0   354
KSPGMRESOrthog     10527 1.0 6.2972e-01 1.0 4.60e+08 1.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0   731
KSPSetUp             341 1.0 3.6742e-02 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
KSPSolve             341 1.0 1.8502e+02 1.0 1.12e+11 1.0 0.0e+00 0.0e+00 0.0e+00 59100  0  0  0  59100  0  0  0   604
MatMult            10866 1.0 1.5247e+01 1.0 1.15e+10 1.0 0.0e+00 0.0e+00 0.0e+00  5 10  0  0  0   5 10  0  0  0   756
MatSolve           11207 1.0 2.6159e+01 1.0 1.19e+10 1.0 0.0e+00 0.0e+00 0.0e+00  8 11  0  0  0   8 11  0  0  0   454
MatLUFactorSym       341 1.0 5.3668e-04 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatLUFactorNum       341 1.0 1.4145e+02 1.0 8.79e+10 1.0 0.0e+00 0.0e+00 0.0e+00 45 79  0  0  0  45 79  0  0  0   621
MatAssemblyBegin     342 1.0 6.5541e-04 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatAssemblyEnd       342 1.0 4.8208e-04 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
Chebpolystuff    10620048 1.0 1.5071e+01 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  5  0  0  0  0   5  0  0  0  0     0
PCSetUp              341 1.0 1.4150e+02 1.0 8.79e+10 1.0 0.0e+00 0.0e+00 0.0e+00 45 79  0  0  0  45 79  0  0  0   621
PCApply            11207 1.0 2.6215e+01 1.0 1.19e+10 1.0 0.0e+00 0.0e+00 0.0e+00  8 11  0  0  0   8 11  0  0  0   453
VecMDot            10527 1.0 2.3961e-01 1.0 2.30e+08 1.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0   960
VecNorm            11207 1.0 4.5474e-02 1.0 1.63e+07 1.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0   359
VecScale           11207 1.0 7.5466e-01 1.0 8.16e+06 1.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0    11
VecCopy             1021 1.0 4.1869e-03 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
VecSet             13600 1.0 3.4933e-02 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
VecAXPY             1019 1.0 4.0911e-02 1.0 1.48e+06 1.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0    36
VecMAXPY           11207 1.0 2.7373e-01 1.0 2.46e+08 1.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0   897
VecNormalize       11207 1.0 8.5982e-01 1.0 2.45e+07 1.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0    28
Getting LaplaceBox  248344 1.0 9.1746e+01 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00 29  0  0  0  0  29  0  0  0  0     0
SRMG                   1 1.0 3.1141e+02 1.0 1.12e+11 1.0 0.0e+00 0.0e+00 0.0e+00100100  0  0  0 100100  0  0  0   359
Recursive old LaplaceBox  247860 1.0 6.8993e+01 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00 22  0  0  0  0  22  0  0  0  0     0
FreeingPatches         1 1.0 1.7800e-03 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
------------------------------------------------------------------------------------------------------------------------

Memory usage is given in bytes:

Object Type          Creations   Destructions     Memory  Descendants' Mem.
Reports information only for process 0.

--- Event Stage 0: Main Stage

       Krylov Solver     1              1      5817440     0.
              Matrix   683            683   2899561496     0.
      Preconditioner     1              1          992     0.
              Vector 12579          12579     92508920     0.
           Index Set   682            682       529232     0.
              Viewer     1              0            0     0.
========================================================================================================================
Average time to get PetscTime(): 4.76837e-08
#PETSc Option Table entries:
-L 4
-N 24
-buffer .2.
-history logchebN_24_L_4_buffer_.2..txt
-log_view
#End of PETSc Option Table entries
Compiled without FORTRAN kernels
Compiled with full precision matrices (default)
sizeof(short) 2 sizeof(int) 4 sizeof(long) 8 sizeof(void*) 8 sizeof(PetscScalar) 8 sizeof(PetscInt) 4
Configure options: --download-mpich --download-f2cblaslapack=1 --download-fftw
-----------------------------------------
Libraries compiled on Fri Aug 25 22:20:03 2017 on jrt54-Blade 
Machine characteristics: Linux-4.10.0-32-generic-x86_64-with-Ubuntu-16.04-xenial
Using PETSc directory: /home/jrt54/petsc
Using PETSc arch: linux-gnu-c-debug
-----------------------------------------

Using C compiler: /home/jrt54/petsc/linux-gnu-c-debug/bin/mpicc    -Wall -Wwrite-strings -Wno-strict-aliasing -Wno-unknown-pragmas -fvisibility=hidden -g3  ${COPTFLAGS} ${CFLAGS}
-----------------------------------------

Using include paths: -I/home/jrt54/petsc/linux-gnu-c-debug/include -I/home/jrt54/petsc/include -I/home/jrt54/petsc/include -I/home/jrt54/petsc/linux-gnu-c-debug/include
-----------------------------------------

Using C linker: /home/jrt54/petsc/linux-gnu-c-debug/bin/mpicc
Using libraries: -Wl,-rpath,/home/jrt54/petsc/linux-gnu-c-debug/lib -L/home/jrt54/petsc/linux-gnu-c-debug/lib -lpetsc -Wl,-rpath,/home/jrt54/petsc/linux-gnu-c-debug/lib -L/home/jrt54/petsc/linux-gnu-c-debug/lib -Wl,-rpath,/usr/lib/gcc/x86_64-linux-gnu/5 -L/usr/lib/gcc/x86_64-linux-gnu/5 -Wl,-rpath,/usr/lib/x86_64-linux-gnu -L/usr/lib/x86_64-linux-gnu -Wl,-rpath,/lib/x86_64-linux-gnu -L/lib/x86_64-linux-gnu -lfftw3_mpi -lfftw3 -lf2clapack -lf2cblas -lm -lpthread -lm -lmpicxx -lstdc++ -lm -ldl -lmpi -lgcc_s -ldl
-----------------------------------------

---------------------------------------------------------
Finished at Tue Oct 17 21:15:20 2017
---------------------------------------------------------
