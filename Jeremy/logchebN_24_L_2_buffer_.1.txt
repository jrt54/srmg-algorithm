---------------------------------------------------------
Petsc Release Version 3.7.6, unknown  Tue Oct 17 20:54:08 2017
./SRMGtree on a linux-gn, 1 proc. with options:
---------------------------------------------------------
************************************************************************************************************************
***             WIDEN YOUR WINDOW TO 120 CHARACTERS.  Use 'enscript -r -fCourier9' to print this document            ***
************************************************************************************************************************

---------------------------------------------- PETSc Performance Summary: ----------------------------------------------

./SRMGtree on a linux-gnu-c-debug named jrt54-Blade with 1 processor, by jrt54 Tue Oct 17 20:54:19 2017
Using Petsc Release Version 3.7.6, unknown 

                         Max       Max/Min        Avg      Total 
Time (sec):           1.120e+01      1.00000   1.120e+01
Objects:              8.650e+02      1.00000   8.650e+02
Flops:                4.402e+09      1.00000   4.402e+09  4.402e+09
Flops/sec:            3.930e+08      1.00000   3.930e+08  3.930e+08
Memory:               1.174e+07      1.00000              1.174e+07
MPI Messages:         0.000e+00      0.00000   0.000e+00  0.000e+00
MPI Message Lengths:  0.000e+00      0.00000   0.000e+00  0.000e+00
MPI Reductions:       0.000e+00      0.00000

Flop counting convention: 1 flop = 1 real number operation of type (multiply/divide/add/subtract)
                            e.g., VecAXPY() for real vectors of length N --> 2N flops
                            and VecAXPY() for complex vectors of length N --> 8N flops

Summary of Stages:   ----- Time ------  ----- Flops -----  --- Messages ---  -- Message Lengths --  -- Reductions --
                        Avg     %Total     Avg     %Total   counts   %Total     Avg         %Total   counts   %Total 
 0:      Main Stage: 1.1201e+01 100.0%  4.4024e+09 100.0%  0.000e+00   0.0%  0.000e+00        0.0%  0.000e+00   0.0% 

------------------------------------------------------------------------------------------------------------------------
See the 'Profiling' chapter of the users' manual for details on interpreting output.
Phase summary info:
   Count: number of times phase was executed
   Time and Flops: Max - maximum over all processors
                   Ratio - ratio of maximum to minimum over all processors
   Mess: number of messages sent
   Avg. len: average message length (bytes)
   Reduct: number of global reductions
   Global: entire computation
   Stage: stages of a computation. Set stages with PetscLogStagePush() and PetscLogStagePop().
      %T - percent time in this phase         %F - percent flops in this phase
      %M - percent messages in this phase     %L - percent message lengths in this phase
      %R - percent reductions in this phase
   Total Mflop/s: 10e-6 * (sum of flops over all processors)/(max time over all processors)
------------------------------------------------------------------------------------------------------------------------


      ##########################################################
      #                                                        #
      #                          WARNING!!!                    #
      #                                                        #
      #   This code was compiled with a debugging option,      #
      #   To get timing results run ./configure                #
      #   using --with-debugging=no, the performance will      #
      #   be generally two or three times faster.              #
      #                                                        #
      ##########################################################


Event                Count      Time (sec)     Flops                             --- Global ---  --- Stage ---   Total
                   Max Ratio  Max     Ratio   Max  Ratio  Mess   Avg len Reduct  %T %F %M %L %R  %T %F %M %L %R Mflop/s
------------------------------------------------------------------------------------------------------------------------

--- Event Stage 0: Main Stage

coarsesolve            1 1.0 2.9872e-01 1.0 1.07e+08 1.0 0.0e+00 0.0e+00 0.0e+00  3  2  0  0  0   3  2  0  0  0   358
KSPGMRESOrthog       651 1.0 3.5108e-02 1.0 2.42e+07 1.0 0.0e+00 0.0e+00 0.0e+00  0  1  0  0  0   0  1  0  0  0   689
KSPSetUp              21 1.0 2.0666e-03 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
KSPSolve              21 1.0 7.2707e+00 1.0 4.40e+09 1.0 0.0e+00 0.0e+00 0.0e+00 65100  0  0  0  65100  0  0  0   606
MatMult              672 1.0 6.6462e-01 1.0 5.15e+08 1.0 0.0e+00 0.0e+00 0.0e+00  6 12  0  0  0   6 12  0  0  0   774
MatSolve             693 1.0 1.1420e+00 1.0 5.31e+08 1.0 0.0e+00 0.0e+00 0.0e+00 10 12  0  0  0  10 12  0  0  0   465
MatLUFactorSym        21 1.0 3.6001e-05 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatLUFactorNum        21 1.0 5.3701e+00 1.0 3.33e+09 1.0 0.0e+00 0.0e+00 0.0e+00 48 76  0  0  0  48 76  0  0  0   620
MatAssemblyBegin      22 1.0 3.8385e-05 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatAssemblyEnd        22 1.0 3.3617e-05 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
Chebpolystuff     305808 1.0 4.3114e-01 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  4  0  0  0  0   4  0  0  0  0     0
PCSetUp               21 1.0 5.3734e+00 1.0 3.33e+09 1.0 0.0e+00 0.0e+00 0.0e+00 48 76  0  0  0  48 76  0  0  0   620
PCApply              693 1.0 1.1450e+00 1.0 5.31e+08 1.0 0.0e+00 0.0e+00 0.0e+00 10 12  0  0  0  10 12  0  0  0   463
VecMDot              651 1.0 1.3407e-02 1.0 1.21e+07 1.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0   902
VecNorm              693 1.0 2.3720e-03 1.0 8.56e+05 1.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0   361
VecScale             693 1.0 1.4867e-02 1.0 4.28e+05 1.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0    29
VecCopy               63 1.0 2.4748e-04 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
VecSet               840 1.0 2.1627e-03 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
VecAXPY               63 1.0 1.0602e-03 1.0 7.79e+04 1.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0    73
VecMAXPY             693 1.0 1.5220e-02 1.0 1.29e+07 1.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0   848
VecNormalize         693 1.0 2.0830e-02 1.0 1.28e+06 1.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0    62
Getting LaplaceBox   12984 1.0 2.2381e+00 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00 20  0  0  0  0  20  0  0  0  0     0
SRMG                   1 1.0 1.0901e+01 1.0 4.30e+09 1.0 0.0e+00 0.0e+00 0.0e+00 97 98  0  0  0  97 98  0  0  0   394
Recursive old LaplaceBox   12500 1.0 1.1163e+00 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00 10  0  0  0  0  10  0  0  0  0     0
FreeingPatches         1 1.0 9.2745e-05 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
------------------------------------------------------------------------------------------------------------------------

Memory usage is given in bytes:

Object Type          Creations   Destructions     Memory  Descendants' Mem.
Reports information only for process 0.

--- Event Stage 0: Main Stage

       Krylov Solver     1              1       359520     0.
              Matrix    43             43    130794136     0.
      Preconditioner     1              1          992     0.
              Vector   777            777      5030520     0.
           Index Set    42             42        32592     0.
              Viewer     1              0            0     0.
========================================================================================================================
Average time to get PetscTime(): 4.76837e-08
#PETSc Option Table entries:
-L 2
-N 24
-buffer .1
-history logchebN_24_L_2_buffer_.1.txt
-log_view
#End of PETSc Option Table entries
Compiled without FORTRAN kernels
Compiled with full precision matrices (default)
sizeof(short) 2 sizeof(int) 4 sizeof(long) 8 sizeof(void*) 8 sizeof(PetscScalar) 8 sizeof(PetscInt) 4
Configure options: --download-mpich --download-f2cblaslapack=1 --download-fftw
-----------------------------------------
Libraries compiled on Fri Aug 25 22:20:03 2017 on jrt54-Blade 
Machine characteristics: Linux-4.10.0-32-generic-x86_64-with-Ubuntu-16.04-xenial
Using PETSc directory: /home/jrt54/petsc
Using PETSc arch: linux-gnu-c-debug
-----------------------------------------

Using C compiler: /home/jrt54/petsc/linux-gnu-c-debug/bin/mpicc    -Wall -Wwrite-strings -Wno-strict-aliasing -Wno-unknown-pragmas -fvisibility=hidden -g3  ${COPTFLAGS} ${CFLAGS}
-----------------------------------------

Using include paths: -I/home/jrt54/petsc/linux-gnu-c-debug/include -I/home/jrt54/petsc/include -I/home/jrt54/petsc/include -I/home/jrt54/petsc/linux-gnu-c-debug/include
-----------------------------------------

Using C linker: /home/jrt54/petsc/linux-gnu-c-debug/bin/mpicc
Using libraries: -Wl,-rpath,/home/jrt54/petsc/linux-gnu-c-debug/lib -L/home/jrt54/petsc/linux-gnu-c-debug/lib -lpetsc -Wl,-rpath,/home/jrt54/petsc/linux-gnu-c-debug/lib -L/home/jrt54/petsc/linux-gnu-c-debug/lib -Wl,-rpath,/usr/lib/gcc/x86_64-linux-gnu/5 -L/usr/lib/gcc/x86_64-linux-gnu/5 -Wl,-rpath,/usr/lib/x86_64-linux-gnu -L/usr/lib/x86_64-linux-gnu -Wl,-rpath,/lib/x86_64-linux-gnu -L/lib/x86_64-linux-gnu -lfftw3_mpi -lfftw3 -lf2clapack -lf2cblas -lm -lpthread -lm -lmpicxx -lstdc++ -lm -ldl -lmpi -lgcc_s -ldl
-----------------------------------------

---------------------------------------------------------
Finished at Tue Oct 17 20:54:19 2017
---------------------------------------------------------
