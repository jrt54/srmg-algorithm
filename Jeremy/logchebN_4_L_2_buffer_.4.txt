---------------------------------------------------------
Petsc Release Version 3.7.6, unknown  Tue Oct 17 20:41:53 2017
./SRMGtree on a linux-gn, 1 proc. with options:
---------------------------------------------------------
************************************************************************************************************************
***             WIDEN YOUR WINDOW TO 120 CHARACTERS.  Use 'enscript -r -fCourier9' to print this document            ***
************************************************************************************************************************

---------------------------------------------- PETSc Performance Summary: ----------------------------------------------

./SRMGtree on a linux-gnu-c-debug named jrt54-Blade with 1 processor, by jrt54 Tue Oct 17 20:41:53 2017
Using Petsc Release Version 3.7.6, unknown 

                         Max       Max/Min        Avg      Total 
Time (sec):           1.612e-01      1.00000   1.612e-01
Objects:              8.170e+02      1.00000   8.170e+02
Flops:                1.280e+06      1.00000   1.280e+06  1.280e+06
Flops/sec:            7.938e+06      1.00000   7.938e+06  7.938e+06
Memory:               2.139e+05      1.00000              2.139e+05
MPI Messages:         0.000e+00      0.00000   0.000e+00  0.000e+00
MPI Message Lengths:  0.000e+00      0.00000   0.000e+00  0.000e+00
MPI Reductions:       0.000e+00      0.00000

Flop counting convention: 1 flop = 1 real number operation of type (multiply/divide/add/subtract)
                            e.g., VecAXPY() for real vectors of length N --> 2N flops
                            and VecAXPY() for complex vectors of length N --> 8N flops

Summary of Stages:   ----- Time ------  ----- Flops -----  --- Messages ---  -- Message Lengths --  -- Reductions --
                        Avg     %Total     Avg     %Total   counts   %Total     Avg         %Total   counts   %Total 
 0:      Main Stage: 1.6119e-01 100.0%  1.2796e+06 100.0%  0.000e+00   0.0%  0.000e+00        0.0%  0.000e+00   0.0% 

------------------------------------------------------------------------------------------------------------------------
See the 'Profiling' chapter of the users' manual for details on interpreting output.
Phase summary info:
   Count: number of times phase was executed
   Time and Flops: Max - maximum over all processors
                   Ratio - ratio of maximum to minimum over all processors
   Mess: number of messages sent
   Avg. len: average message length (bytes)
   Reduct: number of global reductions
   Global: entire computation
   Stage: stages of a computation. Set stages with PetscLogStagePush() and PetscLogStagePop().
      %T - percent time in this phase         %F - percent flops in this phase
      %M - percent messages in this phase     %L - percent message lengths in this phase
      %R - percent reductions in this phase
   Total Mflop/s: 10e-6 * (sum of flops over all processors)/(max time over all processors)
------------------------------------------------------------------------------------------------------------------------


      ##########################################################
      #                                                        #
      #                          WARNING!!!                    #
      #                                                        #
      #   This code was compiled with a debugging option,      #
      #   To get timing results run ./configure                #
      #   using --with-debugging=no, the performance will      #
      #   be generally two or three times faster.              #
      #                                                        #
      ##########################################################


Event                Count      Time (sec)     Flops                             --- Global ---  --- Stage ---   Total
                   Max Ratio  Max     Ratio   Max  Ratio  Mess   Avg len Reduct  %T %F %M %L %R  %T %F %M %L %R Mflop/s
------------------------------------------------------------------------------------------------------------------------

--- Event Stage 0: Main Stage

coarsesolve            1 1.0 3.2098e-03 1.0 2.85e+02 1.0 0.0e+00 0.0e+00 0.0e+00  2  0  0  0  0   2  0  0  0  0     0
KSPGMRESOrthog       594 1.0 1.8434e-02 1.0 5.58e+05 1.0 0.0e+00 0.0e+00 0.0e+00 11 44  0  0  0  11 44  0  0  0    30
KSPSetUp              21 1.0 1.6794e-03 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  1  0  0  0  0   1  0  0  0  0     0
KSPSolve              21 1.0 7.9784e-02 1.0 1.28e+06 1.0 0.0e+00 0.0e+00 0.0e+00 49100  0  0  0  49100  0  0  0    16
MatMult              613 1.0 9.2330e-03 1.0 3.03e+05 1.0 0.0e+00 0.0e+00 0.0e+00  6 24  0  0  0   6 24  0  0  0    33
MatSolve             634 1.0 9.9883e-03 1.0 3.13e+05 1.0 0.0e+00 0.0e+00 0.0e+00  6 24  0  0  0   6 24  0  0  0    31
MatLUFactorSym        21 1.0 3.4809e-05 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatLUFactorNum        21 1.0 1.1692e-03 1.0 5.47e+04 1.0 0.0e+00 0.0e+00 0.0e+00  1  4  0  0  0   1  4  0  0  0    47
MatAssemblyBegin      22 1.0 3.7670e-05 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatAssemblyEnd        22 1.0 3.1948e-05 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
Chebpolystuff       7728 1.0 1.0044e-02 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  6  0  0  0  0   6  0  0  0  0     0
PCSetUp               21 1.0 3.1531e-03 1.0 5.47e+04 1.0 0.0e+00 0.0e+00 0.0e+00  2  4  0  0  0   2  4  0  0  0    17
PCApply              634 1.0 1.2344e-02 1.0 3.13e+05 1.0 0.0e+00 0.0e+00 0.0e+00  8 24  0  0  0   8 24  0  0  0    25
VecMDot              594 1.0 6.4001e-03 1.0 2.75e+05 1.0 0.0e+00 0.0e+00 0.0e+00  4 21  0  0  0   4 21  0  0  0    43
VecNorm              634 1.0 1.3103e-03 1.0 1.96e+04 1.0 0.0e+00 0.0e+00 0.0e+00  1  2  0  0  0   1  2  0  0  0    15
VecScale             634 1.0 8.4088e-03 1.0 1.01e+04 1.0 0.0e+00 0.0e+00 0.0e+00  5  1  0  0  0   5  1  0  0  0     1
VecCopy               61 1.0 1.8835e-04 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
VecSet               790 1.0 1.8265e-03 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  1  0  0  0  0   1  0  0  0  0     0
VecAXPY               59 1.0 8.5068e-04 1.0 1.86e+03 1.0 0.0e+00 0.0e+00 0.0e+00  1  0  0  0  0   1  0  0  0  0     2
VecMAXPY             634 1.0 6.9811e-03 1.0 3.03e+05 1.0 0.0e+00 0.0e+00 0.0e+00  4 24  0  0  0   4 24  0  0  0    43
VecNormalize         634 1.0 1.2707e-02 1.0 2.97e+04 1.0 0.0e+00 0.0e+00 0.0e+00  8  2  0  0  0   8  2  0  0  0     2
Getting LaplaceBox     324 1.0 5.2419e-02 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00 33  0  0  0  0  33  0  0  0  0     0
SRMG                   1 1.0 1.5751e-01 1.0 1.28e+06 1.0 0.0e+00 0.0e+00 0.0e+00 98100  0  0  0  98100  0  0  0     8
Recursive old LaplaceBox     320 1.0 2.6556e-02 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00 16  0  0  0  0  16  0  0  0  0     0
FreeingPatches         1 1.0 8.7738e-05 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
------------------------------------------------------------------------------------------------------------------------

Memory usage is given in bytes:

Object Type          Creations   Destructions     Memory  Descendants' Mem.
Reports information only for process 0.

--- Event Stage 0: Main Stage

       Krylov Solver     1              1       359520     0.
              Matrix    43             43       203656     0.
      Preconditioner     1              1          992     0.
              Vector   729            729      1206456     0.
           Index Set    42             42        32592     0.
              Viewer     1              0            0     0.
========================================================================================================================
Average time to get PetscTime(): 2.38419e-08
#PETSc Option Table entries:
-L 2
-N 4
-buffer .4
-history logchebN_4_L_2_buffer_.4.txt
-log_view
#End of PETSc Option Table entries
Compiled without FORTRAN kernels
Compiled with full precision matrices (default)
sizeof(short) 2 sizeof(int) 4 sizeof(long) 8 sizeof(void*) 8 sizeof(PetscScalar) 8 sizeof(PetscInt) 4
Configure options: --download-mpich --download-f2cblaslapack=1 --download-fftw
-----------------------------------------
Libraries compiled on Fri Aug 25 22:20:03 2017 on jrt54-Blade 
Machine characteristics: Linux-4.10.0-32-generic-x86_64-with-Ubuntu-16.04-xenial
Using PETSc directory: /home/jrt54/petsc
Using PETSc arch: linux-gnu-c-debug
-----------------------------------------

Using C compiler: /home/jrt54/petsc/linux-gnu-c-debug/bin/mpicc    -Wall -Wwrite-strings -Wno-strict-aliasing -Wno-unknown-pragmas -fvisibility=hidden -g3  ${COPTFLAGS} ${CFLAGS}
-----------------------------------------

Using include paths: -I/home/jrt54/petsc/linux-gnu-c-debug/include -I/home/jrt54/petsc/include -I/home/jrt54/petsc/include -I/home/jrt54/petsc/linux-gnu-c-debug/include
-----------------------------------------

Using C linker: /home/jrt54/petsc/linux-gnu-c-debug/bin/mpicc
Using libraries: -Wl,-rpath,/home/jrt54/petsc/linux-gnu-c-debug/lib -L/home/jrt54/petsc/linux-gnu-c-debug/lib -lpetsc -Wl,-rpath,/home/jrt54/petsc/linux-gnu-c-debug/lib -L/home/jrt54/petsc/linux-gnu-c-debug/lib -Wl,-rpath,/usr/lib/gcc/x86_64-linux-gnu/5 -L/usr/lib/gcc/x86_64-linux-gnu/5 -Wl,-rpath,/usr/lib/x86_64-linux-gnu -L/usr/lib/x86_64-linux-gnu -Wl,-rpath,/lib/x86_64-linux-gnu -L/lib/x86_64-linux-gnu -lfftw3_mpi -lfftw3 -lf2clapack -lf2cblas -lm -lpthread -lm -lmpicxx -lstdc++ -lm -ldl -lmpi -lgcc_s -ldl
-----------------------------------------

---------------------------------------------------------
Finished at Tue Oct 17 20:41:53 2017
---------------------------------------------------------
