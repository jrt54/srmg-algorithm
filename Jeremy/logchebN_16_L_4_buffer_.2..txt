---------------------------------------------------------
Petsc Release Version 3.7.6, unknown  Tue Oct 17 20:46:45 2017
./SRMGtree on a linux-gn, 1 proc. with options:
---------------------------------------------------------
************************************************************************************************************************
***             WIDEN YOUR WINDOW TO 120 CHARACTERS.  Use 'enscript -r -fCourier9' to print this document            ***
************************************************************************************************************************

---------------------------------------------- PETSc Performance Summary: ----------------------------------------------

./SRMGtree on a linux-gnu-c-debug named jrt54-Blade with 1 processor, by jrt54 Tue Oct 17 20:47:57 2017
Using Petsc Release Version 3.7.6, unknown 

                         Max       Max/Min        Avg      Total 
Time (sec):           7.222e+01      1.00000   7.222e+01
Objects:              1.388e+04      1.00000   1.388e+04
Flops:                1.252e+10      1.00000   1.252e+10  1.252e+10
Flops/sec:            1.734e+08      1.00000   1.734e+08  1.734e+08
Memory:               4.575e+06      1.00000              4.575e+06
MPI Messages:         0.000e+00      0.00000   0.000e+00  0.000e+00
MPI Message Lengths:  0.000e+00      0.00000   0.000e+00  0.000e+00
MPI Reductions:       0.000e+00      0.00000

Flop counting convention: 1 flop = 1 real number operation of type (multiply/divide/add/subtract)
                            e.g., VecAXPY() for real vectors of length N --> 2N flops
                            and VecAXPY() for complex vectors of length N --> 8N flops

Summary of Stages:   ----- Time ------  ----- Flops -----  --- Messages ---  -- Message Lengths --  -- Reductions --
                        Avg     %Total     Avg     %Total   counts   %Total     Avg         %Total   counts   %Total 
 0:      Main Stage: 7.2224e+01 100.0%  1.2520e+10 100.0%  0.000e+00   0.0%  0.000e+00        0.0%  0.000e+00   0.0% 

------------------------------------------------------------------------------------------------------------------------
See the 'Profiling' chapter of the users' manual for details on interpreting output.
Phase summary info:
   Count: number of times phase was executed
   Time and Flops: Max - maximum over all processors
                   Ratio - ratio of maximum to minimum over all processors
   Mess: number of messages sent
   Avg. len: average message length (bytes)
   Reduct: number of global reductions
   Global: entire computation
   Stage: stages of a computation. Set stages with PetscLogStagePush() and PetscLogStagePop().
      %T - percent time in this phase         %F - percent flops in this phase
      %M - percent messages in this phase     %L - percent message lengths in this phase
      %R - percent reductions in this phase
   Total Mflop/s: 10e-6 * (sum of flops over all processors)/(max time over all processors)
------------------------------------------------------------------------------------------------------------------------


      ##########################################################
      #                                                        #
      #                          WARNING!!!                    #
      #                                                        #
      #   This code was compiled with a debugging option,      #
      #   To get timing results run ./configure                #
      #   using --with-debugging=no, the performance will      #
      #   be generally two or three times faster.              #
      #                                                        #
      ##########################################################


Event                Count      Time (sec)     Flops                             --- Global ---  --- Stage ---   Total
                   Max Ratio  Max     Ratio   Max  Ratio  Mess   Avg len Reduct  %T %F %M %L %R  %T %F %M %L %R Mflop/s
------------------------------------------------------------------------------------------------------------------------

--- Event Stage 0: Main Stage

coarsesolve            1 1.0 5.3830e-02 1.0 1.04e+07 1.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0   193
KSPGMRESOrthog     10450 1.0 4.3962e-01 1.0 2.03e+08 1.0 0.0e+00 0.0e+00 0.0e+00  1  2  0  0  0   1  2  0  0  0   461
KSPSetUp             341 1.0 3.3846e-02 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
KSPSolve             341 1.0 2.3053e+01 1.0 1.25e+10 1.0 0.0e+00 0.0e+00 0.0e+00 32100  0  0  0  32100  0  0  0   543
MatMult            10786 1.0 3.2943e+00 1.0 2.26e+09 1.0 0.0e+00 0.0e+00 0.0e+00  5 18  0  0  0   5 18  0  0  0   685
MatSolve           11127 1.0 5.3364e+00 1.0 2.33e+09 1.0 0.0e+00 0.0e+00 0.0e+00  7 19  0  0  0   7 19  0  0  0   436
MatLUFactorSym       341 1.0 5.2333e-04 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatLUFactorNum       341 1.0 1.2916e+01 1.0 7.71e+09 1.0 0.0e+00 0.0e+00 0.0e+00 18 62  0  0  0  18 62  0  0  0   597
MatAssemblyBegin     342 1.0 6.2728e-04 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatAssemblyEnd       342 1.0 4.8709e-04 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
Chebpolystuff    4719792 1.0 6.5832e+00 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  9  0  0  0  0   9  0  0  0  0     0
PCSetUp              341 1.0 1.2959e+01 1.0 7.71e+09 1.0 0.0e+00 0.0e+00 0.0e+00 18 62  0  0  0  18 62  0  0  0   595
PCApply            11127 1.0 5.3818e+00 1.0 2.33e+09 1.0 0.0e+00 0.0e+00 0.0e+00  7 19  0  0  0   7 19  0  0  0   433
VecMDot            10450 1.0 1.6228e-01 1.0 1.01e+08 1.0 0.0e+00 0.0e+00 0.0e+00  0  1  0  0  0   0  1  0  0  0   624
VecNorm            11127 1.0 3.0620e-02 1.0 7.19e+06 1.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0   235
VecScale           11127 1.0 4.0784e-01 1.0 3.60e+06 1.0 0.0e+00 0.0e+00 0.0e+00  1  0  0  0  0   1  0  0  0  0     9
VecCopy             1018 1.0 3.7806e-03 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
VecSet             13530 1.0 3.3647e-02 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
VecAXPY             1013 1.0 3.7314e-02 1.0 6.56e+05 1.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0    18
VecMAXPY           11127 1.0 1.8471e-01 1.0 1.08e+08 1.0 0.0e+00 0.0e+00 0.0e+00  0  1  0  0  0   0  1  0  0  0   586
VecNormalize       11127 1.0 4.9278e-01 1.0 1.08e+07 1.0 0.0e+00 0.0e+00 0.0e+00  1  0  0  0  0   1  0  0  0  0    22
Getting LaplaceBox  110356 1.0 3.8857e+01 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00 54  0  0  0  0  54  0  0  0  0     0
SRMG                   1 1.0 7.2168e+01 1.0 1.25e+10 1.0 0.0e+00 0.0e+00 0.0e+00100100  0  0  0 100100  0  0  0   173
Recursive old LaplaceBox  110160 1.0 2.9340e+01 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00 41  0  0  0  0  41  0  0  0  0     0
FreeingPatches         1 1.0 1.5218e-03 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
------------------------------------------------------------------------------------------------------------------------

Memory usage is given in bytes:

Object Type          Creations   Destructions     Memory  Descendants' Mem.
Reports information only for process 0.

--- Event Stage 0: Main Stage

       Krylov Solver     1              1      5817440     0.
              Matrix   683            683    574339784     0.
      Preconditioner     1              1          992     0.
              Vector 12512          12512     51511552     0.
           Index Set   682            682       529232     0.
              Viewer     1              0            0     0.
========================================================================================================================
Average time to get PetscTime(): 2.38419e-08
#PETSc Option Table entries:
-L 4
-N 16
-buffer .2.
-history logchebN_16_L_4_buffer_.2..txt
-log_view
#End of PETSc Option Table entries
Compiled without FORTRAN kernels
Compiled with full precision matrices (default)
sizeof(short) 2 sizeof(int) 4 sizeof(long) 8 sizeof(void*) 8 sizeof(PetscScalar) 8 sizeof(PetscInt) 4
Configure options: --download-mpich --download-f2cblaslapack=1 --download-fftw
-----------------------------------------
Libraries compiled on Fri Aug 25 22:20:03 2017 on jrt54-Blade 
Machine characteristics: Linux-4.10.0-32-generic-x86_64-with-Ubuntu-16.04-xenial
Using PETSc directory: /home/jrt54/petsc
Using PETSc arch: linux-gnu-c-debug
-----------------------------------------

Using C compiler: /home/jrt54/petsc/linux-gnu-c-debug/bin/mpicc    -Wall -Wwrite-strings -Wno-strict-aliasing -Wno-unknown-pragmas -fvisibility=hidden -g3  ${COPTFLAGS} ${CFLAGS}
-----------------------------------------

Using include paths: -I/home/jrt54/petsc/linux-gnu-c-debug/include -I/home/jrt54/petsc/include -I/home/jrt54/petsc/include -I/home/jrt54/petsc/linux-gnu-c-debug/include
-----------------------------------------

Using C linker: /home/jrt54/petsc/linux-gnu-c-debug/bin/mpicc
Using libraries: -Wl,-rpath,/home/jrt54/petsc/linux-gnu-c-debug/lib -L/home/jrt54/petsc/linux-gnu-c-debug/lib -lpetsc -Wl,-rpath,/home/jrt54/petsc/linux-gnu-c-debug/lib -L/home/jrt54/petsc/linux-gnu-c-debug/lib -Wl,-rpath,/usr/lib/gcc/x86_64-linux-gnu/5 -L/usr/lib/gcc/x86_64-linux-gnu/5 -Wl,-rpath,/usr/lib/x86_64-linux-gnu -L/usr/lib/x86_64-linux-gnu -Wl,-rpath,/lib/x86_64-linux-gnu -L/lib/x86_64-linux-gnu -lfftw3_mpi -lfftw3 -lf2clapack -lf2cblas -lm -lpthread -lm -lmpicxx -lstdc++ -lm -ldl -lmpi -lgcc_s -ldl
-----------------------------------------

---------------------------------------------------------
Finished at Tue Oct 17 20:47:57 2017
---------------------------------------------------------
