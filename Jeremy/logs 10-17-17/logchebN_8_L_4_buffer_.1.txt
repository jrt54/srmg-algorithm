---------------------------------------------------------
Petsc Release Version 3.7.6, unknown  Mon Oct 16 06:57:41 2017
./SRMGtree on a linux-gn, 1 proc. with options:
---------------------------------------------------------
************************************************************************************************************************
***             WIDEN YOUR WINDOW TO 120 CHARACTERS.  Use 'enscript -r -fCourier9' to print this document            ***
************************************************************************************************************************

---------------------------------------------- PETSc Performance Summary: ----------------------------------------------

./SRMGtree on a linux-gnu-c-debug named jrt54-Blade with 1 processor, by jrt54 Mon Oct 16 06:58:01 2017
Using Petsc Release Version 3.7.6, unknown 

                         Max       Max/Min        Avg      Total 
Time (sec):           1.986e+01      1.00000   1.986e+01
Objects:              1.389e+04      1.00000   1.389e+04
Flops:                9.477e+08      1.00000   9.477e+08  9.477e+08
Flops/sec:            4.773e+07      1.00000   4.773e+07  4.773e+07
Memory:               1.469e+06      1.00000              1.469e+06
MPI Messages:         0.000e+00      0.00000   0.000e+00  0.000e+00
MPI Message Lengths:  0.000e+00      0.00000   0.000e+00  0.000e+00
MPI Reductions:       0.000e+00      0.00000

Flop counting convention: 1 flop = 1 real number operation of type (multiply/divide/add/subtract)
                            e.g., VecAXPY() for real vectors of length N --> 2N flops
                            and VecAXPY() for complex vectors of length N --> 8N flops

Summary of Stages:   ----- Time ------  ----- Flops -----  --- Messages ---  -- Message Lengths --  -- Reductions --
                        Avg     %Total     Avg     %Total   counts   %Total     Avg         %Total   counts   %Total 
 0:      Main Stage: 1.9857e+01 100.0%  9.4775e+08 100.0%  0.000e+00   0.0%  0.000e+00        0.0%  0.000e+00   0.0% 

------------------------------------------------------------------------------------------------------------------------
See the 'Profiling' chapter of the users' manual for details on interpreting output.
Phase summary info:
   Count: number of times phase was executed
   Time and Flops: Max - maximum over all processors
                   Ratio - ratio of maximum to minimum over all processors
   Mess: number of messages sent
   Avg. len: average message length (bytes)
   Reduct: number of global reductions
   Global: entire computation
   Stage: stages of a computation. Set stages with PetscLogStagePush() and PetscLogStagePop().
      %T - percent time in this phase         %F - percent flops in this phase
      %M - percent messages in this phase     %L - percent message lengths in this phase
      %R - percent reductions in this phase
   Total Mflop/s: 10e-6 * (sum of flops over all processors)/(max time over all processors)
------------------------------------------------------------------------------------------------------------------------


      ##########################################################
      #                                                        #
      #                          WARNING!!!                    #
      #                                                        #
      #   This code was compiled with a debugging option,      #
      #   To get timing results run ./configure                #
      #   using --with-debugging=no, the performance will      #
      #   be generally two or three times faster.              #
      #                                                        #
      ##########################################################


Event                Count      Time (sec)     Flops                             --- Global ---  --- Stage ---   Total
                   Max Ratio  Max     Ratio   Max  Ratio  Mess   Avg len Reduct  %T %F %M %L %R  %T %F %M %L %R Mflop/s
------------------------------------------------------------------------------------------------------------------------

--- Event Stage 0: Main Stage

coarsesolve            1 1.0 1.2058e-02 1.0 2.70e+05 1.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0    22
KSPGMRESOrthog     10462 1.0 3.7265e-01 1.0 6.88e+07 1.0 0.0e+00 0.0e+00 0.0e+00  2  7  0  0  0   2  7  0  0  0   185
KSPSetUp             341 1.0 3.3041e-02 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
KSPSolve             341 1.0 3.8261e+00 1.0 9.48e+08 1.0 0.0e+00 0.0e+00 0.0e+00 19100  0  0  0  19100  0  0  0   248
MatMult            10798 1.0 7.5867e-01 1.0 2.68e+08 1.0 0.0e+00 0.0e+00 0.0e+00  4 28  0  0  0   4 28  0  0  0   353
MatSolve           11139 1.0 1.0374e+00 1.0 2.77e+08 1.0 0.0e+00 0.0e+00 0.0e+00  5 29  0  0  0   5 29  0  0  0   267
MatLUFactorSym       341 1.0 6.3467e-04 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatLUFactorNum       341 1.0 6.3233e-01 1.0 3.28e+08 1.0 0.0e+00 0.0e+00 0.0e+00  3 35  0  0  0   3 35  0  0  0   519
MatAssemblyBegin     342 1.0 6.6185e-04 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatAssemblyEnd       342 1.0 5.4550e-04 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
Chebpolystuff    1641712 1.0 2.2240e+00 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00 11  0  0  0  0  11  0  0  0  0     0
PCSetUp              341 1.0 6.6997e-01 1.0 3.28e+08 1.0 0.0e+00 0.0e+00 0.0e+00  3 35  0  0  0   3 35  0  0  0   490
PCApply            11139 1.0 1.0812e+00 1.0 2.77e+08 1.0 0.0e+00 0.0e+00 0.0e+00  5 29  0  0  0   5 29  0  0  0   256
VecMDot            10462 1.0 1.3296e-01 1.0 3.43e+07 1.0 0.0e+00 0.0e+00 0.0e+00  1  4  0  0  0   1  4  0  0  0   258
VecNorm            11139 1.0 2.6705e-02 1.0 2.44e+06 1.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0    91
VecScale           11139 1.0 3.8989e-01 1.0 1.22e+06 1.0 0.0e+00 0.0e+00 0.0e+00  2  0  0  0  0   2  0  0  0  0     3
VecCopy             1018 1.0 3.7296e-03 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
VecSet             13540 1.0 3.2671e-02 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
VecAXPY             1013 1.0 3.6933e-02 1.0 2.22e+05 1.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     6
VecMAXPY           11139 1.0 1.4732e-01 1.0 3.68e+07 1.0 0.0e+00 0.0e+00 0.0e+00  1  4  0  0  0   1  4  0  0  0   250
VecNormalize       11139 1.0 4.7201e-01 1.0 3.66e+06 1.0 0.0e+00 0.0e+00 0.0e+00  2  0  0  0  0   2  0  0  0  0     8
Getting LaplaceBox   37416 1.0 1.3298e+01 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00 67  0  0  0  0  67  0  0  0  0     0
SRMG                   1 1.0 1.9843e+01 1.0 9.47e+08 1.0 0.0e+00 0.0e+00 0.0e+00100100  0  0  0 100100  0  0  0    48
Recursive old LaplaceBox   37380 1.0 1.0152e+01 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00 51  0  0  0  0  51  0  0  0  0     0
FreeingPatches         1 1.0 1.5042e-03 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
------------------------------------------------------------------------------------------------------------------------

Memory usage is given in bytes:

Object Type          Creations   Destructions     Memory  Descendants' Mem.
Reports information only for process 0.

--- Event Stage 0: Main Stage

       Krylov Solver     1              1      5817440     0.
              Matrix   683            683     69977944     0.
      Preconditioner     1              1          992     0.
              Vector 12522          12522     30132152     0.
           Index Set   682            682       529232     0.
              Viewer     1              0            0     0.
========================================================================================================================
Average time to get PetscTime(): 4.76837e-08
#PETSc Option Table entries:
-L 4
-N 8
-buffer .1
-history logchebN_8_L_4_buffer_.1.txt
-log_view
#End of PETSc Option Table entries
Compiled without FORTRAN kernels
Compiled with full precision matrices (default)
sizeof(short) 2 sizeof(int) 4 sizeof(long) 8 sizeof(void*) 8 sizeof(PetscScalar) 8 sizeof(PetscInt) 4
Configure options: --download-mpich --download-f2cblaslapack=1 --download-fftw
-----------------------------------------
Libraries compiled on Fri Aug 25 22:20:03 2017 on jrt54-Blade 
Machine characteristics: Linux-4.10.0-32-generic-x86_64-with-Ubuntu-16.04-xenial
Using PETSc directory: /home/jrt54/petsc
Using PETSc arch: linux-gnu-c-debug
-----------------------------------------

Using C compiler: /home/jrt54/petsc/linux-gnu-c-debug/bin/mpicc    -Wall -Wwrite-strings -Wno-strict-aliasing -Wno-unknown-pragmas -fvisibility=hidden -g3  ${COPTFLAGS} ${CFLAGS}
-----------------------------------------

Using include paths: -I/home/jrt54/petsc/linux-gnu-c-debug/include -I/home/jrt54/petsc/include -I/home/jrt54/petsc/include -I/home/jrt54/petsc/linux-gnu-c-debug/include
-----------------------------------------

Using C linker: /home/jrt54/petsc/linux-gnu-c-debug/bin/mpicc
Using libraries: -Wl,-rpath,/home/jrt54/petsc/linux-gnu-c-debug/lib -L/home/jrt54/petsc/linux-gnu-c-debug/lib -lpetsc -Wl,-rpath,/home/jrt54/petsc/linux-gnu-c-debug/lib -L/home/jrt54/petsc/linux-gnu-c-debug/lib -Wl,-rpath,/usr/lib/gcc/x86_64-linux-gnu/5 -L/usr/lib/gcc/x86_64-linux-gnu/5 -Wl,-rpath,/usr/lib/x86_64-linux-gnu -L/usr/lib/x86_64-linux-gnu -Wl,-rpath,/lib/x86_64-linux-gnu -L/lib/x86_64-linux-gnu -lfftw3_mpi -lfftw3 -lf2clapack -lf2cblas -lm -lpthread -lm -lmpicxx -lstdc++ -lm -ldl -lmpi -lgcc_s -ldl
-----------------------------------------

---------------------------------------------------------
Finished at Mon Oct 16 06:58:01 2017
---------------------------------------------------------
