---------------------------------------------------------
Petsc Release Version 3.7.6, unknown  Wed Oct 11 00:59:11 2017
./SRMGfourier on a linux-gn, 1 proc. with options:
---------------------------------------------------------
************************************************************************************************************************
***             WIDEN YOUR WINDOW TO 120 CHARACTERS.  Use 'enscript -r -fCourier9' to print this document            ***
************************************************************************************************************************

---------------------------------------------- PETSc Performance Summary: ----------------------------------------------

./SRMGfourier on a linux-gnu-c-debug named jrt54-Blade with 1 processor, by jrt54 Wed Oct 11 00:59:35 2017
Using Petsc Release Version 3.7.6, unknown 

                         Max       Max/Min        Avg      Total 
Time (sec):           2.443e+01      1.00000   2.443e+01
Objects:              7.826e+03      1.00000   7.826e+03
Flops:                5.024e+08      1.00000   5.024e+08  5.024e+08
Flops/sec:            2.057e+07      1.00000   2.057e+07  2.057e+07
Memory:               9.313e+06      1.00000              9.313e+06
MPI Messages:         0.000e+00      0.00000   0.000e+00  0.000e+00
MPI Message Lengths:  0.000e+00      0.00000   0.000e+00  0.000e+00
MPI Reductions:       0.000e+00      0.00000

Flop counting convention: 1 flop = 1 real number operation of type (multiply/divide/add/subtract)
                            e.g., VecAXPY() for real vectors of length N --> 2N flops
                            and VecAXPY() for complex vectors of length N --> 8N flops

Summary of Stages:   ----- Time ------  ----- Flops -----  --- Messages ---  -- Message Lengths --  -- Reductions --
                        Avg     %Total     Avg     %Total   counts   %Total     Avg         %Total   counts   %Total 
 0:      Main Stage: 2.4426e+01 100.0%  5.0236e+08 100.0%  0.000e+00   0.0%  0.000e+00        0.0%  0.000e+00   0.0% 

------------------------------------------------------------------------------------------------------------------------
See the 'Profiling' chapter of the users' manual for details on interpreting output.
Phase summary info:
   Count: number of times phase was executed
   Time and Flops: Max - maximum over all processors
                   Ratio - ratio of maximum to minimum over all processors
   Mess: number of messages sent
   Avg. len: average message length (bytes)
   Reduct: number of global reductions
   Global: entire computation
   Stage: stages of a computation. Set stages with PetscLogStagePush() and PetscLogStagePop().
      %T - percent time in this phase         %F - percent flops in this phase
      %M - percent messages in this phase     %L - percent message lengths in this phase
      %R - percent reductions in this phase
   Total Mflop/s: 10e-6 * (sum of flops over all processors)/(max time over all processors)
------------------------------------------------------------------------------------------------------------------------


      ##########################################################
      #                                                        #
      #                          WARNING!!!                    #
      #                                                        #
      #   This code was compiled with a debugging option,      #
      #   To get timing results run ./configure                #
      #   using --with-debugging=no, the performance will      #
      #   be generally two or three times faster.              #
      #                                                        #
      ##########################################################


Event                Count      Time (sec)     Flops                             --- Global ---  --- Stage ---   Total
                   Max Ratio  Max     Ratio   Max  Ratio  Mess   Avg len Reduct  %T %F %M %L %R  %T %F %M %L %R Mflop/s
------------------------------------------------------------------------------------------------------------------------

--- Event Stage 0: Main Stage

coarsesolve            1 1.0 1.4970e-02 1.0 2.70e+05 1.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0    18
KSPGMRESOrthog      2615 1.0 1.3126e-01 1.0 2.35e+07 1.0 0.0e+00 0.0e+00 0.0e+00  1  5  0  0  0   1  5  0  0  0   179
KSPSetUp              85 1.0 8.4031e-03 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
KSPSolve              85 1.0 8.9503e+00 1.0 5.02e+08 1.0 0.0e+00 0.0e+00 0.0e+00 37100  0  0  0  37100  0  0  0    56
MatMult             2699 1.0 2.5741e+00 1.0 1.28e+08 1.0 0.0e+00 0.0e+00 0.0e+00 11 25  0  0  0  11 25  0  0  0    50
MatSolve            2784 1.0 2.8322e+00 1.0 1.32e+08 1.0 0.0e+00 0.0e+00 0.0e+00 12 26  0  0  0  12 26  0  0  0    46
MatLUFactorSym        85 1.0 1.5926e-04 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatLUFactorNum        85 1.0 5.7281e-01 1.0 2.17e+08 1.0 0.0e+00 0.0e+00 0.0e+00  2 43  0  0  0   2 43  0  0  0   379
MatAssemblyBegin      86 1.0 1.6522e-04 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatAssemblyEnd        86 1.0 1.3900e-04 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
Chebpolystuff      84496 1.0 1.3836e-01 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  1  0  0  0  0   1  0  0  0  0     0
PCSetUp               85 1.0 5.8339e-01 1.0 2.17e+08 1.0 0.0e+00 0.0e+00 0.0e+00  2 43  0  0  0   2 43  0  0  0   373
PCApply             2784 1.0 2.8479e+00 1.0 1.32e+08 1.0 0.0e+00 0.0e+00 0.0e+00 12 26  0  0  0  12 26  0  0  0    46
VecDot             16064 1.0 1.3869e+01 1.0 3.58e+05 1.0 0.0e+00 0.0e+00 0.0e+00 57  0  0  0  0  57  0  0  0  0     0
VecMDot             2615 1.0 4.8548e-02 1.0 1.17e+07 1.0 0.0e+00 0.0e+00 0.0e+00  0  2  0  0  0   0  2  0  0  0   242
VecNorm             2784 1.0 1.0178e-02 1.0 8.32e+05 1.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0    82
VecScale            2784 1.0 2.4129e+00 1.0 4.17e+05 1.0 0.0e+00 0.0e+00 0.0e+00 10  0  0  0  0  10  0  0  0  0     0
VecCopy              254 1.0 1.2038e-03 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
VecSet              7736 1.0 2.3960e-02 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
VecAXPY              253 1.0 2.1956e-01 1.0 7.58e+04 1.0 0.0e+00 0.0e+00 0.0e+00  1  0  0  0  0   1  0  0  0  0     0
VecMAXPY            2784 1.0 4.7827e-02 1.0 1.25e+07 1.0 0.0e+00 0.0e+00 0.0e+00  0  2  0  0  0   0  2  0  0  0   262
VecNormalize        2784 1.0 2.4419e+00 1.0 1.25e+06 1.0 0.0e+00 0.0e+00 0.0e+00 10  0  0  0  0  10  0  0  0  0     1
SRMG                   1 1.0 2.4409e+01 1.0 5.02e+08 1.0 0.0e+00 0.0e+00 0.0e+00100100  0  0  0 100100  0  0  0    21
InverseFourier      4016 1.0 1.4238e+01 1.0 3.58e+05 1.0 0.0e+00 0.0e+00 0.0e+00 58  0  0  0  0  58  0  0  0  0     0
FreeingPatches         1 1.0 2.0955e-03 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
------------------------------------------------------------------------------------------------------------------------

Memory usage is given in bytes:

Object Type          Creations   Destructions     Memory  Descendants' Mem.
Reports information only for process 0.

--- Event Stage 0: Main Stage

       Krylov Solver     1              1      1451104     0.
              Matrix   171            171     32627976     0.
      Preconditioner     1              1          992     0.
              Vector  7482           3466      9080816     0.
           Index Set   170            170       131920     0.
              Viewer     1              0            0     0.
========================================================================================================================
Average time to get PetscTime(): 4.76837e-08
#PETSc Option Table entries:
-L 3
-N 8
-buffer .2
-history logfourierN_8_L_3_buffer_.2.txt
-log_view
#End of PETSc Option Table entries
Compiled without FORTRAN kernels
Compiled with full precision matrices (default)
sizeof(short) 2 sizeof(int) 4 sizeof(long) 8 sizeof(void*) 8 sizeof(PetscScalar) 8 sizeof(PetscInt) 4
Configure options: --download-mpich --download-f2cblaslapack=1 --download-fftw
-----------------------------------------
Libraries compiled on Fri Aug 25 22:20:03 2017 on jrt54-Blade 
Machine characteristics: Linux-4.10.0-32-generic-x86_64-with-Ubuntu-16.04-xenial
Using PETSc directory: /home/jrt54/petsc
Using PETSc arch: linux-gnu-c-debug
-----------------------------------------

Using C compiler: /home/jrt54/petsc/linux-gnu-c-debug/bin/mpicc    -Wall -Wwrite-strings -Wno-strict-aliasing -Wno-unknown-pragmas -fvisibility=hidden -g3  ${COPTFLAGS} ${CFLAGS}
-----------------------------------------

Using include paths: -I/home/jrt54/petsc/linux-gnu-c-debug/include -I/home/jrt54/petsc/include -I/home/jrt54/petsc/include -I/home/jrt54/petsc/linux-gnu-c-debug/include
-----------------------------------------

Using C linker: /home/jrt54/petsc/linux-gnu-c-debug/bin/mpicc
Using libraries: -Wl,-rpath,/home/jrt54/petsc/linux-gnu-c-debug/lib -L/home/jrt54/petsc/linux-gnu-c-debug/lib -lpetsc -Wl,-rpath,/home/jrt54/petsc/linux-gnu-c-debug/lib -L/home/jrt54/petsc/linux-gnu-c-debug/lib -Wl,-rpath,/usr/lib/gcc/x86_64-linux-gnu/5 -L/usr/lib/gcc/x86_64-linux-gnu/5 -Wl,-rpath,/usr/lib/x86_64-linux-gnu -L/usr/lib/x86_64-linux-gnu -Wl,-rpath,/lib/x86_64-linux-gnu -L/lib/x86_64-linux-gnu -lfftw3_mpi -lfftw3 -lf2clapack -lf2cblas -lm -lpthread -lm -lmpicxx -lstdc++ -lm -ldl -lmpi -lgcc_s -ldl
-----------------------------------------

---------------------------------------------------------
Finished at Wed Oct 11 00:59:35 2017
---------------------------------------------------------
