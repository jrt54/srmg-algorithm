---------------------------------------------------------
Petsc Release Version 3.7.6, unknown  Mon Oct 16 06:54:54 2017
./SRMGfourier on a linux-gn, 1 proc. with options:
---------------------------------------------------------
************************************************************************************************************************
***             WIDEN YOUR WINDOW TO 120 CHARACTERS.  Use 'enscript -r -fCourier9' to print this document            ***
************************************************************************************************************************

---------------------------------------------- PETSc Performance Summary: ----------------------------------------------

./SRMGfourier on a linux-gnu-c-debug named jrt54-Blade with 1 processor, by jrt54 Mon Oct 16 06:54:54 2017
Using Petsc Release Version 3.7.6, unknown 

                         Max       Max/Min        Avg      Total 
Time (sec):           2.208e-01      1.00000   2.208e-01
Objects:              1.219e+03      1.00000   1.219e+03
Flops:                1.169e+06      1.00000   1.169e+06  1.169e+06
Flops/sec:            5.297e+06      1.00000   5.297e+06  5.297e+06
Memory:               9.644e+05      1.00000              9.644e+05
MPI Messages:         0.000e+00      0.00000   0.000e+00  0.000e+00
MPI Message Lengths:  0.000e+00      0.00000   0.000e+00  0.000e+00
MPI Reductions:       0.000e+00      0.00000

Flop counting convention: 1 flop = 1 real number operation of type (multiply/divide/add/subtract)
                            e.g., VecAXPY() for real vectors of length N --> 2N flops
                            and VecAXPY() for complex vectors of length N --> 8N flops

Summary of Stages:   ----- Time ------  ----- Flops -----  --- Messages ---  -- Message Lengths --  -- Reductions --
                        Avg     %Total     Avg     %Total   counts   %Total     Avg         %Total   counts   %Total 
 0:      Main Stage: 2.2075e-01 100.0%  1.1694e+06 100.0%  0.000e+00   0.0%  0.000e+00        0.0%  0.000e+00   0.0% 

------------------------------------------------------------------------------------------------------------------------
See the 'Profiling' chapter of the users' manual for details on interpreting output.
Phase summary info:
   Count: number of times phase was executed
   Time and Flops: Max - maximum over all processors
                   Ratio - ratio of maximum to minimum over all processors
   Mess: number of messages sent
   Avg. len: average message length (bytes)
   Reduct: number of global reductions
   Global: entire computation
   Stage: stages of a computation. Set stages with PetscLogStagePush() and PetscLogStagePop().
      %T - percent time in this phase         %F - percent flops in this phase
      %M - percent messages in this phase     %L - percent message lengths in this phase
      %R - percent reductions in this phase
   Total Mflop/s: 10e-6 * (sum of flops over all processors)/(max time over all processors)
------------------------------------------------------------------------------------------------------------------------


      ##########################################################
      #                                                        #
      #                          WARNING!!!                    #
      #                                                        #
      #   This code was compiled with a debugging option,      #
      #   To get timing results run ./configure                #
      #   using --with-debugging=no, the performance will      #
      #   be generally two or three times faster.              #
      #                                                        #
      ##########################################################


Event                Count      Time (sec)     Flops                             --- Global ---  --- Stage ---   Total
                   Max Ratio  Max     Ratio   Max  Ratio  Mess   Avg len Reduct  %T %F %M %L %R  %T %F %M %L %R Mflop/s
------------------------------------------------------------------------------------------------------------------------

--- Event Stage 0: Main Stage

coarsesolve            1 1.0 3.4580e-03 1.0 2.85e+02 1.0 0.0e+00 0.0e+00 0.0e+00  2  0  0  0  0   2  0  0  0  0     0
KSPGMRESOrthog       593 1.0 1.7766e-02 1.0 5.19e+05 1.0 0.0e+00 0.0e+00 0.0e+00  8 44  0  0  0   8 44  0  0  0    29
KSPSetUp              21 1.0 1.6148e-03 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  1  0  0  0  0   1  0  0  0  0     0
KSPSolve              21 1.0 1.2460e-01 1.0 1.16e+06 1.0 0.0e+00 0.0e+00 0.0e+00 56 99  0  0  0  56 99  0  0  0     9
MatMult              612 1.0 2.3944e-02 1.0 2.69e+05 1.0 0.0e+00 0.0e+00 0.0e+00 11 23  0  0  0  11 23  0  0  0    11
MatSolve             633 1.0 2.5033e-02 1.0 2.78e+05 1.0 0.0e+00 0.0e+00 0.0e+00 11 24  0  0  0  11 24  0  0  0    11
MatLUFactorSym        21 1.0 2.9564e-05 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatLUFactorNum        21 1.0 1.2980e-03 1.0 4.57e+04 1.0 0.0e+00 0.0e+00 0.0e+00  1  4  0  0  0   1  4  0  0  0    35
MatAssemblyBegin      22 1.0 4.6015e-05 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatAssemblyEnd        22 1.0 2.8372e-05 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
Chebpolystuff       2456 1.0 3.1199e-03 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  1  0  0  0  0   1  0  0  0  0     0
PCSetUp               21 1.0 3.0134e-03 1.0 4.57e+04 1.0 0.0e+00 0.0e+00 0.0e+00  1  4  0  0  0   1  4  0  0  0    15
PCApply              633 1.0 2.7363e-02 1.0 2.78e+05 1.0 0.0e+00 0.0e+00 0.0e+00 12 24  0  0  0  12 24  0  0  0    10
VecDot              1312 1.0 4.6231e-02 1.0 1.15e+04 1.0 0.0e+00 0.0e+00 0.0e+00 21  1  0  0  0  21  1  0  0  0     0
VecMDot              593 1.0 6.1741e-03 1.0 2.55e+05 1.0 0.0e+00 0.0e+00 0.0e+00  3 22  0  0  0   3 22  0  0  0    41
VecNorm              633 1.0 1.3072e-03 1.0 1.81e+04 1.0 0.0e+00 0.0e+00 0.0e+00  1  2  0  0  0   1  2  0  0  0    14
VecScale             633 1.0 2.3806e-02 1.0 9.38e+03 1.0 0.0e+00 0.0e+00 0.0e+00 11  1  0  0  0  11  1  0  0  0     0
VecCopy               61 1.0 1.8144e-04 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
VecSet              1192 1.0 2.6422e-03 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  1  0  0  0  0   1  0  0  0  0     0
VecAXPY               59 1.0 2.2981e-03 1.0 1.72e+03 1.0 0.0e+00 0.0e+00 0.0e+00  1  0  0  0  0   1  0  0  0  0     1
VecMAXPY             633 1.0 6.6261e-03 1.0 2.81e+05 1.0 0.0e+00 0.0e+00 0.0e+00  3 24  0  0  0   3 24  0  0  0    42
VecNormalize         633 1.0 2.8043e-02 1.0 2.75e+04 1.0 0.0e+00 0.0e+00 0.0e+00 13  2  0  0  0  13  2  0  0  0     1
SRMG                   1 1.0 2.1651e-01 1.0 1.17e+06 1.0 0.0e+00 0.0e+00 0.0e+00 98100  0  0  0  98100  0  0  0     5
InverseFourier       328 1.0 5.9887e-02 1.0 1.15e+04 1.0 0.0e+00 0.0e+00 0.0e+00 27  1  0  0  0  27  1  0  0  0     0
FreeingPatches         1 1.0 4.1771e-04 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
------------------------------------------------------------------------------------------------------------------------

Memory usage is given in bytes:

Object Type          Creations   Destructions     Memory  Descendants' Mem.
Reports information only for process 0.

--- Event Stage 0: Main Stage

       Krylov Solver     1              1       359520     0.
              Matrix    43             43       192344     0.
      Preconditioner     1              1          992     0.
              Vector  1131            803      1315424     0.
           Index Set    42             42        32592     0.
              Viewer     1              0            0     0.
========================================================================================================================
Average time to get PetscTime(): 4.76837e-08
#PETSc Option Table entries:
-L 2
-N 4
-buffer .1
-history logfourierN_4_L_2_buffer_.1.txt
-log_view
#End of PETSc Option Table entries
Compiled without FORTRAN kernels
Compiled with full precision matrices (default)
sizeof(short) 2 sizeof(int) 4 sizeof(long) 8 sizeof(void*) 8 sizeof(PetscScalar) 8 sizeof(PetscInt) 4
Configure options: --download-mpich --download-f2cblaslapack=1 --download-fftw
-----------------------------------------
Libraries compiled on Fri Aug 25 22:20:03 2017 on jrt54-Blade 
Machine characteristics: Linux-4.10.0-32-generic-x86_64-with-Ubuntu-16.04-xenial
Using PETSc directory: /home/jrt54/petsc
Using PETSc arch: linux-gnu-c-debug
-----------------------------------------

Using C compiler: /home/jrt54/petsc/linux-gnu-c-debug/bin/mpicc    -Wall -Wwrite-strings -Wno-strict-aliasing -Wno-unknown-pragmas -fvisibility=hidden -g3  ${COPTFLAGS} ${CFLAGS}
-----------------------------------------

Using include paths: -I/home/jrt54/petsc/linux-gnu-c-debug/include -I/home/jrt54/petsc/include -I/home/jrt54/petsc/include -I/home/jrt54/petsc/linux-gnu-c-debug/include
-----------------------------------------

Using C linker: /home/jrt54/petsc/linux-gnu-c-debug/bin/mpicc
Using libraries: -Wl,-rpath,/home/jrt54/petsc/linux-gnu-c-debug/lib -L/home/jrt54/petsc/linux-gnu-c-debug/lib -lpetsc -Wl,-rpath,/home/jrt54/petsc/linux-gnu-c-debug/lib -L/home/jrt54/petsc/linux-gnu-c-debug/lib -Wl,-rpath,/usr/lib/gcc/x86_64-linux-gnu/5 -L/usr/lib/gcc/x86_64-linux-gnu/5 -Wl,-rpath,/usr/lib/x86_64-linux-gnu -L/usr/lib/x86_64-linux-gnu -Wl,-rpath,/lib/x86_64-linux-gnu -L/lib/x86_64-linux-gnu -lfftw3_mpi -lfftw3 -lf2clapack -lf2cblas -lm -lpthread -lm -lmpicxx -lstdc++ -lm -ldl -lmpi -lgcc_s -ldl
-----------------------------------------

---------------------------------------------------------
Finished at Mon Oct 16 06:54:54 2017
---------------------------------------------------------
