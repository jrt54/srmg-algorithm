---------------------------------------------------------
Petsc Release Version 3.7.6, unknown  Tue Oct 17 20:42:31 2017
./SRMGtree on a linux-gn, 1 proc. with options:
---------------------------------------------------------
************************************************************************************************************************
***             WIDEN YOUR WINDOW TO 120 CHARACTERS.  Use 'enscript -r -fCourier9' to print this document            ***
************************************************************************************************************************

---------------------------------------------- PETSc Performance Summary: ----------------------------------------------

./SRMGtree on a linux-gnu-c-debug named jrt54-Blade with 1 processor, by jrt54 Tue Oct 17 20:42:34 2017
Using Petsc Release Version 3.7.6, unknown 

                         Max       Max/Min        Avg      Total 
Time (sec):           3.384e+00      1.00000   3.384e+00
Objects:              3.432e+03      1.00000   3.432e+03
Flops:                1.783e+08      1.00000   1.783e+08  1.783e+08
Flops/sec:            5.269e+07      1.00000   5.269e+07  5.269e+07
Memory:               6.617e+05      1.00000              6.617e+05
MPI Messages:         0.000e+00      0.00000   0.000e+00  0.000e+00
MPI Message Lengths:  0.000e+00      0.00000   0.000e+00  0.000e+00
MPI Reductions:       0.000e+00      0.00000

Flop counting convention: 1 flop = 1 real number operation of type (multiply/divide/add/subtract)
                            e.g., VecAXPY() for real vectors of length N --> 2N flops
                            and VecAXPY() for complex vectors of length N --> 8N flops

Summary of Stages:   ----- Time ------  ----- Flops -----  --- Messages ---  -- Message Lengths --  -- Reductions --
                        Avg     %Total     Avg     %Total   counts   %Total     Avg         %Total   counts   %Total 
 0:      Main Stage: 3.3842e+00 100.0%  1.7833e+08 100.0%  0.000e+00   0.0%  0.000e+00        0.0%  0.000e+00   0.0% 

------------------------------------------------------------------------------------------------------------------------
See the 'Profiling' chapter of the users' manual for details on interpreting output.
Phase summary info:
   Count: number of times phase was executed
   Time and Flops: Max - maximum over all processors
                   Ratio - ratio of maximum to minimum over all processors
   Mess: number of messages sent
   Avg. len: average message length (bytes)
   Reduct: number of global reductions
   Global: entire computation
   Stage: stages of a computation. Set stages with PetscLogStagePush() and PetscLogStagePop().
      %T - percent time in this phase         %F - percent flops in this phase
      %M - percent messages in this phase     %L - percent message lengths in this phase
      %R - percent reductions in this phase
   Total Mflop/s: 10e-6 * (sum of flops over all processors)/(max time over all processors)
------------------------------------------------------------------------------------------------------------------------


      ##########################################################
      #                                                        #
      #                          WARNING!!!                    #
      #                                                        #
      #   This code was compiled with a debugging option,      #
      #   To get timing results run ./configure                #
      #   using --with-debugging=no, the performance will      #
      #   be generally two or three times faster.              #
      #                                                        #
      ##########################################################


Event                Count      Time (sec)     Flops                             --- Global ---  --- Stage ---   Total
                   Max Ratio  Max     Ratio   Max  Ratio  Mess   Avg len Reduct  %T %F %M %L %R  %T %F %M %L %R Mflop/s
------------------------------------------------------------------------------------------------------------------------

--- Event Stage 0: Main Stage

coarsesolve            1 1.0 1.0843e-02 1.0 2.70e+05 1.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0    25
KSPGMRESOrthog      2562 1.0 8.6684e-02 1.0 1.52e+07 1.0 0.0e+00 0.0e+00 0.0e+00  3  9  0  0  0   3  9  0  0  0   175
KSPSetUp              85 1.0 7.0982e-03 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
KSPSolve              85 1.0 6.7278e-01 1.0 1.78e+08 1.0 0.0e+00 0.0e+00 0.0e+00 20100  0  0  0  20100  0  0  0   265
MatMult             2643 1.0 1.1907e-01 1.0 5.20e+07 1.0 0.0e+00 0.0e+00 0.0e+00  4 29  0  0  0   4 29  0  0  0   437
MatSolve            2728 1.0 1.6796e-01 1.0 5.37e+07 1.0 0.0e+00 0.0e+00 0.0e+00  5 30  0  0  0   5 30  0  0  0   320
MatLUFactorSym        85 1.0 1.4353e-04 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatLUFactorNum        85 1.0 1.0686e-01 1.0 5.60e+07 1.0 0.0e+00 0.0e+00 0.0e+00  3 31  0  0  0   3 31  0  0  0   524
MatAssemblyBegin      86 1.0 1.4329e-04 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatAssemblyEnd        86 1.0 1.3852e-04 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
Chebpolystuff     278832 1.0 3.7315e-01 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00 11  0  0  0  0  11  0  0  0  0     0
PCSetUp               85 1.0 1.1583e-01 1.0 5.60e+07 1.0 0.0e+00 0.0e+00 0.0e+00  3 31  0  0  0   3 31  0  0  0   484
PCApply             2728 1.0 1.7816e-01 1.0 5.37e+07 1.0 0.0e+00 0.0e+00 0.0e+00  5 30  0  0  0   5 30  0  0  0   301
VecMDot             2562 1.0 3.0631e-02 1.0 7.57e+06 1.0 0.0e+00 0.0e+00 0.0e+00  1  4  0  0  0   1  4  0  0  0   247
VecNorm             2728 1.0 6.1195e-03 1.0 5.39e+05 1.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0    88
VecScale            2728 1.0 4.7520e-02 1.0 2.71e+05 1.0 0.0e+00 0.0e+00 0.0e+00  1  0  0  0  0   1  0  0  0  0     6
VecCopy              251 1.0 8.3065e-04 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
VecSet              3339 1.0 7.7643e-03 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
VecAXPY              247 1.0 4.7817e-03 1.0 4.90e+04 1.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0    10
VecMAXPY            2728 1.0 3.4594e-02 1.0 8.12e+06 1.0 0.0e+00 0.0e+00 0.0e+00  1  5  0  0  0   1  5  0  0  0   235
VecNormalize        2728 1.0 6.6854e-02 1.0 8.09e+05 1.0 0.0e+00 0.0e+00 0.0e+00  2  0  0  0  0   2  0  0  0  0    12
Getting LaplaceBox    8436 1.0 2.1146e+00 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00 62  0  0  0  0  62  0  0  0  0     0
SRMG                   1 1.0 3.3727e+00 1.0 1.78e+08 1.0 0.0e+00 0.0e+00 0.0e+00100100  0  0  0 100100  0  0  0    53
Recursive old LaplaceBox    8400 1.0 1.4185e+00 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00 42  0  0  0  0  42  0  0  0  0     0
FreeingPatches         1 1.0 3.5667e-04 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
------------------------------------------------------------------------------------------------------------------------

Memory usage is given in bytes:

Object Type          Creations   Destructions     Memory  Descendants' Mem.
Reports information only for process 0.

--- Event Stage 0: Main Stage

       Krylov Solver     1              1      1451104     0.
              Matrix   171            171     13982280     0.
      Preconditioner     1              1          992     0.
              Vector  3088           3088      7169920     0.
           Index Set   170            170       131920     0.
              Viewer     1              0            0     0.
========================================================================================================================
Average time to get PetscTime(): 2.38419e-08
#PETSc Option Table entries:
-L 3
-N 8
-buffer .5
-history logchebN_8_L_3_buffer_.5.txt
-log_view
#End of PETSc Option Table entries
Compiled without FORTRAN kernels
Compiled with full precision matrices (default)
sizeof(short) 2 sizeof(int) 4 sizeof(long) 8 sizeof(void*) 8 sizeof(PetscScalar) 8 sizeof(PetscInt) 4
Configure options: --download-mpich --download-f2cblaslapack=1 --download-fftw
-----------------------------------------
Libraries compiled on Fri Aug 25 22:20:03 2017 on jrt54-Blade 
Machine characteristics: Linux-4.10.0-32-generic-x86_64-with-Ubuntu-16.04-xenial
Using PETSc directory: /home/jrt54/petsc
Using PETSc arch: linux-gnu-c-debug
-----------------------------------------

Using C compiler: /home/jrt54/petsc/linux-gnu-c-debug/bin/mpicc    -Wall -Wwrite-strings -Wno-strict-aliasing -Wno-unknown-pragmas -fvisibility=hidden -g3  ${COPTFLAGS} ${CFLAGS}
-----------------------------------------

Using include paths: -I/home/jrt54/petsc/linux-gnu-c-debug/include -I/home/jrt54/petsc/include -I/home/jrt54/petsc/include -I/home/jrt54/petsc/linux-gnu-c-debug/include
-----------------------------------------

Using C linker: /home/jrt54/petsc/linux-gnu-c-debug/bin/mpicc
Using libraries: -Wl,-rpath,/home/jrt54/petsc/linux-gnu-c-debug/lib -L/home/jrt54/petsc/linux-gnu-c-debug/lib -lpetsc -Wl,-rpath,/home/jrt54/petsc/linux-gnu-c-debug/lib -L/home/jrt54/petsc/linux-gnu-c-debug/lib -Wl,-rpath,/usr/lib/gcc/x86_64-linux-gnu/5 -L/usr/lib/gcc/x86_64-linux-gnu/5 -Wl,-rpath,/usr/lib/x86_64-linux-gnu -L/usr/lib/x86_64-linux-gnu -Wl,-rpath,/lib/x86_64-linux-gnu -L/lib/x86_64-linux-gnu -lfftw3_mpi -lfftw3 -lf2clapack -lf2cblas -lm -lpthread -lm -lmpicxx -lstdc++ -lm -ldl -lmpi -lgcc_s -ldl
-----------------------------------------

---------------------------------------------------------
Finished at Tue Oct 17 20:42:34 2017
---------------------------------------------------------
