#include "SRMG.h"
#include <petscsys.h>
#include <petscdm.h>
#include <petscdmda.h>
#include <petscsnes.h>
#include <petscmatlab.h>
static char help[] = "Solving Laplace via Chebyshev";
#include <petsctime.h>
#include <petscmat.h>
#include <stdio.h>
#include <petscdm.h>
#include <petscdmda.h>
#include <petscdraw.h>
#include <time.h>

//float a = 15.0;
float a = 5.0;
PetscScalar zero(PetscScalar x, PetscScalar y)
{
  return 0;
}


//beginning of test1
//BC at x = -1
/*
PetscScalar test(PetscScalar x, PetscScalar y)
{
  return PetscSinhReal(a*PETSC_PI*(x-1))*PetscSinReal(a*PETSC_PI*(y-1))/PetscSinhReal(a*PETSC_PI*-2);
//  return 20;
}

PetscScalar northboundary(PetscScalar x)
{

  PetscScalar y = 1;
  //return (2*y*y-2)*(2*x*x-2);
  return PetscSinhReal(a*PETSC_PI*(x-1))*PetscSinReal(a*PETSC_PI*(y-1))/PetscSinhReal(a*PETSC_PI*-2);
}

PetscScalar southboundary(PetscScalar x)
{

  PetscScalar y = -1;
  //return (2*x*x-2)*(2*y*y-2);
    return PetscSinhReal(a*PETSC_PI*(x-1))*PetscSinReal(a*PETSC_PI*(y-1))/PetscSinhReal(a*PETSC_PI*-2);
}

PetscScalar eastboundary(PetscScalar y)
{
  PetscScalar x = 1;
  //return (2*x*x-2)*(2*y*y-2); 
  return PetscSinhReal(a*PETSC_PI*(x-1))*PetscSinReal(a*PETSC_PI*(y-1))/PetscSinhReal(a*PETSC_PI*-2);
}

PetscScalar westboundary(PetscScalar y)
{
  PetscScalar x = -1;
  return PetscSinhReal(a*PETSC_PI*(x-1))*PetscSinReal(a*PETSC_PI*(y-1))/PetscSinhReal(a*PETSC_PI*-2);
}


PetscScalar westboundarysecondder(PetscScalar y)
{
  PetscScalar x = -1;
  return -a*a*PETSC_PI*PETSC_PI*PetscSinhReal(a*PETSC_PI*(x-1))*PetscSinReal(a*PETSC_PI*(y-1))/PetscSinhReal(a*PETSC_PI*-2);
}

PetscScalar northboundarysecondder(PetscScalar x)
{
  PetscScalar y = 1;
  return a*a*PETSC_PI*PETSC_PI*PetscSinhReal(a*PETSC_PI*(x-1))*PetscSinReal(a*PETSC_PI*(y-1))/PetscSinhReal(a*PETSC_PI*-2);
}

PetscScalar southboundarysecondder(PetscScalar x)
{
  PetscScalar y = -1;
  return a*a*PETSC_PI*PETSC_PI*PetscSinhReal(a*PETSC_PI*(x-1))*PetscSinReal(a*PETSC_PI*(y-1))/PetscSinhReal(a*PETSC_PI*-2);
}

PetscScalar eastboundarysecondder(PetscScalar y)
{
  PetscScalar x = 1;
  return -a*a*PETSC_PI*PETSC_PI*PetscSinhReal(a*PETSC_PI*(x-1))*PetscSinReal(a*PETSC_PI*(y-1))/PetscSinhReal(a*PETSC_PI*-2);

  //return 2;
}

PetscScalar forcing(PetscScalar x, PetscScalar y)
{
  return 0;
}
*/
//end of test1


//beginning of test2
/*
PetscScalar test(PetscScalar x, PetscScalar y)
 {
   return (x-1)*PetscSinReal(a*PETSC_PI*y);
 }


 PetscScalar northboundary(PetscScalar x)
 {
   PetscScalar y = 1;
   return (x-1)*PetscSinReal(a*PETSC_PI*y);
 }

 PetscScalar southboundary(PetscScalar x)
 {
   PetscScalar y = -1;
   return (x-1)*PetscSinReal(a*PETSC_PI*y);
 }

 PetscScalar eastboundary(PetscScalar y)
 {
   PetscScalar x = 1;
   return (x-1)*PetscSinReal(a*PETSC_PI*y);
 }

 PetscScalar westboundary(PetscScalar y)
 {
   PetscScalar x = -1;
   return (x-1)*PetscSinReal(a*PETSC_PI*y);
 }


 PetscScalar westboundarysecondder(PetscScalar y)
 {
   PetscScalar x = -1;

   return (x-1)*a*a*-PETSC_PI*PETSC_PI*PetscSinReal(a*PETSC_PI*y);
 }

 PetscScalar northboundarysecondder(PetscScalar x)
 {
   PetscScalar y = 1;
   return 0;
   //return 2;
 }

 PetscScalar southboundarysecondder(PetscScalar x)
 {

   PetscScalar y = -1;
   return 0;
   //return 2; 
 }

 PetscScalar eastboundarysecondder(PetscScalar y)
 {
   PetscScalar x = 1;
   return (x-1)*a*a*-PETSC_PI*PETSC_PI*PetscSinReal(a*PETSC_PI*y);

   //return 2;
 }

 PetscScalar forcing(PetscScalar x, PetscScalar y)
 {

   //for u = (x+1)*sin(pi*a*y)
   //dxx = 0
   //dyy = -pi*pi*a*a*sin(pi*a*y)

   return (x-1)*a*a*-PETSC_PI*PETSC_PI*PetscSinReal(a*PETSC_PI*y);
 }
*/
//end of test2

//beginning of test3
PetscScalar test(PetscScalar x, PetscScalar y)
{
  return PetscSinReal(a*PETSC_PI*x)*PetscSinReal(a*PETSC_PI*y);
}


PetscScalar northboundary(PetscScalar x)
{
  PetscScalar y = 1;
  return PetscSinReal(a*PETSC_PI*x)*PetscSinReal(a*PETSC_PI*y);
}

PetscScalar southboundary(PetscScalar x)
{
  PetscScalar y = -1;
  return PetscSinReal(a*PETSC_PI*x)*PetscSinReal(a*PETSC_PI*y);
}

PetscScalar eastboundary(PetscScalar y)
{
  PetscScalar x = 1;
  return PetscSinReal(a*PETSC_PI*x)*PetscSinReal(a*PETSC_PI*y);
}

PetscScalar westboundary(PetscScalar y)
{
  PetscScalar x = -1;
  return PetscSinReal(a*PETSC_PI*x)*PetscSinReal(a*PETSC_PI*y);
}


PetscScalar westboundarysecondder(PetscScalar y)
{
  PetscScalar x = -1;
  return 0;
}

PetscScalar northboundarysecondder(PetscScalar x)
{
  PetscScalar y = 1;
  return 0;
}

PetscScalar southboundarysecondder(PetscScalar x)
{

  PetscScalar y = -1;
  return 0;
}

PetscScalar eastboundarysecondder(PetscScalar y)
{
  PetscScalar x = 1;
  return 0; 
}

PetscScalar forcing(PetscScalar x, PetscScalar y)
{

  return a*a*-PETSC_PI*PETSC_PI*(PetscSinReal(a*PETSC_PI*x)*PetscSinReal(a*PETSC_PI*y)+PetscSinReal(a*PETSC_PI*y)*PetscSinReal(a*PETSC_PI*x));
}
//end of test3

//beginning of test4
/*
PetscScalar test(PetscScalar x, PetscScalar y)
{
	return (2*y*y-2)*(2*x*x-2);
}


PetscScalar northboundary(PetscScalar x)
{
	PetscScalar y = 1;
	return (2*y*y-2)*(2*x*x-2);
}

PetscScalar southboundary(PetscScalar x)
{
	PetscScalar y = -1;
	return (2*x*x-2)*(2*y*y-2);
}

PetscScalar eastboundary(PetscScalar y)
{
	PetscScalar x = 1;
	return (2*x*x-2)*(2*y*y-2); 
}

PetscScalar westboundary(PetscScalar y)
{
	PetscScalar x = -1;
	return (2*y*y-2)*(2*x*x-2); 
}


PetscScalar westboundarysecondder(PetscScalar y)
{
	PetscScalar x = -1;
	return 0;
}

PetscScalar northboundarysecondder(PetscScalar x)
{
	PetscScalar y = 1;
	return 0;
}

PetscScalar southboundarysecondder(PetscScalar x)
{

	PetscScalar y = -1;
	return 0;
}

PetscScalar eastboundarysecondder(PetscScalar y)
{
	PetscScalar x = 1;
	return 0; 
}

PetscScalar forcing(PetscScalar x, PetscScalar y)
{
	return 4*(2*x*x-2) +4*(2*y*y-2);
}
*/
//end of test4


//beginning of test5
/*int polydeg=10;
PetscScalar test(PetscScalar x, PetscScalar y)
{
	PetscScalar ux = PetscPowScalarInt((x-1)/2.0, polydeg);
	PetscScalar uy = PetscPowScalarInt((y-1)/2.0, polydeg);
	return ux*uy;
}


PetscScalar northboundary(PetscScalar x)
{
	PetscScalar y = 1;
	PetscScalar ux = PetscPowScalarInt((x-1)/2.0, polydeg);
	PetscScalar uy = PetscPowScalarInt((y-1)/2.0, polydeg);
	return ux*uy;
}

PetscScalar southboundary(PetscScalar x)
{
	PetscScalar y = -1;
	PetscScalar ux = PetscPowScalarInt((x-1)/2.0, polydeg);
	PetscScalar uy = PetscPowScalarInt((y-1)/2.0, polydeg);
	return ux*uy;
}

PetscScalar eastboundary(PetscScalar y)
{
	PetscScalar x = 1;
	PetscScalar ux = PetscPowScalarInt((x-1)/2.0, polydeg);
	PetscScalar uy = PetscPowScalarInt((y-1)/2.0, polydeg);
	return ux*uy;
}

PetscScalar westboundary(PetscScalar y)
{
	PetscScalar x = -1;
	PetscScalar ux = PetscPowScalarInt((x-1)/2.0, polydeg);
	PetscScalar uy = PetscPowScalarInt((y-1)/2.0, polydeg);
	return ux*uy;
}


PetscScalar westboundarysecondder(PetscScalar y)
{
	PetscScalar x = -1;
	PetscScalar ux = PetscPowScalarInt((x-1)/2.0, polydeg);
	PetscScalar uy = PetscPowScalarInt((y-1)/2.0, polydeg);
	PetscScalar uxder = polydeg*(polydeg-1)/4.0*PetscPowScalarInt((x-1)/2.0, polydeg-2);
	PetscScalar uyder = polydeg*(polydeg-1)/4.0*PetscPowScalarInt((y-1)/2.0, polydeg-2);
 	return uxder*uy;
}

PetscScalar northboundarysecondder(PetscScalar x)
{
	PetscScalar y = 1;
	PetscScalar ux = PetscPowScalarInt((x-1)/2.0, polydeg);
	PetscScalar uy = PetscPowScalarInt((y-1)/2.0, polydeg);
	PetscScalar uxder = polydeg*(polydeg-1)/4.0*PetscPowScalarInt((x-1)/2.0, polydeg-2);
	PetscScalar uyder = polydeg*(polydeg-1)/4.0*PetscPowScalarInt((y-1)/2.0, polydeg-2);
 	return uxder*uy;
}

PetscScalar southboundarysecondder(PetscScalar x)
{

	PetscScalar y = -1;
	PetscScalar ux = PetscPowScalarInt((x-1)/2.0, polydeg);
	PetscScalar uy = PetscPowScalarInt((y-1)/2.0, polydeg);
	PetscScalar uxder = polydeg*(polydeg-1)/4.0*PetscPowScalarInt((x-1)/2.0, polydeg-2);
	PetscScalar uyder = polydeg*(polydeg-1)/4.0*PetscPowScalarInt((y-1)/2.0, polydeg-2);
 	return uxder*uy;
}

PetscScalar eastboundarysecondder(PetscScalar y)
{
	PetscScalar x = 1;
	PetscScalar ux = PetscPowScalarInt((x-1)/2.0, polydeg);
	PetscScalar uy = PetscPowScalarInt((y-1)/2.0, polydeg);
	PetscScalar uxder = polydeg*(polydeg-1)/4.0*PetscPowScalarInt((x-1)/2.0, polydeg-2);
	PetscScalar uyder = polydeg*(polydeg-1)/4.0*PetscPowScalarInt((y-1)/2.0, polydeg-2);
 	return uyder*ux;
}

PetscScalar forcing(PetscScalar x, PetscScalar y)
{
	PetscScalar ux = PetscPowScalarInt((x-1)/2.0, polydeg);
	PetscScalar uy = PetscPowScalarInt((y-1)/2.0, polydeg);
	PetscScalar uxder = polydeg*(polydeg-1)/4.0*PetscPowScalarInt((x-1)/2.0, polydeg-2);
	PetscScalar uyder = polydeg*(polydeg-1)/4.0*PetscPowScalarInt((y-1)/2.0, polydeg-2);
 	return uxder*uy + uyder*ux;	
}*/
//end of test5
