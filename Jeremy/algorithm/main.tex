\documentclass[12pt]{article}

\usepackage{fullpage}
\usepackage{caption}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{fancyvrb}
\usepackage{amsmath}
\usepackage{setspace}
\usepackage[margin=1in]{geometry}
\usepackage[resetlabels,labeled]{multibib}
\usepackage{wrapfig}
\usepackage{color}

\newcites{New}{The other list}

\begin{document}

\begin{wrapfigure}{r}{0.4\textwidth}
  \begin{center}
    \includegraphics[scale=0.4]{nsfpic.pdf}
  \end{center}
  \caption{\footnotesize{Blue:grid points to be calculated. Red:fixed boundary estimates}}
\end{wrapfigure}
    
 \section{Introduction} 
    
    Consider solving the problem $\bigtriangleup u = f$ numerically. 
    %Suppose we used a second-order discretization and an exact (e.g. spectral) solver. 
    
    Discretizing the domain by the Spectral Method and solving using a multigrid method yields an approximate solution with a fourier representation that is accurate up to the first k modes. To increase the value of this k, one can solve on a more refined grid. We consider a modification of multigrid: segmental refinement, which solves an overlapping decomposition of the domain by patches at a time. 
    
    
\begin{itemize}
\item Solve coarse grid
\item Quarter grid into four patches
\item Extend boundary of patch to include buffer region. 
\item Refine domain
\item Solve on patch, then repeat previous steps on this new patch. 
\end{itemize}

The main benefit of this algorithm is a decrease in memory requirements. Since each solve is down patch-wise, the total memory requirement scales logarithmically with respect to the total number of unknowns to be solved for on the smoothest level. The additional computations needed are a modest tradeoff for this massive decrease in memory requirements. As an illustration of the potential benefit of this algorithm, assume for now that the memory requirement is $O(logN)$.  Consider a problem that requires us to solve for $10^{12}$ unknowns, a challenge even for most supercomputers \cite{doi}. With this algorithm, memory would no longer be a bottleneck, meaning on a typical laptop capable of $10^9$ operations per second, performing multigrid, which requires about 100 operations per unknown \cite{brandt2011multigrid}, one could solve for $10^{12}$ unknowns in a little over 24 hours.

There have been previous explorations of this method, which was first proposed in 1994 and offered a theoretical justification \cite{brandt1994multigrid}. However, the justification here was purely theoretical and no code was published that performed the algorithm. Further studies have been done on the method since then, showing potential and marginal success, but so far there has been a limit on the number of refinements that can be done before the error introduced by this method grows too large, slowing down convergence. \cite{paper1}.

\textbf{cite multigrid}.
    
    
    
\section {Method of Analysis}    
    To study the convergence of multigrid methods, we will look at the decay of the solution to a PDE on a square domain with boundary values driven by high-frequency errors. As one moves from the boundary, the solution to this problem decays exponentially, which will motivate the size we require for our buffer. We require that the solution perturbation, driven by errors in the boundary values with frequencies greater than k, decays to discretization error some distance $d_l$ away from the patch boundary, where l is the level index for the patch. Continuing this process recursively, we can show that segmental refinement at each level finds solutions to a PDE up to discretization error when one allows an additional buffer of size $d_l$ for each level. 
    
    We assume that there is no interpolation error. So the solver used would be, for example, a spectral method that guarantees accuracy in a certain frequency range. 
    %Remark: We must show that the fourier coefficients of solutions to these laplace equations decay at a certain rate in order to show that solving up to $(2^{n})$ coefficients will yield a sum that decays very quickly as we take a shorter and shorter tail of the sum. Each of the functions Sin(k x) exist on the buffer zone boundary. Assume that the boundary has finite energy, meaning it exists in $L^2$, this implies that the sum of fourier coefficients converges by Parseval's Theorem. 
    
    First, we must show that on a square of length L with one nonhomogenous boundary condition, the function decays exponentially away from the boundary. 
    
    Consider the following PDE. 
    
 \begin{gather}
\nabla^2 \Phi (x,y) = 0 \label{laplaceexstate}\\
\begin{split}
\Phi(x,0) &= 0 \quad \Phi(0,y) = 0 \\
\Phi(L,y) &= 0 \quad \Phi(x,L) = g(x). \label{laplaceexboundary}
\end{split}
\end{gather}    

Now we must apply the nonhomogeneous boundary condition $\Phi(x,L) = g(x)$ to help us find the coefficients $B_n$.
\begin{align*}
\Phi(x,y)  &= \sum_{n=1}^{\infty} B_n \sin(\frac{n\pi x}{L}) \sinh(\frac{n\pi y}{L}) \\
\end{align*}
\begin{equation}
B_n = \frac{\frac{2}{L}}{\sinh(\frac{n\pi}{L})} \int_0^1 g(x) \sin(n\pi x) \, dx
\end{equation}

\begin{align*}
\text{Let C }= max(\int_0^1 (g(x) \sin(\frac{n\pi x}{L}) \, dx)\\
\textbf{To generalize this, I may have to make this C the max fourier coefficient of $g_y(x)$} \\
\textbf {for all y where $g_y$ is the function of the error at position y}\\
\textbf{May also need to use parsevals due to aliasing} \\
\text{Let $\Phi_n(x, y)$ }=  \frac{\frac{2}{L}}{\sinh(n\pi)}\int_0^1 (g(x) \sin(\frac{n\pi x}{L}) \, dx) \sin(\frac{n\pi x}{L}) \sinh(\frac{n\pi y}{L})\\
|\Phi_n(x, y)| \leq \frac{2C*sinh \frac{n \pi y}{L}}{L\sinh(n\pi)} \leq \frac{2C e^{\frac{n \pi y}{L}}}{L.5*e^{n \pi}} \leq \frac{4C e^{\frac{n \pi y}{L}}}{e^{n \pi}} = \frac{4C}{L} e^\frac{{n \pi (y-L)}}{L}
\end{align*}

Let $d = L-y$ be the distance from the boundary. 

\begin{align*}
|\Phi_n(x, y)| \leq \frac{4C}{L} e^\frac{{-n \pi d}}{L} \\
\end{align*}



Let $l = 0$ refer to the coarsest grid.  Higher levels refer to patches of greater refinement. 

The discretization error at each level is $C_D * \frac{1}{2^l n_0}^2$. Bear in mind that, in general, we want error from each boundary to contribute at most $\frac{C_D}{4} * \frac{1}{2^l n_0}^2$ so that the superposition of error induced by a maximum of four extended boundaries is less than discretization error


At level $l = 1$:
$L = \frac{1}{2}$

$|\Phi(x, y)| \leq \sum_{n=n_0}^{2n_0} \frac{4C * e^\frac{-n \pi d}{L}}{L} < n_0 *\frac{4C * e^\frac{-n_0 \pi d}{L}}{L} <  \frac{C_D}{4} * (\frac{1}{2 n_0})^2$

$d > \frac{-ln(C_D)  + ln(4) + ln(2) + 3 ln(n_0) + ln(4) + ln(C)}{2 n_0 \pi} $ 

Let $d_l$ refer to the distance requirement at l levels from the coarsest grid in order to have the erroneous solution decay to discretization error. 
 
$d_l > \frac{(2l-1)ln(2) + ln(4C) + ln(4) + 3ln(n_0) - ln(C_D)}{n_0 \pi 2^{l-1}}$

The sum $\sum_{l=1}^\infty d_l$ converges


Let $d'_l$ be the buffer required at level l, i.e. $d'_l= \sum_{k = l}^\infty d_k = \frac{2*ln(4C) + 2l*ln(4) + 6ln(n_0) + ln(16) + ln(4)}{n_0 \pi 2^{l-1}}$. So the number of cells required in the buffer in each direction is $d'_l* 2^{l} =  \frac{4ln2 *(2l+1)}{n_0 \pi} + \frac{4(-ln2 + ln(4C) +ln(4) + 3ln(n_0) - ln(C_D))}{n_0 \pi}$ 

\section{Extension to Other Bases}

Above we have proven that if we approximate the solution to a Laplacian with the basis $\{Sinh(n\pix\}


\section{Memory Usage}
Let $n_0$ be the number of unknowns in one dimension at the coarsest level. Consider at i levels of refinement, we have $2^i n_0$ unknowns in one dimension as a result of i smoothing operations. Denote the  number of unknowns at the smoothest level by $N$.  Thus the maximum number of refinements is $l = log(N) - log(n_0)$. Since we half (in each dimension) the size of the subdomains domain every time , we have $\frac{2^i}{2^i} n_0 = n_0$ unknowns in the area of interest at every level, but must also calculate cells in a buffer region. 

Note that the number of cells in the buffer region grows at a constant rate (which is proven in the previous section). So we need $n_0 + c i$ cells in each dimension at i levels of refinement. Larger buffer regions correspond to a larger constant. 

Let d be the dimension of the domain. 

$O(\sum_{i=0}^l (n_0 + c i)^d) = O(n_0 * l) + O(\sum_{i=0}^l c i^d) = O(n_0 logN) + O(l^{d+1}) = O( n_0 log N) + O(logN)^d $

\section{Results}

Below are some numerical examples that give an idea of situations in which it would be feasible to use segmental refinement. The variable $n_0$ refers to the number of frequencies that are solved for on the coarse grid. For extremely small $n_0$, the buffer size required is larger than the size of the patch.

However, one sees that the buffer size required grows smaller as we solve for more frequencies on the coarsest level, making segmental refinement a more tractctable approach.. 
\begin{table}
\begin{center}
\begin{tabular}{c | c |  c | c}
$n_0$  & $d$ & $d_2'$ & $d_3'$\\
\hline
{\color{red} 10} & \color{red} .75 & \color{red}.42 & \color{red}.23\\ 
\hline
100 & .12 & .06 & .03\\
\hline
1000 & .016   &  .009 & .005\\
\hline
\end{tabular}
\caption*{Colored in red is the case where only ten unknowns are solved for because in this situation, segmental refinement would not be feasible.The buffer region required would be of size .75 in each dimension at the first level of refinement. However, at the first level of refinement, one is solving on a domain that is of size .5x.5, leaving a maximum additional region of .5 in each dimension. The buffer size required is larger than the largest possible buffer size. 
}
\end{center}
\end{table}






\section{Conclusion}
Segmental refinement has massive advantages with regards to memory usage while only requiring a modest amount of redundant computations. If one has a sufficiently refined coarse grid, then performing additional refinements using this algorithm will yield errors that are smaller than discretization error with a tractable amount of required redundant calculations. 
 
{\footnotesize
\bibliographystyle{IEEEtran}
\bibliography{references}


\bibliographystyleNew{IEEEtran}
\bibliographyNew{references}
}
\end{document}

