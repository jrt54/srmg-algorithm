#include "SRMG.h"
#include <petscsys.h>
#include <petscdm.h>
#include <petscdmda.h>
#include <petscsnes.h>
#include <petscmatlab.h>
static char help[] = "Solving Laplace via Chebyshev";
#include <petsctime.h>
#include <petscmat.h>
#include <stdio.h>
#include <petscdm.h>
#include <petscdmda.h>
#include <petscdraw.h>
#include <time.h>
//#include "TestFunctions.h"

//maps [a, b] to [c, d]



PetscScalar BoxSoln(struct patch *mypatch, PetscScalar xpoint, PetscScalar ypoint){
    PetscErrorCode ierr;
    
    PetscScalar LowerLeft;
    PetscScalar UpperLeft;
    PetscScalar UpperRight;
    PetscScalar LowerRight;
    PetscScalar v;
    
    
//assumes root patch is [-1, 1] x [-1, 1]
if(mypatch->parent == NULL){
    LowerLeft = mypatch->south(mypatch->LLxcoord);
    UpperLeft = mypatch->north(mypatch->LLxcoord);
    UpperRight = mypatch->north(mypatch->URxcoord);
    LowerRight = mypatch->south(mypatch->URxcoord);

    v =  (1-xpoint)/2.0*mypatch->west(ypoint)
    + (1+xpoint)/2.0*mypatch->east(ypoint) 
    + (1-ypoint)/2.0*mypatch->south(xpoint) 
    + (1+ypoint)/2.0*mypatch->north(xpoint)
    - (1-xpoint)*(1-ypoint)/4.0*LowerLeft 
    - (1-xpoint)*(1+ypoint)/4.0*UpperLeft 
    - (1+xpoint)*(1-ypoint)/4.0*LowerRight
    - (1+xpoint)*(1+ypoint)/4.0*UpperRight;
     
}



else{
    //Must impose equality at x = llxcord, y = llycord etc
    PetscScalar relativexpoint = xpoint;
    PetscScalar relativeypoint = ypoint;
    intervalShift(mypatch->LLxcoord, mypatch->URxcoord, -1, 1,  &relativexpoint);
    intervalShift(mypatch->LLycoord, mypatch->URycoord, -1, 1,  &relativeypoint);
    //relative points are what x, y would be if shifted to [-1, 1] x [-1, 1] square
    v= (1-relativexpoint)/2.0*PatchSolution(mypatch->parent, mypatch->LLxcoord, ypoint)  //impose west boundary from parent patch at new patch boundary
    + (1+relativexpoint)/2.0*PatchSolution(mypatch->parent, mypatch->URxcoord, ypoint)  // east boundary
    + (1-relativeypoint)/2.0*PatchSolution(mypatch->parent, xpoint, mypatch->LLycoord)   // south boundary
    + (1+relativeypoint)/2.0*PatchSolution(mypatch->parent, xpoint, mypatch->URycoord) // north boundary
    - (1-relativexpoint)*(1-relativeypoint)/4.0*PatchSolution(mypatch->parent, mypatch->LLxcoord, mypatch->LLycoord) //LL corner
    - (1-relativexpoint)*(1+relativeypoint)/4.0*PatchSolution(mypatch->parent, mypatch->LLxcoord, mypatch->URycoord) //UL corner
    - (1+relativexpoint)*(1-relativeypoint)/4.0*PatchSolution(mypatch->parent, mypatch->URxcoord, mypatch->LLycoord) //LR corner
    - (1+relativexpoint)*(1+relativeypoint)/4.0*PatchSolution(mypatch->parent, mypatch->URxcoord, mypatch->URycoord); //UR corner

}

    return v;

}

PetscScalar LaplaceBoxSoln(struct patch* mypatch, PetscScalar xpoint, PetscScalar ypoint){
	PetscErrorCode ierr;
	PetscScalar v = 0;
	PetscScalar vyy;
	PetscScalar vxx;
	PetscScalar northder, eastder, southder, westder;
	//PetscInt NumBases;
	PetscLogEvent FormLaplaceBox;
	//PetscLogDouble user_event_flops_laplacebox;

	PetscLogEventRegister("Getting LaplaceBox",0,&FormLaplaceBox);
	PetscLogEventBegin(FormLaplaceBox,0,0,0,0);


	    /* Box =  (1-xpoint)/2.0*mypatch->west(ypoint)
		    + (1+xpoint)/2.0*mypatch->east(ypoint) 
		    + (1-ypoint)/2.0*mypatch->south(xpoint) 
		    + (1+ypoint)/2.0*mypatch->north(xpoint)
		    - (1-xpoint)*(1-ypoint)/4.0*LowerLeft
		    - (1-xpoint)*(1+ypoint)/4.0*UpperLeft
		    - (1+xpoint)*(1-ypoint)/4.0*LowerRight
		    - (1+xpoint)*(1+ypoint)/4.0*UpperRight*/
	if(mypatch->parent == NULL){
	   
	    vxx = (1-ypoint)/2.0*mypatch->southsecondder(xpoint) 
		+ (1+ypoint)/2.0*mypatch->northsecondder(xpoint);
	    
	    vyy = (1-xpoint)/2.0*mypatch->westsecondder(ypoint)
		+ (1+xpoint)/2.0*mypatch->eastsecondder(ypoint);
	    
	    v = vxx + vyy;

	    }


        else{
	    PetscLogEvent RecursiveFormLaplace;
	    PetscScalar *coeff;

	    //Number of bases from parent patch
	    PetscInt NumPoints = mypatch->parent -> NumPoints;

	    VecGetArray(mypatch->parent->coefficients, &coeff);

	    southder = 0;
	    westder = 0;
	    northder = 0;
	    eastder = 0;

	    PetscScalar relativexpoint = xpoint;
	    PetscScalar relativeypoint = ypoint;

	    //get the exact coordinate value of the new  boundaries
	    //then shift it to the relative value of where it is if we mapped its position on the parent node to  a full square
	    PetscScalar southboundary = mypatch->LLycoord;
	    intervalShift(mypatch->parent->LLycoord, mypatch->parent->URycoord, -1, 1, &southboundary);
	    PetscScalar northboundary = mypatch->URycoord;
	    intervalShift(mypatch->parent->LLycoord, mypatch->parent->URycoord, -1, 1, &northboundary);


	    PetscScalar eastboundary = mypatch->URxcoord;
	    intervalShift(mypatch->parent->LLxcoord, mypatch->parent->URxcoord, -1, 1, &eastboundary);
	    PetscScalar westboundary = mypatch->LLxcoord;
	    intervalShift(mypatch->parent->LLxcoord, mypatch->parent->URxcoord, -1, 1, &westboundary);


	    //here we are getting info from PARENT patch because we are constructing box given by soln from PREVIOUS patch
	    intervalShift(mypatch->parent->LLxcoord, mypatch->parent->URxcoord, -1, 1,  &relativexpoint);
	    intervalShift(mypatch->parent->LLycoord, mypatch->parent->URycoord, -1, 1,  &relativeypoint);

	    PetscScalar yshiftder =  4/((mypatch->parent->URycoord-mypatch->parent->LLycoord)*(mypatch->parent->URycoord-mypatch->parent->LLycoord));
	    PetscScalar xshiftder =  4/((mypatch->parent->URxcoord-mypatch->parent->LLxcoord)*(mypatch->parent->URxcoord-mypatch->parent->LLxcoord));

	    double *chebtestx = Chebpoly(relativexpoint, NumPoints);
	    double *chebderivx = Chebderiv(relativexpoint, NumPoints, chebtestx);
	    double *chebsecondderivx = Chebsecondderiv(relativexpoint, NumPoints, chebderivx);
	    double *chebtesty = Chebpoly(relativeypoint, NumPoints);
	    double *chebderivy = Chebderiv(relativeypoint, NumPoints, chebtesty);
	    double *chebsecondderivy = Chebsecondderiv(relativeypoint, NumPoints, chebderivy);
	    double *chebtestxatwest = Chebpoly(westboundary, NumPoints);
	    double *chebtestyatsouth = Chebpoly(southboundary, NumPoints);
	    double *chebtestyatnorth = Chebpoly(northboundary, NumPoints);
	    double *chebtestxateast = Chebpoly(eastboundary, NumPoints);


	    //set southder , fix y at current southboundary
	      //returns d^2/dx^2 cheb(southypoint, x)
              for (PetscInt jcol = 0; jcol < NumPoints-2; ++jcol){
                for (PetscInt icol = 0; icol < NumPoints-2; ++icol){      
                   PetscInt col = icol + jcol*(NumPoints-2);

                   //set northder , fix y at current northboundary
	           //returns d^2/dx^2 cheb(northypoint, x)
                   if ((icol+2)%2 == 0 && (jcol+2)%2 == 0){
	           //using recombination of bases that has homogenous boundaries
                   northder = northder + coeff[col]* xshiftder*chebsecondderivx[icol+2]*(chebtestyatnorth[jcol+2]-1);
                   southder = southder + coeff[col]* xshiftder*chebsecondderivx[icol+2]*(chebtestyatsouth[jcol+2]-1);  
                   eastder = eastder + coeff[col]* yshiftder*chebsecondderivy[jcol+2]*(chebtestxateast[icol+2]-1);
                   westder = westder + coeff[col]* yshiftder*chebsecondderivy[jcol+2]*(chebtestxatwest[icol+2]-1);
                   }
                   if ((icol+2)%2 == 0 && (jcol+2)%2 == 1){
                   //using recombination of bases that has homogenous boundaries
                   southder = southder + coeff[col]*xshiftder*chebsecondderivx[icol+2]*(chebtestyatsouth[jcol+2]-southboundary);
                   northder = northder + coeff[col]*xshiftder*chebsecondderivx[icol+2]*(chebtestyatnorth[jcol+2]-northboundary);
                   eastder = eastder + coeff[col]*yshiftder*chebsecondderivy[jcol+2]*(chebtestxateast[icol+2]-1);
                   westder = westder + coeff[col]*yshiftder*chebsecondderivy[jcol+2]*(chebtestxatwest[icol+2]-1);
                   }
                   if ((icol+2)%2 == 1 && (jcol+2)%2 == 0){
                   //using recombination of bases that has homogenous boundaries
                   southder = southder + coeff[col]* xshiftder*chebsecondderivx[icol+2]*(chebtestyatsouth[jcol+2]-1);
                   northder = northder + coeff[col]* xshiftder*chebsecondderivx[icol+2]*(chebtestyatnorth[jcol+2]-1);
                   eastder = eastder + coeff[col]*yshiftder*chebsecondderivy[jcol+2]*(chebtestxateast[icol+2]-eastboundary);
                   westder = westder + coeff[col]*yshiftder*chebsecondderivy[jcol+2]*(chebtestxatwest[icol+2]-1);
                   }
                   if ((icol+2)%2 == 1 && (jcol+2)%2 == 1){
                   //using recombination of bases that has homogenous boundaries
                   southder = southder + coeff[col]*xshiftder*chebsecondderivx[icol+2]*(chebtestyatsouth[jcol+2]-southboundary);
                   northder = northder + coeff[col]*xshiftder*chebsecondderivx[icol+2]*(chebtestyatnorth[jcol+2]-northboundary);
                   eastder = eastder + coeff[col]*yshiftder*chebsecondderivy[jcol+2]*(chebtestxateast[icol+2]-eastboundary);
                   westder = westder + coeff[col]*yshiftder*chebsecondderivy[jcol+2]*(chebtestxatwest[icol+2]-westboundary);
                   }
             
                }
             }
             
             relativexpoint = xpoint;
             relativeypoint = ypoint;
             //here we are getting info from current patch, so that exact boundary conditions are met at boundary of CURRENT patch
             intervalShift(mypatch->LLxcoord, mypatch->URxcoord, -1, 1,  &relativexpoint);
             intervalShift(mypatch->LLycoord, mypatch->URycoord, -1, 1,  &relativeypoint);
             
             
             vxx = (1-relativeypoint)/2.0*southder//encodes south boundary laplace
             + (1+relativeypoint)/2.0*northder; //north boundary laplace
                 
             vyy = (1-relativexpoint)/2.0*westder //encodes east boundary laplace
             + (1+relativexpoint)/2.0*eastder; // west laplace
             
             v = vxx + vyy;
             PetscLogEventRegister("Recursive old LaplaceBox",0,&RecursiveFormLaplace);
             PetscLogEventBegin(RecursiveFormLaplace,0,0,0,0);
             v = LaplaceBoxSoln(mypatch->parent, xpoint, ypoint) + v;
             PetscLogEventEnd(RecursiveFormLaplace, 0, 0, 0, 0);
             
             VecRestoreArray(mypatch->parent->coefficients, &coeff);
             
             free(chebtestx);
             free(chebderivx);
             free(chebsecondderivx);
             free(chebtesty);
             free(chebderivy);
             free(chebsecondderivy);
             free(chebtestxatwest);
             free(chebtestyatsouth);
             free(chebtestyatnorth);
             free(chebtestxateast);
	 
        }


    PetscLogEventEnd(FormLaplaceBox,0,0,0,0);

    return v;
}


PetscErrorCode FreePatches(struct patch* mypatch)
{
if(mypatch != NULL)
{

   

  
  struct patch* lowerleft = mypatch->lowerleft; 
  struct patch* lowerright = mypatch->lowerright; 
  struct patch* upperright = mypatch->upperright; 
  struct patch* upperleft = mypatch -> upperleft;

  VecDestroy(&mypatch->coefficients);
  free(mypatch);
  mypatch = NULL;
  
  FreePatches(lowerleft);
  FreePatches(lowerright);
  FreePatches(upperleft);
  FreePatches(upperright);
}

PetscFunctionReturn(0);
}



PetscErrorCode constructSubPatches(struct patch* mypatch, KSP ksp, PetscScalar buffersize)
{

PC pc;
PetscInt NumRows, NumColumns;
PetscErrorCode ierr;


  struct patch* lowerleft = malloc(sizeof(struct patch));
  struct patch* lowerright = malloc(sizeof(struct patch));
  struct patch* upperleft = malloc(sizeof(struct patch));
  struct patch* upperright = malloc(sizeof(struct patch));
  //struct patch* lowerright, struct patch* upperleft, struct patch* upperright


    //struct patch* currentpatch = mypatch;
    //while(currentpatch->parent != NULL)
    //{  
    //currentpatch = mypatch->parent;  
    //}
    PetscScalar CoarseResolution = mypatch->CoarseResolution;  
    lowerleft->CoarseResolution = CoarseResolution;
    lowerright->CoarseResolution = CoarseResolution;
    upperleft->CoarseResolution = CoarseResolution;
    upperright->CoarseResolution = CoarseResolution;
    //PetscScalar NumPoints = currentpatch->NumPoints;  

  PetscScalar xmidpoint = .5*(mypatch->interiorLLxcoord + mypatch ->interiorURxcoord);

  PetscScalar ymidpoint = .5*(mypatch -> interiorLLycoord + mypatch -> interiorURycoord);


  
  lowerleft ->interiorLLxcoord = mypatch -> interiorLLxcoord;
  lowerleft ->interiorLLycoord = mypatch -> interiorLLycoord;
  lowerleft ->interiorURxcoord = xmidpoint;
  lowerleft ->interiorURycoord = ymidpoint;
  lowerleft ->LLxcoord = lowerleft->interiorLLxcoord;//LowerLeft patch touches lowerleft xboundary
  lowerleft ->LLycoord = lowerleft->interiorLLycoord;//Lowerleft patch touches lowerleft yboundary
  lowerleft ->URxcoord = xmidpoint+buffersize*(lowerleft->interiorURxcoord-lowerleft->interiorLLxcoord);
  lowerleft ->URycoord = ymidpoint+buffersize*(lowerleft->interiorURycoord-lowerleft->interiorLLycoord);
  lowerleft -> parent = mypatch;
  lowerleft -> forcing = mypatch -> forcing;
  //lowerleft -> NumPoints = PetscCeilReal(NumPoints*(1+buffer));
  lowerleft -> NumPoints = (int)PetscCeilReal(CoarseResolution*(1+buffersize));
  mypatch -> lowerleft = lowerleft;
  lowerleft->lowerleft = NULL;
  lowerleft->lowerright = NULL;
  lowerleft->upperleft = NULL;
  lowerleft->upperright = NULL;
  
  lowerright ->interiorLLxcoord = xmidpoint;
  lowerright ->interiorLLycoord = mypatch -> interiorLLycoord;
  lowerright ->interiorURxcoord = mypatch -> interiorURxcoord;
  lowerright ->interiorURycoord = ymidpoint;
  lowerright ->LLxcoord = xmidpoint-buffersize*(lowerright->interiorURxcoord-lowerright->interiorLLxcoord); 
  lowerright ->LLycoord = lowerright->interiorLLycoord;//LowerRight patch touches lowerleft yboundary
  lowerright ->URxcoord = lowerright->interiorURxcoord; //Lowerright touches upperright xboundary
  lowerright ->URycoord = ymidpoint+buffersize*(lowerright->interiorURycoord-lowerright->interiorLLycoord);
  lowerright -> parent = mypatch;
  lowerright -> forcing = mypatch -> forcing;
  //lowerright-> NumPoints = PetscCeilReal(NumPoints*(1+buffer));
  lowerright -> NumPoints = (int)PetscCeilReal(CoarseResolution*(1+buffersize));
  mypatch -> lowerright = lowerright;
  lowerright->lowerleft = NULL;
  lowerright->lowerright = NULL;
  lowerright->upperleft = NULL;
  lowerright->upperright = NULL;


  upperright ->interiorLLxcoord =  xmidpoint;
  upperright ->interiorLLycoord = ymidpoint;
  upperright ->interiorURxcoord = mypatch -> interiorURxcoord;
  upperright ->interiorURycoord = mypatch -> interiorURycoord;
  upperright ->LLxcoord = xmidpoint-buffersize*(upperright->interiorURxcoord-upperright->interiorLLxcoord);
  upperright ->LLycoord = ymidpoint-buffersize*(upperright->interiorURycoord-upperright->interiorLLycoord);
  upperright ->URxcoord = upperright -> interiorURxcoord;
  upperright ->URycoord = upperright -> interiorURycoord;
  upperright -> parent = mypatch;
  upperright -> forcing = mypatch -> forcing;
  //upperright-> NumPoints = PetscCeilReal(NumPoints*(1+buffer));
  upperright -> NumPoints = (int)PetscCeilReal(CoarseResolution*(1+buffersize));
  mypatch -> upperright = upperright;
  upperright->lowerleft = NULL;
  upperright->lowerright = NULL;
  upperright->upperleft = NULL;
  upperright->upperright = NULL;




  upperleft ->interiorLLxcoord =  mypatch -> interiorLLxcoord;
  upperleft ->interiorLLycoord = ymidpoint;
  upperleft ->interiorURxcoord = xmidpoint;
  upperleft ->interiorURycoord = mypatch -> interiorURycoord;
  upperleft ->LLxcoord =  upperleft -> interiorLLxcoord;
  upperleft ->LLycoord = ymidpoint-buffersize*(upperleft->interiorURycoord-upperleft->interiorLLycoord);
  upperleft ->URxcoord = xmidpoint+buffersize*(upperleft->interiorURxcoord-upperleft->interiorLLxcoord);
  upperleft ->URycoord = upperleft -> interiorURycoord;
  upperleft -> parent = mypatch;
  upperleft -> forcing = mypatch -> forcing;
  upperleft -> NumPoints = mypatch -> NumPoints;
  //upperleft-> NumPoints = PetscCeilReal(NumPoints*(1+buffer));
  upperleft -> NumPoints = (int)PetscCeilReal(CoarseResolution*(1+buffersize));
  mypatch -> upperleft = upperleft;
  upperleft->lowerleft = NULL;
  upperleft->lowerright = NULL;
  upperleft->upperleft = NULL;
  upperleft->upperright = NULL;


  
  SolveCoeff(mypatch -> lowerleft, ksp);
  SolveCoeff(mypatch -> lowerright, ksp);
  SolveCoeff(mypatch -> upperleft, ksp);
  SolveCoeff(mypatch -> upperright, ksp);

  PetscFunctionReturn(0);



}



int main(int argc,char **args) {
PetscErrorCode ierr; 
//PetscDraw draw; 
KSP ksp;
PC pc;
Mat derivmat;
PetscLogEvent FreeingPatches, TestingPatches, SRMG, coarsesolve;
//PetscLogDouble user_event_flops_test;//,user_event_flops_free;
PetscInt CoarseResolution = 5;
PetscScalar buffersize = 0.0;
PetscInt L = 1;

PetscInitialize(&argc,&args,(char*)0,help);
ierr = PetscOptionsGetInt(NULL,NULL,"-N",&CoarseResolution,NULL);CHKERRQ(ierr);
ierr = PetscOptionsGetInt(NULL,NULL,"-L",&L,NULL);CHKERRQ(ierr);
ierr = PetscOptionsGetScalar(NULL,NULL,"-buffer",&buffersize,NULL);CHKERRQ(ierr);

PetscScalar eps = 10e-7;
PetscScalar tol = 10e-4;


struct patch* coarse = malloc(sizeof(struct patch));
  //lowerleft ->interiorLLxcoord = mypatch -> LLxcoord;
  //lowerleft ->interiorLLycoord = mypatch -> LLycoord;
  //lowerleft ->interiorURxcoord = xmidpoint;
  //lowerleft ->interiorURycoord = ymidpoint;

coarse->parent = NULL;
coarse->interiorLLxcoord = -1;
coarse->interiorLLycoord = -1;
coarse->interiorURxcoord = 1;
coarse->interiorURycoord = 1;
coarse->LLxcoord = -1;
coarse->LLycoord = -1;
coarse->URxcoord = 1;
coarse->URycoord = 1;
coarse->north = &northboundary;
coarse->south = &southboundary;
coarse->east = &eastboundary;
coarse->west = &westboundary;
coarse->northsecondder = &northboundarysecondder;
coarse->southsecondder = &southboundarysecondder;
coarse->eastsecondder = &eastboundarysecondder;
coarse->westsecondder = &westboundarysecondder;
coarse->forcing = &forcing;
coarse->NumPoints = CoarseResolution;
coarse->CoarseResolution = CoarseResolution;
coarse->lowerleft = NULL;
coarse->lowerright = NULL;
coarse->upperleft = NULL;
coarse->upperright = NULL;



PetscLogEventRegister("coarsesolve",0,&coarsesolve);
PetscLogEventBegin(coarsesolve,0,0,0,0);

KSPCreate(PETSC_COMM_WORLD,&ksp);

ierr = MatCreateSeqDense(PETSC_COMM_WORLD, (coarse->NumPoints-2)*(coarse->NumPoints-2), (coarse->NumPoints-2)*(coarse->NumPoints-2), NULL, &derivmat);CHKERRQ(ierr);
ierr = FormInteriorChebMatrix(derivmat, coarse);CHKERRQ(ierr);
ierr = MatAssemblyBegin(derivmat,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
ierr = MatAssemblyEnd(derivmat,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
//ierr = MatView(derivmat, PETSC_VIEWER_STDOUT_SELF); CHKERRQ(ierr);


ierr = KSPSetOperators(ksp,derivmat,derivmat); CHKERRQ(ierr);
ierr = KSPGetPC(ksp,&pc); CHKERRQ(ierr);
ierr = PCSetType(pc,PCLU); CHKERRQ(ierr);
ierr = KSPSetTolerances(ksp,1.e-18,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT);CHKERRQ(ierr);
ierr = KSPSetFromOptions(ksp);CHKERRQ(ierr);



SolveCoeff(coarse, ksp);

PetscLogEventEnd(coarsesolve,0,0,0,0);


PetscLogEventRegister("SRMG",0,&SRMG);
PetscLogEventBegin(SRMG,0,0,0,0);
SegmentalRefinement(coarse, L, ksp, buffersize);
PetscLogEventEnd(SRMG,0,0,0,0);





int resolution=20;
PetscInt Num_Quad_Points = 10;
int curr_level = 0;
//struct patch* currentpatch = coarse;
/*while(curr_level <= L)
{
PetscScalar integral = 0;
PatchError(currentpatch, test, Num_Quad_Points, &integral);
printf("The integral of the error function is: %f\n", integral);
PatchErrorinBuffer(currentpatch, test, Num_Quad_Points, .5, &integral);
printf("The integral of the error function in half of total region is: %f\n", integral);
printf("Region needed is %f \n", BufferNeeded(currentpatch, test, eps, Num_Quad_Points));
printf("Region is (%f, %f)x(%f, %f) \n", currentpatch->LLxcoord, currentpatch->URxcoord, currentpatch->LLycoord, currentpatch->URycoord);
int sizeoflevel=sizeof(curr_level)*(sizeof(char)/sizeof(int)+1);//get size of level digit
char *plotname = malloc(sizeof(char)*(strlen("heatmapofpatch_") +sizeoflevel+ strlen(".png")));
sprintf(plotname, "heatmapofpatch_%d.png", curr_level);
printf("filename %s \n", plotname);
PlotPatch(currentpatch, resolution, plotname);  
currentpatch = currentpatch->lowerleft;
curr_level = curr_level + 1;
}*/

printf("buffersize: %f, N_0: %d \n", buffersize, CoarseResolution);
for(PetscInt depth_level=0; depth_level <= L; depth_level++)
{
PetscScalar total_integral = 0;
TotalError(coarse, test, Num_Quad_Points, &total_integral, 0, depth_level);
printf("Total integral at level %d is %f \n", depth_level, total_integral );
PetscScalar total_res = 0;
TotalRes(coarse, Num_Quad_Points, &total_res, 0, depth_level);
printf("Total integral of res at level %d is %f \n", depth_level, total_res );
}
PlotError(coarse, resolution, "heatmaperror.png", test);



//Very slow code that exhaustively searches every patch for large error
/*
PetscLogEventRegister("TestPatches",0,&TestingPatches);
PetscLogEventBegin(TestingPatches,0,0,0,0);
TestPatches(coarse, test, tol, Num_Quad_Points);
PetscLogEventEnd(TestingPatches,0,0,0,0);
*/


PetscLogEventRegister("FreeingPatches",0,&FreeingPatches);
PetscLogEventBegin(FreeingPatches,0,0,0,0);

FreePatches(coarse);
//PetscLogFlops(user_event_flops_free);
PetscLogEventEnd(FreeingPatches,0,0,0,0);


KSPDestroy(&ksp);
MatDestroy(&derivmat);

PetscFinalize();
return 0;
}




