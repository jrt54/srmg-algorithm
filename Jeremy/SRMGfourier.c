#include "SRMG.h"
#include <petscsys.h>
#include <petscdm.h>
#include <petscdmda.h>
#include <petscsnes.h>
#include <petscmatlab.h>
static char help[] = "Solving Laplace via Chebyshev";
#include <petsctime.h>
#include <petscmat.h>
#include <stdio.h>
#include <petscdm.h>
#include <petscdmda.h>
#include <petscdraw.h>
#include <time.h>
#include <fftw3.h>
//#include "TestFunctions.h"



PetscScalar BoxSoln(struct patch* mypatch, PetscScalar xpoint, PetscScalar ypoint)
{
    PetscErrorCode ierr;
    PetscLogEvent InverseFourier;
    PetscScalar LowerLeft;
    PetscScalar UpperLeft;
    PetscScalar UpperRight;
    PetscScalar LowerRight;
    PetscScalar v;
    PetscScalar val;
    PetscScalar totalval=0;
    Vec Sinh;
    PetscInt NumPoints=mypatch->NumPoints;
    
    
    
    PetscLogEventRegister("InverseFourier",0,&InverseFourier);
    PetscLogEventBegin(InverseFourier,0,0,0,0);
    PetscScalar Eastx = mypatch->URxcoord;
    PetscScalar Westx = mypatch->LLxcoord;
    PetscScalar Northy = mypatch->URycoord;
    PetscScalar Southy = mypatch->LLycoord;
    
    ierr = VecCreate(PETSC_COMM_WORLD,&Sinh);CHKERRQ(ierr);
    ierr = VecSetSizes(Sinh,PETSC_DECIDE,NumPoints);CHKERRQ(ierr);
    ierr = VecSetFromOptions(Sinh);CHKERRQ(ierr);
    
    
    //East Value
    for(int i=0; i<NumPoints; ++i){
    v = PetscSinhReal(PETSC_PI*(i+1)*(xpoint-Westx)/(Northy-Southy))*2.0/(PetscSinhReal(PETSC_PI*(i+1)*(Eastx-Westx)/(Northy-Southy)));
    v*=PetscSinReal(PETSC_PI*(i+1)*(ypoint-Southy)/(Northy-Southy));
    ierr = VecSetValues(Sinh, 1, &i, &v, INSERT_VALUES);CHKERRQ(ierr);    
    }
    ierr=VecDot(Sinh,mypatch->Eastcoefficients,&val);CHKERRQ(ierr);
    totalval+=val;
    
    //West Value
    for(int i=0; i<NumPoints; ++i){
    v = PetscSinhReal(PETSC_PI*(i+1)*(xpoint-Eastx)/(Northy-Southy))*2.0/(PetscSinhReal(PETSC_PI*(i+1)*(Westx-Eastx)/(Northy-Southy)));
    v*=PetscSinReal(PETSC_PI*(i+1)*(ypoint-Southy)/(Northy-Southy));
    ierr = VecSetValues(Sinh, 1, &i, &v, INSERT_VALUES);CHKERRQ(ierr);    
    }
    ierr=VecDot(Sinh,mypatch->Westcoefficients,&val);CHKERRQ(ierr);
    totalval+=val;
    
    //North Value
    for(int i=0; i<NumPoints; ++i){
    v = PetscSinhReal(PETSC_PI*(i+1)*(ypoint-Southy)/(Eastx-Westx))*2.0/(PetscSinhReal(PETSC_PI*(i+1)*(Northy-Southy)/(Eastx-Westx)));
    v*=PetscSinReal(PETSC_PI*(i+1)*(xpoint-Westx)/(Eastx-Westx));
    ierr = VecSetValues(Sinh, 1, &i, &v, INSERT_VALUES);CHKERRQ(ierr);    
    }
    ierr=VecDot(Sinh,mypatch->Northcoefficients,&val);CHKERRQ(ierr);
    totalval+=val;
    
    //South Value
    for(int i=0; i<NumPoints; ++i){
    v = PetscSinhReal(PETSC_PI*(i+1)*(ypoint-Northy)/(Eastx-Westx))*2.0/(PetscSinhReal(PETSC_PI*(i+1)*(Southy-Northy)/(Eastx-Westx)));
    v*=PetscSinReal(PETSC_PI*(i+1)*(xpoint-Westx)/(Eastx-Westx));
    ierr = VecSetValues(Sinh, 1, &i, &v, INSERT_VALUES);CHKERRQ(ierr);    
    }
    ierr=VecDot(Sinh,mypatch->Southcoefficients,&val);CHKERRQ(ierr);
    totalval+=val;
    


    PetscLogEventEnd(InverseFourier,0,0,0,0);
    return totalval;
}

PetscScalar LaplaceBoxSoln(struct patch* mypatch, PetscScalar xpoint, PetscScalar ypoint){
    return 0;
}


PetscErrorCode ConstructFourier(struct patch* mypatch)
{
    PetscErrorCode ierr;
    PetscInt NumPoints = mypatch -> NumPoints;
    //Building box coefficients
    ierr = VecCreate(PETSC_COMM_WORLD,&mypatch->Eastcoefficients);
    ierr = VecSetSizes(mypatch->Eastcoefficients,PETSC_DECIDE,NumPoints);
    ierr = VecSetFromOptions(mypatch->Eastcoefficients);

    ierr = VecCreate(PETSC_COMM_WORLD,&mypatch->Westcoefficients);
    ierr = VecSetSizes(mypatch->Westcoefficients,PETSC_DECIDE,NumPoints);
    ierr = VecSetFromOptions(mypatch->Westcoefficients);
    
    ierr = VecCreate(PETSC_COMM_WORLD,&mypatch->Northcoefficients);
    ierr = VecSetSizes(mypatch->Northcoefficients,PETSC_DECIDE,NumPoints);
    ierr = VecSetFromOptions(mypatch->Northcoefficients);
    
    ierr = VecCreate(PETSC_COMM_WORLD,&mypatch->Southcoefficients);
    ierr = VecSetSizes(mypatch->Southcoefficients,PETSC_DECIDE,NumPoints);
    ierr = VecSetFromOptions(mypatch->Southcoefficients);
    
    FourierBoundaryCoeff(mypatch->Eastcoefficients, mypatch->Westcoefficients, mypatch->Northcoefficients, mypatch->Southcoefficients, mypatch); 

    PetscFunctionReturn(0);    
}



PetscErrorCode constructSubPatches(struct patch* mypatch, KSP ksp, PetscScalar buffersize)
{

PC pc;
PetscInt NumRows, NumColumns;
PetscErrorCode ierr;


    struct patch* lowerleft = malloc(sizeof(struct patch));
    struct patch* lowerright = malloc(sizeof(struct patch));
    struct patch* upperleft = malloc(sizeof(struct patch));
    struct patch* upperright = malloc(sizeof(struct patch));
    //struct patch* lowerright, struct patch* upperleft, struct patch* upperright
    struct patch* currentpatch = mypatch;
   // while(currentpatch->parent != NULL)
   // {  
   // currentpatch = mypatch->parent;  
   // }
   // PetscScalar NumPoints = currentpatch->NumPoints;  
    //PetscScalar NumPoints = mypatch->NumPoints;  
    PetscScalar CoarseResolution = mypatch->CoarseResolution;  
    lowerleft->CoarseResolution = CoarseResolution;
    lowerright->CoarseResolution = CoarseResolution;
    upperleft->CoarseResolution = CoarseResolution;
    upperright->CoarseResolution = CoarseResolution;
    PetscScalar xmidpoint = .5*(mypatch->interiorLLxcoord + mypatch ->interiorURxcoord);
    PetscScalar ymidpoint = .5*(mypatch -> interiorLLycoord + mypatch -> interiorURycoord);
    
    lowerleft ->interiorLLxcoord = mypatch -> interiorLLxcoord;
    lowerleft ->interiorLLycoord = mypatch -> interiorLLycoord;
    lowerleft ->interiorURxcoord = xmidpoint;
    lowerleft ->interiorURycoord = ymidpoint;
    lowerleft ->LLxcoord = PetscMax(lowerleft->interiorLLxcoord-buffersize*(lowerleft->interiorURxcoord-lowerleft->interiorLLxcoord), -1);
    lowerleft ->LLycoord = PetscMax(lowerleft->interiorLLycoord-buffersize*(lowerleft->interiorURycoord-lowerleft->interiorLLycoord), -1);//Lowerleft patch touches lowerleft yboundary
    lowerleft ->URxcoord = xmidpoint+buffersize*(lowerleft->interiorURxcoord-lowerleft->interiorLLxcoord);
    lowerleft ->URycoord = ymidpoint+buffersize*(lowerleft->interiorURycoord-lowerleft->interiorLLycoord);
    lowerleft -> parent = mypatch;
    lowerleft -> forcing = mypatch -> forcing;
    lowerleft -> NumPoints = (int)PetscCeilReal(CoarseResolution*(1+buffersize));
    mypatch -> lowerleft = lowerleft;
    lowerleft->lowerleft = NULL;
    lowerleft->lowerright = NULL;
    lowerleft->upperleft = NULL;
    lowerleft->upperright = NULL;
    lowerleft->north = mypatch->north;
    lowerleft->south = mypatch->south;
    lowerleft->east =  mypatch->east;
    lowerleft->west =  mypatch->west;
    
    lowerright ->interiorLLxcoord = xmidpoint;
    lowerright ->interiorLLycoord = mypatch -> interiorLLycoord;
    lowerright ->interiorURxcoord = mypatch -> interiorURxcoord;
    lowerright ->interiorURycoord = ymidpoint;
    lowerright ->LLxcoord = xmidpoint-buffersize*(lowerright->interiorURxcoord-lowerright->interiorLLxcoord); 
    lowerright ->LLycoord = PetscMax(lowerright->interiorLLycoord-buffersize*(lowerright->interiorURycoord-lowerright->interiorLLycoord), -1);//LowerRight patch touches lowerleft yboundary
    lowerright ->URxcoord = PetscMin(lowerright->interiorURxcoord+buffersize*(lowerright->interiorURxcoord-lowerright->interiorLLxcoord), 1); //Lowerright touches upperright xboundary
    lowerright ->URycoord = ymidpoint+buffersize*(lowerright->interiorURycoord-lowerright->interiorLLycoord);
    lowerright -> parent = mypatch;
    lowerright -> forcing = mypatch -> forcing;
    lowerright-> NumPoints = (int)PetscCeilReal(CoarseResolution*(1+buffersize));
    mypatch -> lowerright = lowerright;
    lowerright->lowerleft = NULL;
    lowerright->lowerright = NULL;
    lowerright->upperleft = NULL;
    lowerright->upperright = NULL;
    lowerright->north = mypatch->north;
    lowerright->south = mypatch->south;
    lowerright->east =  mypatch->east;
    lowerright->west =  mypatch->west;
  
  
    upperright ->interiorLLxcoord =  xmidpoint;
    upperright ->interiorLLycoord = ymidpoint;
    upperright ->interiorURxcoord = mypatch -> interiorURxcoord;
    upperright ->interiorURycoord = mypatch -> interiorURycoord;
    upperright ->LLxcoord = xmidpoint-buffersize*(upperright->interiorURxcoord-upperright->interiorLLxcoord);
    upperright ->LLycoord = ymidpoint-buffersize*(upperright->interiorURycoord-upperright->interiorLLycoord);
    upperright ->URxcoord = PetscMin(upperright -> interiorURxcoord+buffersize*(upperright->interiorURxcoord-upperright->interiorLLxcoord),1);
    upperright ->URycoord = PetscMin(upperright -> interiorURycoord+buffersize*(upperright->interiorURycoord-upperright->interiorLLycoord),1) ;
    //upperright ->URycoord = upperright -> interiorURycoord;//+buffersize*(upperright->interiorURycoord-upperright->interiorLLycoord) ;
    //upperright ->URycoord = upperright -> interiorURycoord+buffersize*(upperright->interiorURycoord-upperright->interiorLLycoord) ;
    //upperright ->URycoord = PetscMin(upperright -> interiorURycoord+buffersize*(upperright->interiorURycoord-upperright->interiorLLycoord),1) ;
    upperright -> parent = mypatch;
    upperright -> forcing = mypatch -> forcing;
    upperright-> NumPoints = (int)PetscCeilReal(CoarseResolution*(1+buffersize));
    mypatch -> upperright = upperright;
    upperright->lowerleft = NULL;
    upperright->lowerright = NULL;
    upperright->upperleft = NULL;
    upperright->upperright = NULL;
    upperright->north = mypatch->north;
    upperright->south = mypatch->south;
    upperright->east =  mypatch->east;
    upperright->west =  mypatch->west;
  
  
  
  
    upperleft ->interiorLLxcoord =  mypatch -> interiorLLxcoord;
    upperleft ->interiorLLycoord = ymidpoint;
    upperleft ->interiorURxcoord = xmidpoint;
    upperleft ->interiorURycoord = mypatch -> interiorURycoord;
    upperleft ->LLxcoord =  PetscMax(upperleft -> interiorLLxcoord-buffersize*(upperleft->interiorURxcoord-upperleft->interiorLLxcoord),-1);
    upperleft ->LLycoord = ymidpoint-buffersize*(upperleft->interiorURycoord-upperleft->interiorLLycoord);
    upperleft ->URxcoord = xmidpoint+buffersize*(upperleft->interiorURxcoord-upperleft->interiorLLxcoord);
    upperleft ->URycoord = PetscMin(upperleft -> interiorURycoord + buffersize*(upperleft->interiorURycoord-upperleft->interiorLLycoord), 1);
    upperleft -> parent = mypatch;
    upperleft -> forcing = mypatch -> forcing;
    upperleft -> NumPoints = mypatch -> NumPoints;
    upperleft-> NumPoints = (int)PetscCeilReal(CoarseResolution*(1+buffersize));
    mypatch -> upperleft = upperleft;
    upperleft->lowerleft = NULL;
    upperleft->lowerright = NULL;
    upperleft->upperleft = NULL;
    upperleft->upperright = NULL;
    upperleft->north = mypatch->north;
    upperleft->south = mypatch->south;
    upperleft->east =  mypatch->east;
    upperleft->west =  mypatch->west;
  
  
  
    ConstructFourier(mypatch -> lowerleft);
    ConstructFourier(mypatch -> lowerright);
    ConstructFourier(mypatch -> upperleft);
    ConstructFourier(mypatch -> upperright);
    SolveCoeff(mypatch -> lowerleft, ksp);
    SolveCoeff(mypatch -> lowerright, ksp);
    SolveCoeff(mypatch -> upperleft, ksp);
    SolveCoeff(mypatch -> upperright, ksp);
    
  
    PetscFunctionReturn(0);

}




PetscErrorCode FreePatches(struct patch* mypatch)
{
    if(mypatch != NULL){ 
    struct patch* lowerleft = mypatch->lowerleft; 
    struct patch* lowerright = mypatch->lowerright; 
    struct patch* upperright = mypatch->upperright; 
    struct patch* upperleft = mypatch -> upperleft;
  
    VecDestroy(&mypatch->coefficients);
    VecDestroy(&mypatch->Northcoefficients);
    VecDestroy(&mypatch->Southcoefficients);
    VecDestroy(&mypatch->Eastcoefficients);
    VecDestroy(&mypatch->Westcoefficients);
    free(mypatch);
    mypatch = NULL;
    
    FreePatches(lowerleft);
    FreePatches(lowerright);
    FreePatches(upperleft);
    FreePatches(upperright);
    }

PetscFunctionReturn(0);
}









PetscErrorCode FourierBoundaryCoeff(Vec East, Vec West, Vec North, Vec South, struct patch* mypatch) 
{
fftw_plan p;

PetscInt NumPoints = mypatch -> NumPoints;


double *in = (double*) fftw_malloc(sizeof(double) * NumPoints);
double *out = (double*) fftw_malloc(sizeof(double) * NumPoints);

PetscScalar Eastx = mypatch -> URxcoord;
PetscScalar Westx = mypatch -> LLxcoord;
PetscScalar Northy = mypatch -> URycoord;
PetscScalar Southy = mypatch -> LLycoord;

	VecGetArray(East, &out);
	//Touches East boundary
	if(Eastx == 1){
	    for(int i=0; i<NumPoints; ++i){
	    in[i] = mypatch->east(Southy + (i+1)*(Northy-Southy)/(NumPoints+1));
	    } 
	}
	//Does not touch East
	else{ 
	    for(int i=0; i<NumPoints; ++i){
	    in[i] = PatchSolution(mypatch->parent, Eastx, Southy + (i+1)*(Northy-Southy)/(NumPoints+1));
	    //in[i] = test(Eastx, Southy + (i+1)*(Northy-Southy)/(NumPoints+1));
	    } 
	}
        SineCoeff(in, out, NumPoints);
	VecRestoreArray(East, &out);

	VecGetArray(West, &out);
	//Touches West boundary
	if(Westx == -1){
	    for(int i=0; i<NumPoints; ++i){
	    in[i] = mypatch->west(Southy + (i+1)*(Northy-Southy)/(NumPoints+1));
	    } 
	}
	//Does not touch West
	else{ 
	    for(int i=0; i<NumPoints; ++i){
	    in[i] = PatchSolution(mypatch->parent, Westx, Southy + (i+1)*(Northy-Southy)/(NumPoints+1));
	    //in[i] = test( Westx, Southy + (i+1)*(Northy-Southy)/(NumPoints+1));
	    } 
	}
	SineCoeff(in, out, NumPoints);
	VecRestoreArray(West, &out);

	VecGetArray(North, &out);
	//Touches North boundary
	if(Northy == 1){
	    for(int i=0; i<NumPoints; ++i){
	    in[i] = mypatch->north(Westx+(i+1)*(Eastx-Westx)/(NumPoints+1));
	    } 
	}
	//Does not touch North
	else{ 
	    for(int i=0; i<NumPoints; ++i){
	    in[i] = PatchSolution(mypatch->parent, Westx+(i+1)*(Eastx-Westx)/(NumPoints+1), Northy);
	    //in[i] = test(Westx+(i+1)*(Eastx-Westx)/(NumPoints+1), Northy);
	    } 
	}
	SineCoeff(in, out, NumPoints);
	VecRestoreArray(North, &out);

	VecGetArray(South, &out);
	//Touches South boundary
	if(Southy == -1){
	    for(int i=0; i<NumPoints; ++i){
	    in[i] = mypatch->south(Westx+(i+1)*(Eastx-Westx)/(NumPoints+1));
	    } 
	}
	//Does not touch South
	else{ 
	    for(int i=0; i<NumPoints; ++i){
	    in[i] = PatchSolution(mypatch->parent, Westx+(i+1)*(Eastx-Westx)/(NumPoints+1), Southy);
	    //in[i] =test(Westx+(i+1)*(Eastx-Westx)/(NumPoints+1), Southy);
	    } 
	}
	SineCoeff(in, out, NumPoints);
	VecRestoreArray(South, &out);

    fftw_free(in);
    fftw_free(out);
}



//in is a filled array (theoretically generated from some function = in_function(x))
//out is an empty array to be filled with integral(in_function, sin([n_pi_x]) from 0 to 1
//e.g. out is the coefficients that approximate 2*sum_from_1_to_NumCoeff(out[n]*sin[n*pi*x]) = in_function(x)
//IMPORTANT NOTE: these coefficients are sine coefficients divided by 2 because they correspond to an integral against sine
PetscErrorCode SineCoeff(double *in, double *out, int NumCoeff)
{
fftw_plan p;
p = fftw_plan_r2r_1d(NumCoeff, in, out, FFTW_RODFT00, FFTW_ESTIMATE);
fftw_execute(p); /* repeat as needed */
for (int i = 0; i < NumCoeff; ++i ) {
        out[i] *= (1.0)/(2*(NumCoeff+1));
	}

fftw_destroy_plan(p);
PetscFunctionReturn(0);
}

int main(int argc, char **args)
{

    //PetscScalar buffersize=0.25;
    PetscScalar buffersize=0.0;
    PetscScalar eps = 10e-7;
    PetscScalar tol = 10e-4;
    PetscInt CoarseResolution = 5;
    PetscInt L = 1;
    KSP ksp;
    PC pc;
    Mat derivmat;
    PetscLogEvent FreeingPatches, TestingPatches, SRMG, coarsesolve;
    PetscErrorCode ierr; 

    PetscInitialize(&argc,&args,(char*)0,help);
    
    ierr = PetscOptionsGetInt(NULL,NULL,"-N",&CoarseResolution,NULL);CHKERRQ(ierr);
    ierr = PetscOptionsGetInt(NULL,NULL,"-L",&L,NULL);CHKERRQ(ierr);
    ierr = PetscOptionsGetScalar(NULL,NULL,"-buffer",&buffersize,NULL);CHKERRQ(ierr);
    struct patch* coarse = malloc(sizeof(struct patch));
    
    coarse->parent = NULL;
    coarse->interiorLLxcoord = -1;
    coarse->interiorLLycoord = -1;
    coarse->interiorURxcoord = 1;
    coarse->interiorURycoord = 1;
    coarse->LLxcoord = -1;
    coarse->LLycoord = -1;
    coarse->URxcoord = 1;
    coarse->URycoord = 1;
    coarse->north = &northboundary;
    coarse->south = &southboundary;
    coarse->east = &eastboundary;
    coarse->west = &westboundary;
    coarse->northsecondder = &northboundarysecondder;
    coarse->southsecondder = &southboundarysecondder;
    coarse->eastsecondder = &eastboundarysecondder;
    coarse->westsecondder = &westboundarysecondder;
    coarse->forcing = &forcing;
    coarse->NumPoints = CoarseResolution;
    coarse->CoarseResolution = CoarseResolution;
    coarse->lowerleft = NULL;
    coarse->lowerright = NULL;
    coarse->upperleft = NULL;
    coarse->upperright = NULL;
       

    PetscLogEventRegister("coarsesolve",0,&coarsesolve);
    PetscLogEventBegin(coarsesolve,0,0,0,0);

    KSPCreate(PETSC_COMM_WORLD,&ksp);
    ierr = MatCreateSeqDense(PETSC_COMM_WORLD, (coarse->NumPoints-2)*(coarse->NumPoints-2), (coarse->NumPoints-2)*(coarse->NumPoints-2), NULL, &derivmat);CHKERRQ(ierr);
    ierr = FormInteriorChebMatrix(derivmat, coarse);CHKERRQ(ierr);
    ierr = MatAssemblyBegin(derivmat,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(derivmat,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    
    
    ierr = KSPSetOperators(ksp,derivmat,derivmat); CHKERRQ(ierr);
    ierr = KSPGetPC(ksp,&pc); CHKERRQ(ierr);
    ierr = PCSetType(pc,PCLU); CHKERRQ(ierr);
    ierr = KSPSetTolerances(ksp,1.e-18,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT);CHKERRQ(ierr);
    
    SolveCoeff(coarse, ksp);
    ConstructFourier(coarse);

    PetscLogEventEnd(coarsesolve,0,0,0,0);


    
    
    PetscLogEventRegister("SRMG",0,&SRMG);
    PetscLogEventBegin(SRMG,0,0,0,0);
    SegmentalRefinement(coarse, L, ksp, buffersize);
    PetscLogEventEnd(SRMG,0,0,0,0);


    
    int resolution=20;
    PetscInt Num_Quad_Points = 10;
    int curr_level = 0;
    PetscScalar integral; 
    struct patch * currentpatch = coarse; 
    while(curr_level <= L)
    {
    PatchError(currentpatch, test, Num_Quad_Points, &integral);
    printf("The integral of the error function at level %d is: %f\n", curr_level, integral);
    PatchErrorInterior(currentpatch, test, Num_Quad_Points, &integral);
    printf("The integral of the interior error function at level %d is: %f\n", curr_level, integral);
    int sizeoflevel=sizeof(curr_level)*(sizeof(char)/sizeof(int)+1);//get size of level digit
    char *plotname = malloc(sizeof(char)*(strlen("heatmapofpatch_") +sizeoflevel+ strlen(".png")));
    sprintf(plotname, "heatmapofpatch_%d.png", curr_level);
    PlotPatch(currentpatch, resolution, plotname);  
    printf("filename %s \n", plotname);
    char *errorplotname = malloc(sizeof(char)*(strlen("heatmapoferror_") +sizeoflevel+ strlen(".png")));
    sprintf(errorplotname, "heatmapoferror_%d.png", curr_level);
    PlotError(currentpatch, resolution, errorplotname, test);  
    char *resplotname = malloc(sizeof(char)*(strlen("heatmapofres_") +sizeoflevel+ strlen(".png")));
    sprintf(resplotname, "heatmapofres_%d.png", curr_level);
    PlotRes(currentpatch, resolution, resplotname);  
    printf("filename %s \n", resplotname);
    currentpatch = currentpatch->lowerleft;
    curr_level = curr_level + 1;
    }
    
    /*struct patch* mypatch = coarse -> lowerleft -> upperright;
    PlotPatch(mypatch, resolution, "LLURmapfourier.png");
    PlotError(mypatch, resolution, "interiormaperrorfourier.png", test);
    PlotPatch(mypatch->parent, resolution, "LLURmapfourierparent.png");
    PlotError(mypatch->parent, resolution, "interiormaperrorfourierparent.png", test);
    PlotPatch(coarse, resolution, "coarsemapfourier.png");
    PlotError(coarse, resolution, "coarsemaperrorfourier.png", test);
    */
    //PatchErrorinRegion(coarse, test, Num_Quad_Points, &integral, -1, 0, -1, 0);
    //printf("The integral of the error function at lowerleft section coarse is: %f\n",  integral);
    //PatchErrorinRegion(coarse->lowerleft, test, Num_Quad_Points, &integral, -1, 0, -1, 0);
    //printf("The integral of the error function at lowerleft is: %f\n", integral);
    /*printf("buffersize: %f, N_0: %d \n", buffersize, CoarseResolution);
    for(PetscInt depth_level=0; depth_level <= L; depth_level++)
    {
    PetscScalar total_integral = 0;
    TotalError(coarse, test, Num_Quad_Points, &total_integral, 0, depth_level);
    printf("Total integral at level %d is %f \n", depth_level, total_integral );
    PetscScalar total_res = 0;
    TotalRes(coarse, Num_Quad_Points, &total_res, 0, depth_level);
    printf("Total integral of res at level %d is %f \n", depth_level, total_res );
    }*/
    
    //ierr = VecView(coarse->Westcoefficients, PETSC_VIEWER_STDOUT_SELF);
    //struct patch* mypatch = coarse -> lowerleft -> lowerleft;
    struct patch* mypatch = coarse -> lowerleft -> lowerleft;
    printf("Doing stuff for small \n");
    PlotPatch(mypatch, resolution, "LLLLmapfourier.png");
    PlotError(mypatch, resolution, "smallmaperrorfourier.png", test);
    PlotRes(mypatch, resolution, "smallmapresfourier.png");
    //ierr = VecView(mypatch->Eastcoefficients, PETSC_VIEWER_STDOUT_SELF);
    
    printf("Doing stuff for medium \n");
    mypatch = coarse -> lowerleft -> upperleft;
    PlotPatch(mypatch, resolution, "LLULmapfourier.png");
    PlotError(mypatch, resolution, "mediummaperrorfourier.png", test);
    PlotRes(mypatch, resolution, "mediummapresfourier.png");
    //ierr = VecView(mypatch->Eastcoefficients, PETSC_VIEWER_STDOUT_SELF);
    
    printf("Doing stuff for large \n");
    //mypatch = coarse -> lowerleft -> lowerright;
    mypatch = coarse -> lowerleft -> upperright;
    PlotPatch(mypatch, resolution, "LLLRmapfourier.png");
    PlotError(mypatch, resolution, "largemaperrorfourier.png", test);
    PlotRes(mypatch, resolution, "largemapresfourier.png");
    //ierr = VecView(mypatch->Eastcoefficients, PETSC_VIEWER_STDOUT_SELF);
   
    //PlotPatch(coarse->lowerleft->upperright, resolution, "interiorRmapfourier.png");
   
    //Very slow code that exhaustively searches every patch for large error
    /*PetscLogEventRegister("TestPatches",0,&TestingPatches);
    PetscLogEventBegin(TestingPatches,0,0,0,0);
    TestPatches(coarse, test, tol, Num_Quad_Points);
    PetscLogEventEnd(TestingPatches,0,0,0,0);*/
    
    PetscLogEventRegister("FreeingPatches",0,&FreeingPatches);
    PetscLogEventBegin(FreeingPatches,0,0,0,0);
    
    FreePatches(coarse);
    PetscLogEventEnd(FreeingPatches,0,0,0,0);
    
    KSPDestroy(&ksp);
    MatDestroy(&derivmat);
    

    
    PetscFinalize();
    return 0;

}



