#include <petscmat.h>
#include <stdio.h>
#include <petscsys.h>
#include <petscdm.h>
#include <petscdmda.h>
#include <petscsnes.h>
#include <petscmatlab.h>
#include <petscdraw.h>
#include <petsctime.h>
#include <petscmat.h>
#include <stdio.h>
#include <petscsys.h>
#include <petscdm.h>
#include <petscdmda.h>
#include <petscsnes.h>
#include <petscmatlab.h>


PetscErrorCode intervalShift(PetscScalar a, PetscScalar b, PetscScalar c, PetscScalar d, PetscScalar *point);
double *Chebpoly(double point, int degN);
double *Chebderiv(double point, int degN, double *chebfunceval);
double *Chebsecondderiv(double point, int degN, double *chebderiv);

struct patch{
	struct patch * parent;
        PetscScalar interiorLLxcoord;
        PetscScalar interiorLLycoord;
	PetscScalar interiorURxcoord;
	PetscScalar interiorURycoord;
	PetscScalar LLxcoord;
	PetscScalar LLycoord;
	PetscScalar URxcoord;
	PetscScalar URycoord;
	PetscScalar (*north)(PetscScalar);
	PetscScalar (*south)(PetscScalar);
	PetscScalar (*east)(PetscScalar);
	PetscScalar (*west)(PetscScalar);
	PetscScalar (*northsecondder)(PetscScalar);
	PetscScalar (*southsecondder)(PetscScalar);
	PetscScalar (*eastsecondder)(PetscScalar);
	PetscScalar (*westsecondder)(PetscScalar);
	PetscScalar (*forcing)(PetscScalar, PetscScalar);
	PetscInt NumPoints;
	PetscInt CoarseResolution;
	Vec coefficients;
	Vec Northcoefficients;
	Vec Southcoefficients;
	Vec Eastcoefficients;
	Vec Westcoefficients;
	PetscScalar (*approx)(PetscScalar, PetscScalar);
	//points to subpatch
	struct patch * lowerleft, * upperleft, * lowerright, * upperright;

};

PetscScalar BoxSoln(struct patch* mypatch, PetscScalar xpoint, PetscScalar ypoint);
PetscScalar LaplaceBoxSoln(struct patch* mypatch, PetscScalar xpoint, PetscScalar ypoint);
PetscErrorCode FormConditions(Vec conditions, struct patch* mypatch);
PetscErrorCode FormInteriorChebMatrix(Mat A, struct patch* mypatch);
PetscErrorCode SolveCoeff(struct patch* mypatch, KSP ksp);
PetscScalar PatchSolution(struct patch* mypatch, PetscScalar xpoint, PetscScalar ypoint);
PetscScalar PatchLaplace(struct patch* mypatch, PetscScalar xpoint, PetscScalar ypoint);
PetscErrorCode constructSubPatches(struct patch* mypatch, KSP ksp, PetscScalar buffersize);
PetscScalar test(PetscScalar x, PetscScalar y);
PetscScalar northboundary(PetscScalar x);
PetscScalar southboundary(PetscScalar x);
PetscScalar eastboundary(PetscScalar y);
PetscScalar westboundary(PetscScalar y);
PetscScalar westboundarysecondder(PetscScalar y);
PetscScalar northboundarysecondder(PetscScalar x);
PetscScalar southboundarysecondder(PetscScalar x);
PetscScalar eastboundarysecondder(PetscScalar y);
PetscScalar forcing(PetscScalar x, PetscScalar y);
PetscErrorCode PlotPatch(struct patch* mypatch, int resolution, char *FILENAME);
PetscErrorCode PlotPatchPortion(struct patch* mypatch, PetscScalar xmin, PetscScalar xmax, PetscScalar ymin, PetscScalar ymax, int resolution, char *FILENAME);
PetscErrorCode PlotError(struct patch* mypatch, int resolution, char *FILENAME,PetscScalar(*test)(PetscScalar, PetscScalar)); 
PetscErrorCode PatchErrorinRegion(struct patch *mypatch, PetscScalar test(PetscScalar, PetscScalar), PetscInt Num_Quad_Points, PetscScalar* integral, PetscScalar xmin,  PetscScalar xmax, PetscScalar ymin, PetscScalar ymax);
PetscErrorCode PatchErrorInterior(struct patch *mypatch, PetscScalar test(PetscScalar, PetscScalar), PetscInt Num_Quad_Points, PetscScalar* integral);
PetscErrorCode PatchResinRegion(struct patch *mypatch, PetscInt Num_Quad_Points, PetscScalar* integral, PetscScalar xmin,  PetscScalar xmax, PetscScalar ymin, PetscScalar ymax);
PetscErrorCode PatchResInterior(struct patch *mypatch, PetscInt Num_Quad_Points, PetscScalar* integral);
PetscErrorCode PlotErrorPortion(struct patch* mypatch, PetscScalar xmin, PetscScalar xmax, PetscScalar ymin, PetscScalar ymax, int resolution, char *FILENAME, PetscScalar(*test)(PetscScalar, PetscScalar)); 
PetscErrorCode SineCoeff(double *in, double *out, int NumCoeff);
PetscErrorCode TestPatches(struct patch* mypatch,  PetscScalar test(PetscScalar, PetscScalar), PetscScalar tol, PetscInt Num_Quad_Points);
PetscErrorCode TotalError(struct patch *mypatch, PetscScalar test(PetscScalar, PetscScalar), PetscInt Num_Quad_Points, PetscScalar* integral, PetscInt curr_level, PetscInt total_level);
PetscErrorCode RefinedFourierBoundaryCoeff(PetscInt NumCoeff, struct patch* mypatch);
PetscErrorCode PassCoeff(struct patch* parentpatch, struct patch* child);
PetscErrorCode TotalRes(struct patch *mypatch, PetscInt Num_Quad_Points, PetscScalar* integral, PetscInt curr_level, PetscInt total_level);
