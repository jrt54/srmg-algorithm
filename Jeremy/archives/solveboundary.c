static char help[]="This program solves a PDE on a square domain with given boundary values";

#include <petscmat.h>
#include <stdio.h>
#include <petscsys.h>
#include <petscdm.h>
#include <petscdmda.h>
#include <petscsnes.h>
#include <petscmatlab.h>
#include <fftw3.h>



typedef struct {
  PetscReal param;          /* test problem parameter */
  PetscInt  mPar;           /* MMS3 m parameter */
  PetscInt  nPar;           /* MMS3 n parameter */
} AppCtx;


  //boundary occurs at the top
  PetscErrorCode FormBoundary1(Vec input, PetscInt N)
  {
    PetscErrorCode ierr;
    PetscScalar    v;
    PetscInt j;

    for (j = 0; j < N; j++)
    {
      //boundary function is Sin(2*pi*x)*Sinh(2*pi)
      v = PetscSinReal(2*PETSC_PI*(j)/N)*PetscSinhReal(2*PETSC_PI);
      ierr = VecSetValues(input, 1, &j, &v, INSERT_VALUES);CHKERRQ(ierr);    
    }

    
 
      PetscFunctionReturn(0);
}

//input is the sample of points, y is the output of the fourier transform
PetscErrorCode BuildFourier(Vec input, Vec *y, PetscInt N)
{
  PetscScalar *in, *out;
  PetscInt    DIM, dim[2];
  Mat            A;
  dim[0] = N;
  DIM = 1;
  PetscErrorCode ierr;
  Vec x, z;

  PetscFunctionBegin;
  ierr = VecSetType(input, VECSEQ); CHKERRQ(ierr);
  ierr = VecSetType(*y, VECSEQ); CHKERRQ(ierr);
  ierr = VecGetArray(input,&in);CHKERRQ(ierr);
  fftw_plan p, pinv;

  //fftw output returns integral of function times sin(j*pi*x)*(N+1)
  out = (double*) fftw_malloc(sizeof(PetscScalar) * N);
  p = fftw_plan_r2r_1d(N, in, out, FFTW_RODFT00, FFTW_ESTIMATE);

  // ierr   = MatCreateFFT(PETSC_COMM_WORLD,DIM,dim,MATFFTW,&A);CHKERRQ(ierr);
  // ierr   = MatCreateVecsFFTW(A,&x,y,&z);CHKERRQ(ierr);

  //VecView(x, PETSC_VIEWER_STDOUT_WORLD);
  //MatView(A, PETSC_VIEWER_ASCII_INFO);
  //ierr = MatMult(A,x,*y);CHKERRQ(ierr);
  VecRestoreArray(&in, input);
  VecRestoreArray(*out, y);
  ierr = VecScale(*y,1.0/(N+1));CHKERRQ(ierr); 
  PetscFunctionReturn(0);
}




#undef __FUNCT__
#define __FUNCT__ "FormExactSolution1"

PetscErrorCode FormExactSolution1(DM da, AppCtx *user, Vec U)
{
  DM             coordDA;
  Vec            coordinates;
  DMDACoor2d   **coords;
  PetscScalar  **u;
  PetscReal      x, y;
  PetscInt       xs, ys, xm, ym, i, j;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = DMDAGetCorners(da, &xs, &ys, NULL, &xm, &ym, NULL);CHKERRQ(ierr);
  ierr = DMGetCoordinateDM(da, &coordDA);CHKERRQ(ierr);
  ierr = DMGetCoordinates(da, &coordinates);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da, U, &u);CHKERRQ(ierr);
  for (j = ys; j < ys+ym; ++j) {
    for (i = xs; i < xs+xm; ++i) {
      x = PetscRealPart(coords[j][i].x);
      y = PetscRealPart(coords[j][i].y);
      u[j][i] = PetscSinReal(2*PETSC_PI*x)*PetscSinhReal(2*PETSC_PI*y);
    }
  }
  ierr = DMDAVecRestoreArray(da, U, &u);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

//input is the DM, and data points U
//output is a vector HalfBdry encoding the points along the horizontal line bisecting the grid
PetscErrorCode GetHalfBdry(DM da, Vec U, Vec HalfBdry)
{
  DM             coordDA;
  Vec            coordinates;
  DMDACoor2d   **coords;
  PetscScalar  **u, *halfbdry;
  PetscReal      x, y;
  PetscInt       xs, ys, xm, ym, i, j, halfvert;
  PetscErrorCode ierr;

  ierr = DMDAGetCorners(da, &xs, &ys, NULL, &xm, &ym, NULL);CHKERRQ(ierr);
  ierr = DMGetCoordinateDM(da, &coordDA);CHKERRQ(ierr);
  ierr = DMGetCoordinates(da, &coordinates);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da, U, &u);CHKERRQ(ierr);
 
  ierr = VecGetArrayRead(HalfBdry, &halfbdry);CHKERRQ(ierr);
  for (i = xs; i< xs+xm-1; ++i){
    halfbdry[i-xs] = u[ys+ ym/2][i];

  }

  ierr = VecRestoreArray(HalfBdry, &halfbdry);CHKERRQ(ierr);

  ierr = DMDAVecRestoreArray(da, U, &u);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

//H is height
//L is width
PetscErrorCode FormTopSol(DM da, AppCtx *user, Vec U, Vec FourierTop, PetscReal H, PetscReal L)
{
  DM             coordDA;
  Vec            coordinates;
  DMDACoor2d   **coords;
  PetscScalar  **u;
  const PetscScalar *fourierspace;
  PetscReal      x, y;
  PetscInt       xs, ys, xm, ym, i, j, k, size;
  PetscErrorCode ierr;

  ierr = VecGetSize(FourierTop,  &size);
  ierr = VecGetArrayRead(FourierTop, &fourierspace);CHKERRQ(ierr);
  //getarray from FourierTop

  ierr = DMDAGetCorners(da, &xs, &ys, NULL, &xm, &ym, NULL);CHKERRQ(ierr);
  ierr = DMGetCoordinateDM(da, &coordDA);CHKERRQ(ierr);
  ierr = DMGetCoordinates(da, &coordinates);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da, U, &u);CHKERRQ(ierr);
  for (j = ys; j < ys+ym; ++j) {
    for (i = xs; i < xs+xm; ++i) {
      x = PetscRealPart(coords[j][i].x);
      y = PetscRealPart(coords[j][i].y);
      u[j][i] = 0;
      //k = 1 corresponds to sin(0) motion, k = 2 corresponds to cos(0) motion, k = 3 corresponds to the first sine motion.
      for (k = 3; k <size; k+=1) {
        //go through all fourier coeffs
        if(k%2==1)
          //iterate through the FFT, which gives sin(2*pi*n) motions. Divide by 2 because we're skipping the Cos motions. 
          u[j][i] += (-2*fourierspace[k] * (PetscSinReal(PETSC_PI*(k-1)/L*x)*PetscSinhReal(PETSC_PI*(k-1)*y/L)/(L*PetscSinhReal(PETSC_PI*(k-1)*H/L))));
      }
    }
  }
  ierr = DMDAVecRestoreArray(da, U, &u);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(coordDA, coordinates, &coords);CHKERRQ(ierr);
  //ierr = VecRestoreArray() with fourier fix
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "main"

int main(int argc,char **args)
{
  Vec            boundary, fourier, error, errorhalf, exact, highres, exacthalf, HalfBdry, HalfFourier;                            /* solution vector */
  AppCtx         user;                         /* user-defined work context */
  PetscErrorCode ierr;
  PetscReal      norm, halfnorm;
  PetscInt       N = 13;
  DM             da, halfda, highresda;

  ierr = PetscInitialize(&argc,&args,(char*)0,help);if (ierr) return ierr;

  ierr       = PetscOptionsGetInt(NULL,NULL,"-N",&N,NULL);CHKERRQ(ierr);


  //create boundary condition on whole square
  ierr = VecCreate(PETSC_COMM_WORLD,&boundary);CHKERRQ(ierr);
  ierr = VecSetSizes(boundary,PETSC_DECIDE,N);CHKERRQ(ierr);
  ierr = VecSetFromOptions(boundary);CHKERRQ(ierr);
  ierr = FormBoundary1(boundary, N);CHKERRQ(ierr);
  
    //VecView(boundary, PETSC_VIEWER_STDOUT_WORLD);

  //declare and build fourier coefficients on boundary
  ierr = VecCreate(PETSC_COMM_WORLD,&fourier);CHKERRQ(ierr);
  ierr = VecSetSizes(fourier,PETSC_DECIDE,2*N);CHKERRQ(ierr);
  ierr= BuildFourier(boundary, &fourier, N);CHKERRQ(ierr);



  //form exact solution 
  ierr = DMDACreate2d(PETSC_COMM_WORLD, DM_BOUNDARY_NONE, DM_BOUNDARY_NONE,DMDA_STENCIL_STAR,-(N+1),-(N+1),PETSC_DECIDE,PETSC_DECIDE,1,1,NULL,NULL,&da);CHKERRQ(ierr);
  ierr = DMDASetUniformCoordinates(da, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0);CHKERRQ(ierr);
  ierr = DMSetApplicationContext(da,&user);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(da,&exact);CHKERRQ(ierr);
  ierr = FormExactSolution1(da, &user, exact);CHKERRQ(ierr);

    //VecView(exact, PETSC_VIEWER_STDOUT_WORLD);

  //form approximate solution from fourier on boundary, store it in error vector
  ierr = DMCreateGlobalVector(da,&error);CHKERRQ(ierr);
  ierr = FormTopSol(da, &user, error, fourier, 1.0, 1.0);CHKERRQ(ierr);

  //get highres created from original fourier
  ierr = DMDACreate2d(PETSC_COMM_WORLD, DM_BOUNDARY_NONE, DM_BOUNDARY_NONE,DMDA_STENCIL_STAR,-(2*N+1),-(2*N+1),PETSC_DECIDE,PETSC_DECIDE,1,1,NULL,NULL,&highresda);CHKERRQ(ierr);
  ierr = DMDASetUniformCoordinates(highresda, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0);CHKERRQ(ierr);
  ierr = DMSetApplicationContext(highresda,&user);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(highresda,&highres);CHKERRQ(ierr);
  ierr = FormTopSol(highresda, &user, highres, fourier, 1.0, 1.0);CHKERRQ(ierr);
  //ierr = VecView(highres, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);

  //VecView(error, PETSC_VIEWER_STDOUT_WORLD);
  ierr = VecCreate(PETSC_COMM_WORLD,&HalfBdry);CHKERRQ(ierr);
  ierr = VecSetSizes(HalfBdry,PETSC_DECIDE,2*N);CHKERRQ(ierr);
  ierr = VecSetFromOptions(HalfBdry);CHKERRQ(ierr);

  //need to fix this duplicate later, might have higher nodes
  ierr = VecDuplicate(fourier, &HalfFourier); CHKERRQ(ierr);
  
  ierr = GetHalfBdry(highresda, highres, HalfBdry);CHKERRQ(ierr);
  
  ierr = BuildFourier(HalfBdry, &HalfFourier, 2*N);CHKERRQ(ierr);

  


     //viewvecs

  //ierr = VecView(boundary, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  //ierr = VecView(fourier, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  //ierr = VecView(HalfFourier, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  //ierr = VecView(HalfBdry, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);


  //make half solution
  ierr = DMDACreate2d(PETSC_COMM_WORLD, DM_BOUNDARY_NONE, DM_BOUNDARY_NONE,DMDA_STENCIL_STAR,-(N+1),-(N+1),PETSC_DECIDE,PETSC_DECIDE,1,1,NULL,NULL,&halfda);CHKERRQ(ierr);
  ierr = DMDASetUniformCoordinates(halfda, 0.0, 1.0, 0.0, .5, 0.0, 1.0);CHKERRQ(ierr);
  ierr = DMSetApplicationContext(halfda,&user);CHKERRQ(ierr);


  ierr = DMCreateGlobalVector(halfda,&exacthalf);CHKERRQ(ierr);
  ierr = FormExactSolution1(halfda, &user, exacthalf);CHKERRQ(ierr);

  ierr = DMCreateGlobalVector(halfda,&errorhalf);CHKERRQ(ierr);
  ierr = FormTopSol(halfda, &user, errorhalf, HalfFourier, .5, 1.0);CHKERRQ(ierr);


  //whole grid error
  ierr = VecAXPY(error, -1.0, exact);CHKERRQ(ierr);
  ierr = VecNorm(error,NORM_2,&norm);CHKERRQ(ierr);

  ierr = PetscPrintf(PETSC_COMM_WORLD,"Norm of entire vector %g\n",(double)norm);CHKERRQ(ierr);

  //half grid error
  // ierr = VecView(HalfFourier, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  //ierr = VecView(errorhalf, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  ierr = VecAXPY(errorhalf, -1.0, exacthalf);CHKERRQ(ierr);
  //This norm is just the sum of elements squared 
  ierr = VecNorm(errorhalf,NORM_2,&halfnorm);CHKERRQ(ierr);

  ierr = PetscPrintf(PETSC_COMM_WORLD,"Norm of high res vector %g\n",(double)halfnorm);CHKERRQ(ierr);


  // ierr = VecView(exacthalf, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);

  //FormTopSol(DM da, AppCtx *user, Vec U, Vec FourierTop)

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Initialize program
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

return 0;

}
