static char help[]="This program solves a PDE on a square domain with given boundary values";
#include <petscmat.h>
#include <stdio.h>
#include <petscsys.h>

#undef __FUNCT__
#define __FUNCT__ "main"

PetscErrorCode FormTest4(Vec input, Vec exactfun, PetscInt interpolants, PetscInt N)
{
  PetscScalar    v;
for (int j = 0; j < N; j++)
  {

  	v = PetscPowScalar((double)j/N, 2);
  
  	VecSetValues(input, 1, &j, &v, INSERT_VALUES);
  }


  //form exact fun at all interpolants
    
  

  for (int j = 0; j < interpolants; j++)
  {

    v = PetscPowScalar((double)j/interpolants, 2);
  
    VecSetValues(exactfun, 1, &j, &v, INSERT_VALUES);
  }
PetscFunctionReturn(0);
}

int main(int argc, char **args)
{
PetscErrorCode ierr;

  PetscInt       N = 11, interpolants = 1000;
  Vec            x,y,z,output,input, error, exactfun, approxfun;
  Mat            A;
  PetscInt       DIM,dim[2];
  PetscScalar    v;
  PetscReal    norm;

  ierr = PetscInitialize(&argc,&args,(char*)0,help);if (ierr) return ierr;

  
  ierr = VecCreate(PETSC_COMM_WORLD,&input);CHKERRQ(ierr);
  ierr = VecSetSizes(input,PETSC_DECIDE,N);CHKERRQ(ierr);
  VecSetType(input,VECSEQ);

  ierr = VecCreate(PETSC_COMM_WORLD,&exactfun);CHKERRQ(ierr);
  ierr = VecSetSizes(exactfun,PETSC_DECIDE,interpolants);CHKERRQ(ierr);
  VecSetType(exactfun,VECSEQ);
  FormTest4(input, exactfun, interpolants, N);

  VecView(exactfun,PETSC_VIEWER_STDOUT_WORLD);



  return 0;
}
