#! /usr/bin/env python
import os
import subprocess

error = [ ]
samplesize = []
highreserror = []


# for k in range ( 1 ) :
#     Nx = 2**k + 1
#     options = ['-N ', str(Nx)]
#     error.append(os.system('./boundaryvalues '+' '.join(options)))
#     samplesize.append(Nx)
# print zip(error, samplesize)



for k in range ( 5) :
	Nx = 2*k +5
	cmd = './boundaryvalues -N ' + str(Nx)
	out = subprocess.check_output(cmd, shell=True) 
	splitstring = out.split("\n")
	for linenumber, lines in enumerate(splitstring):
		start = (lines.find("vector"))
		if start > 0:
			if linenumber == 0:
				print(float(lines[start + len("vector "):]))
				error.append(float(lines[start + len("vector "):]))
				samplesize.append(Nx)
				print(linenumber, lines)
			if linenumber == 1:
				highreserror.append(float(lines[start + len("vector "):]))

from pylab import legend, plot, loglog, show, title, xlabel, ylabel, savefig
plot(samplesize, error, '--k')
legend(['fft accuracy'])
title('fft')
xlabel('Problem Size $N$')
ylabel('Error (N)')
show()


plot(samplesize, highreserror, '--k')
legend(['fft accuracy'])
title('fft')
xlabel('Problem Size $N$')
ylabel('High Resolution Error (N)')
show()
#loglog(sizes, times)
#title('SNES ex5')
#xlabel('Problem Size $N$')
#ylabel('Time (s)')
#show()


