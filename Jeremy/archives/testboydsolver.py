#! /usr/bin/env python
import os
import subprocess

error = [ ]
samplesize = []
highreserror = []
reserror = []
work = []
compare = []
compareres =[]



# for k in range ( 1 ) :
#     Nx = 2**k + 1
#     options = ['-N ', str(Nx)]
#     error.append(os.system('./boundaryvalues '+' '.join(options)))
#     samplesize.append(Nx)
# print zip(error, samplesize)



for k in range ( 5) :
	Nx = 10*k + 6
	cmd = './chebsolve2d -N ' + str(Nx)
	out = subprocess.check_output(cmd, shell=True) 
	splitstring = out.split("\n")
	for linenumber, lines in enumerate(splitstring):
		start = (lines.find("error vector"))
		if start > 0:
				print(float(lines[start + len("error vector "):]))
				error.append(float(lines[start + len("error vector "):]))
				compare.append(1.0/(Nx)**2)
				samplesize.append(Nx)
				print(linenumber, lines)
#work is approximately (Numpoints^2)^3 b/c we invert N^2xN^2 matrices
		start = (lines.find("res vector"))
		if start > 0:
				print(float(lines[start + len("res vector "):]))
				reserror.append(float(lines[start + len("res vector "):]))
				work.append(Nx**6)
				compareres.append((1.0/(Nx-2)**2))
				print(linenumber, lines)

from pylab import legend, plot, loglog, show, title, xlabel, ylabel, savefig
#plot(samplesize, error, '--k')
loglog(samplesize, error, '--k', samplesize, compare, 'r--')
legend(['cheby accuracy', '1/h^2'])
title('u(x) = (2x^2-2)*(2y^2-2) on patch [-1, 0]x[-1, 0]')
xlabel('Problem Size $N$')
ylabel('Error (N)')
show()

#plot(work, reserror, '--k')
loglog(work, reserror, '--k', samplesize, compareres, 'r--')
legend(['cheby precision vs work', '1/h^2'])
title('u(x) = (2x^2-2)*(2y^2-2) on patch [-1, 0]x[-1, 0]')
xlabel('Problem Size $N$')
ylabel('Error (N)')
show()

# plot(samplesize, highreserror, '--k')
# legend(['fft accuracy'])
# title('fft')
# xlabel('Problem Size $N$')
# ylabel('High Resolution Error (N)')
# show()
#loglog(sizes, times)
#title('SNES ex5')
#xlabel('Problem Size $N$')
#ylabel('Time (s)')
#show()


