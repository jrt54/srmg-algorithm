#include <petscmat.h>
#include <stdio.h>
#include <petscsys.h>
#include <petscdm.h>
#include <petscdmda.h>
#include <petscsnes.h>
#include <petscmatlab.h>
#include <fftw3.h>
#import <petscsys.h>
#import <petscdraw.h>
static char help[] = "Solving Laplace via Chebyshev";


//Map from interval (a, b) to (c, d)
PetscErrorCode intervalShift(PetscScalar a, PetscScalar b, PetscScalar c, PetscScalar d, PetscScalar *point)
{

  *point = (*point - a)/(b-a)*(d-c) + c;
  PetscFunctionReturn(0);
}

double *Chebpoly(double point, int degN)
{       

       
		double *chebfunceval = malloc(sizeof(double) * (degN+1)); 

        for (int i=0; i < degN+1; ++i) {
                if (i == 0)
                {
                	chebfunceval[i] = 1;
                }
                else if (i == 1)
                {
                	chebfunceval[i] = point;
                }
                else
                {
                	chebfunceval[i] = 2*point*chebfunceval[i-1] - chebfunceval[i-2];

        		}
        }

        return chebfunceval;
}

double *Chebderiv(double point, int degN, double *chebfunceval)
{       

       
		double *chebderiv = malloc(sizeof(double) * (degN+1)); 

        for (int i=0; i < degN + 1; ++i) {
                if (i == 0)
                {
                	chebderiv[i] = 0;
                }
                else if (i == 1)
                {
                	chebderiv[i] = 1;
                }
                else if (i==2)
                {
                	chebderiv[i] = 4*point;
                }
                else
                //Tk' = (-k*x*Tk(x) + k*T_(k-1)(x))/(1-x^2)
                {
                	chebderiv[i] = i*(2*chebfunceval[i-1] + chebderiv[i-2]/(i-2));

        		}
        }

        return chebderiv;
}


double *Chebsecondderiv(double point, int degN, double *chebderiv)
{       

       
		double *chebsecondderiv = malloc(sizeof(double) * (degN+1)); 

        for (int i=0; i < degN + 1; ++i) {
                if (i == 0)
                {
                	chebsecondderiv[i] = 0;
                }
                else if (i == 1)
                {
                	chebsecondderiv[i] = 0;
                }
                else if (i==2)
                {
                	chebsecondderiv[i] = 4;
                }
                else
                //Tk'; = (-k*x*Tk;(x) + k*T_(k-1)''(x))/(1-x^2)
                {
                	chebsecondderiv[i] = i*(2*chebderiv[i-1] + chebsecondderiv[i-2]/(i-2));

        		}
        }

        return chebsecondderiv;
}


PetscErrorCode FormInteriorChebMatrix(Mat A, PetscScalar LowerLeftxcord, PetscScalar LowerLeftycord, PetscScalar UpperRightxcord, PetscScalar UpperRightycord, PetscInt NumPoints)
{
PetscErrorCode ierr;
for (PetscInt j = 0; j < NumPoints-2; ++j)
{
    //generate each row
	for (PetscInt i = 0; i < NumPoints-2; ++i)
	{
  PetscInt row = i + j*(NumPoints-2);

	//only taking interior points

  double xpoint = -PetscCosReal(PETSC_PI*(double)(i+1)/(NumPoints - 1));
  double ypoint = -PetscCosReal(PETSC_PI*(double)(j+1)/(NumPoints - 1));

  //We want the laplacian of bases phi(M^-1(x)) where M maps from square to patch
  //Invert this matrix to see what would yield equality at laplace(u) = f(x)
  //So each basis must be e.g. phi_xx(M^-1(x)) * [M^-1]'^2 = 4/(Rightx-Leftx)
  PetscScalar yshiftder =  4/((UpperRightycord-LowerLeftycord)*(UpperRightycord-LowerLeftycord));
  PetscScalar xshiftder =  4/((UpperRightxcord-LowerLeftxcord)*(UpperRightxcord-LowerLeftxcord));
  //PetscPrintf(PETSC_COMM_SELF, "scaling (x, y): %f, %f", xshiftder, yshiftder);

	double *chebtestx = Chebpoly(xpoint, NumPoints);
  double *chebtesty = Chebpoly(ypoint, NumPoints);
	double *chebderivx = Chebderiv(xpoint, NumPoints, chebtestx);
  double *chebderivy = Chebderiv(ypoint, NumPoints, chebtesty);
	double *chebsecondderivx = Chebsecondderiv(xpoint, NumPoints, chebderivx);
	double *chebsecondderivy = Chebsecondderiv(ypoint, NumPoints, chebderivy);
    for (PetscInt jcol = 0; jcol < NumPoints-2; ++jcol)
		{
            for (PetscInt icol = 0; icol < NumPoints-2; ++icol)
            {
                PetscInt col = icol + jcol*(NumPoints-2);
                if ((icol+2)%2 == 0 && (jcol+2)%2 == 0)
          			{
          			//recombination of bases that has homogenous boundaries
          			PetscScalar v = yshiftder*chebsecondderivy[jcol+2]*(chebtestx[icol+2]-1) + xshiftder*chebsecondderivx[icol+2]*(chebtesty[jcol+2]-1);
          			ierr = MatSetValues(A, 1, &row, 1, &col, &v, INSERT_VALUES);CHKERRQ(ierr);
          			}
                if ((icol+2)%2 == 0 && (jcol+2)%2 == 1)
                {
                //recombination of bases that has homogenous boundaries
                PetscScalar v = yshiftder*chebsecondderivy[jcol+2]*(chebtestx[icol+2]-1) + xshiftder*chebsecondderivx[icol+2]*(chebtesty[jcol+2]-ypoint);
                ierr = MatSetValues(A, 1, &row, 1, &col, &v, INSERT_VALUES);CHKERRQ(ierr);
                }
                if ((icol+2)%2 == 1 && (jcol+2)%2 == 0)
                {
                //recombination of bases that has homogenous boundaries
                PetscScalar v = yshiftder*chebsecondderivy[jcol+2]*(chebtestx[icol+2]-xpoint) + xshiftder*chebsecondderivx[icol+2]*(chebtesty[jcol+2]-1);
                ierr = MatSetValues(A, 1, &row, 1, &col, &v, INSERT_VALUES);CHKERRQ(ierr);
                }
                if ((icol+2)%2 == 1 && (jcol+2)%2 == 1)
                {
                //recombination of bases that has homogenous boundaries
                PetscScalar v = yshiftder*chebsecondderivy[jcol+2]*(chebtestx[icol+2]-xpoint) + xshiftder*chebsecondderivx[icol+2]*(chebtesty[jcol+2]-ypoint);
                ierr = MatSetValues(A, 1, &row, 1, &col, &v, INSERT_VALUES);CHKERRQ(ierr);
                }
		    }
	    }
    }
}
	PetscFunctionReturn(0);
}





//cardinal vectors must satisfy equality at adjoined corners
PetscErrorCode FormBox(Vec Box, Vec East, Vec West, Vec North, Vec South, PetscScalar LowerLeftxcord, PetscScalar LowerLeftycord, PetscScalar UpperRightxcord, PetscScalar UpperRightycord, PetscInt NumPoints)
{
PetscErrorCode ierr;
PetscScalar *westarray, *eastarray, *northarray, *southarray;
PetscScalar LowerLeft, LowerRight, UpperLeft, UpperRight;

ierr = VecGetArray(East,&eastarray);CHKERRQ(ierr);
ierr = VecGetArray(West,&westarray);CHKERRQ(ierr);
ierr = VecGetArray(South,&southarray);CHKERRQ(ierr);
ierr = VecGetArray(North,&northarray);CHKERRQ(ierr);
for (PetscInt j = 0; j < NumPoints; ++j)

LowerLeft = westarray[0];
UpperLeft = northarray[0];
UpperRight = eastarray[NumPoints-1];
LowerRight = southarray[NumPoints-1];

//printf("Corners are %f %f %f %f", LowerLeft, LowerRight, UpperLeft, UpperRight);

for(PetscInt j = 0; j < NumPoints; ++j)
{
    //generate each row
  for (PetscInt i = 0; i < NumPoints; ++i)
  {
  PetscInt row = i + j*(NumPoints);

  //only taking interior points


  // double xpoint = -PetscCosReal(PETSC_PI*(double)i/(NumPoints - 1));
  // double ypoint =-PetscCosReal(PETSC_PI*(double)j/(NumPoints - 1));

  double xpoint = 2*(double)(i)/(NumPoints - 1)-1;
  double ypoint = 2*(double)(j)/(NumPoints - 1)-1;

  // ierr = intervalShift(-1,  1, LowerLeftxcord, UpperRightxcord,  &xpoint);CHKERRQ(ierr);
  // ierr = intervalShift(-1,  1, LowerLeftycord, UpperRightycord,  &ypoint);CHKERRQ(ierr);
    

            PetscScalar v = (1-xpoint)/2.0*westarray[j] + (1+xpoint)/2.0*eastarray[j] + (1-ypoint)/2.0*southarray[i] + (1+ypoint)/2.0*northarray[i]
            -(1-xpoint)*(1-ypoint)/4.0*LowerLeft - (1-xpoint)*(1+ypoint)/4.0*UpperLeft - (1+xpoint)*(1-ypoint)/4.0*LowerRight - (1+xpoint)*(1+ypoint)/4.0*UpperRight;
      
            ierr = VecSetValues(Box, 1, &row, &v, INSERT_VALUES);CHKERRQ(ierr);   
            }

        
    }




  PetscFunctionReturn(0);
}



//vectors encode derivatives
PetscErrorCode FormInteriorBox(Vec Interior, Vec EastDer, Vec WestDer, Vec NorthDer, Vec SouthDer, PetscScalar LowerLeftxcord, PetscScalar LowerLeftycord, PetscScalar UpperRightxcord, PetscScalar UpperRightycord, PetscInt NumPoints)
{
PetscErrorCode ierr;
PetscScalar *eastarray, *westarray, *southarray, *northarray;
ierr = VecGetArray(EastDer,&eastarray);CHKERRQ(ierr);
ierr = VecGetArray(WestDer,&westarray);CHKERRQ(ierr);
ierr = VecGetArray(SouthDer,&southarray);CHKERRQ(ierr);
ierr = VecGetArray(NorthDer,&northarray);CHKERRQ(ierr);

for (PetscInt j = 0; j < NumPoints-2; ++j)
{
    //generate each row
  for (PetscInt i = 0; i < NumPoints-2; ++i)
  {
  PetscInt row = i + j*(NumPoints-2);


  //only taking interior points

  // double xpoint = -PetscCosReal(PETSC_PI*(double)(i+1)/(NumPoints - 1));
  // double ypoint = -PetscCosReal(PETSC_PI*(double)(j+1)/(NumPoints - 1));
  double xpoint = -PetscCosReal(PETSC_PI*(double)(i+1)/(NumPoints - 1));
  double ypoint = -PetscCosReal(PETSC_PI*(double)(j+1)/(NumPoints - 1));
  // ierr = intervalShift(-1,  1, LowerLeftxcord, UpperRightxcord,  &xpoint);CHKERRQ(ierr);
  // ierr = intervalShift(-1,  1, LowerLeftycord, UpperRightycord,  &ypoint);CHKERRQ(ierr);

            

            PetscScalar vxx = (1-ypoint)/2.0*southarray[i] + (1+ypoint)/2.0*northarray[i];
      
            PetscScalar vyy = (1-xpoint)/2.0*westarray[j] + (1+xpoint)/2.0*eastarray[j];
      
            PetscScalar v = vxx + vyy;
            ierr = VecSetValues(Interior, 1, &row, &v, INSERT_VALUES);CHKERRQ(ierr);   
  }

        
}

  

  PetscFunctionReturn(0);
}

//vectors encode derivatives
PetscErrorCode FormInteriorResBox(Vec Interior, Vec EastDer, Vec WestDer, Vec NorthDer, Vec SouthDer, PetscScalar LowerLeftxcord, PetscScalar LowerLeftycord, PetscScalar UpperRightxcord, PetscScalar UpperRightycord, PetscInt NumPoints)
{
PetscErrorCode ierr;
PetscScalar *eastarray, *westarray, *southarray, *northarray;
ierr = VecGetArray(EastDer,&eastarray);CHKERRQ(ierr);
ierr = VecGetArray(WestDer,&westarray);CHKERRQ(ierr);
ierr = VecGetArray(SouthDer,&southarray);CHKERRQ(ierr);
ierr = VecGetArray(NorthDer,&northarray);CHKERRQ(ierr);

for (PetscInt j = 0; j < NumPoints-2; ++j)
{
    //generate each row
  for (PetscInt i = 0; i < NumPoints-2; ++i)
  {
  PetscInt row = i + j*(NumPoints-2);


  //only taking interior points

  // double xpoint = -PetscCosReal(PETSC_PI*(double)(i+1)/(NumPoints - 1));
  // double ypoint = -PetscCosReal(PETSC_PI*(double)(j+1)/(NumPoints - 1));
  double xpoint = 2*(double)(i+1)/(NumPoints - 1) -1;
  double ypoint = 2*(double)(j+1)/(NumPoints - 1) -1;
  // ierr = intervalShift(-1,  1, LowerLeftxcord, UpperRightxcord,  &xpoint);CHKERRQ(ierr);
  // ierr = intervalShift(-1,  1, LowerLeftycord, UpperRightycord,  &ypoint);CHKERRQ(ierr);

            

            PetscScalar vxx = (1-ypoint)/2.0*southarray[i] + (1+ypoint)/2.0*northarray[i];
      
            PetscScalar vyy = (1-xpoint)/2.0*westarray[j] + (1+xpoint)/2.0*eastarray[j];
      
            PetscScalar v = vxx + vyy;
            ierr = VecSetValues(Interior, 1, &row, &v, INSERT_VALUES);CHKERRQ(ierr);   
  }

        
}

  

  PetscFunctionReturn(0);
}


PetscErrorCode FormResMatrix(Mat A, PetscScalar LowerLeftxcord, PetscScalar LowerLeftycord, PetscScalar UpperRightxcord, PetscScalar UpperRightycord, PetscInt NumPoints)
{
PetscErrorCode ierr;
for (PetscInt j = 0; j < NumPoints-2; ++j)
{
    //generate each row
  for (PetscInt i = 0; i < NumPoints-2; ++i)
  {
  PetscInt row = i + j*(NumPoints-2);

  //only taking interior points

  double xpoint = 2*(double)(i+1)/(NumPoints - 1) -1;
  double ypoint = 2*(double)(j+1)/(NumPoints - 1) -1;

  //We want the laplacian of bases phi(M^-1(x)) where M maps from square to patch
  //Invert this matrix to see what would yield equality at laplace(u) = f(x)
  //So each basis must be e.g. phi_xx(M^-1(x)) * [M^-1]'^2 = 4/(Rightx-Leftx)
  PetscScalar yshiftder =  4/((UpperRightycord-LowerLeftycord)*(UpperRightycord-LowerLeftycord));
  PetscScalar xshiftder =  4/((UpperRightxcord-LowerLeftxcord)*(UpperRightxcord-LowerLeftxcord));
  //PetscPrintf(PETSC_COMM_SELF, "scaling (x, y): %f, %f", xshiftder, yshiftder);

  double *chebtestx = Chebpoly(xpoint, NumPoints);
  double *chebtesty = Chebpoly(ypoint, NumPoints);
  double *chebderivx = Chebderiv(xpoint, NumPoints, chebtestx);
  double *chebderivy = Chebderiv(ypoint, NumPoints, chebtesty);
  double *chebsecondderivx = Chebsecondderiv(xpoint, NumPoints, chebderivx);
  double *chebsecondderivy = Chebsecondderiv(ypoint, NumPoints, chebderivy);
    for (PetscInt jcol = 0; jcol < NumPoints-2; ++jcol)
    {
            for (PetscInt icol = 0; icol < NumPoints-2; ++icol)
            {
                PetscInt col = icol + jcol*(NumPoints-2);
                if ((icol+2)%2 == 0 && (jcol+2)%2 == 0)
                {
                //recombination of bases that has homogenous boundaries
                PetscScalar v = yshiftder*chebsecondderivy[jcol+2]*(chebtestx[icol+2]-1) + xshiftder*chebsecondderivx[icol+2]*(chebtesty[jcol+2]-1);
                ierr = MatSetValues(A, 1, &row, 1, &col, &v, INSERT_VALUES);CHKERRQ(ierr);
                }
                if ((icol+2)%2 == 0 && (jcol+2)%2 == 1)
                {
                //recombination of bases that has homogenous boundaries
                PetscScalar v = yshiftder*chebsecondderivy[jcol+2]*(chebtestx[icol+2]-1) + xshiftder*chebsecondderivx[icol+2]*(chebtesty[jcol+2]-ypoint);
                ierr = MatSetValues(A, 1, &row, 1, &col, &v, INSERT_VALUES);CHKERRQ(ierr);
                }
                if ((icol+2)%2 == 1 && (jcol+2)%2 == 0)
                {
                //recombination of bases that has homogenous boundaries
                PetscScalar v = yshiftder*chebsecondderivy[jcol+2]*(chebtestx[icol+2]-xpoint) + xshiftder*chebsecondderivx[icol+2]*(chebtesty[jcol+2]-1);
                ierr = MatSetValues(A, 1, &row, 1, &col, &v, INSERT_VALUES);CHKERRQ(ierr);
                }
                if ((icol+2)%2 == 1 && (jcol+2)%2 == 1)
                {
                //recombination of bases that has homogenous boundaries
                PetscScalar v = yshiftder*chebsecondderivy[jcol+2]*(chebtestx[icol+2]-xpoint) + xshiftder*chebsecondderivx[icol+2]*(chebtesty[jcol+2]-ypoint);
                ierr = MatSetValues(A, 1, &row, 1, &col, &v, INSERT_VALUES);CHKERRQ(ierr);
                }
        }
      }
    }
}
  PetscFunctionReturn(0);
}


//Sin(Pi*x)Sin(Pi*y)

PetscErrorCode FormTestFunctionBox1(Vec East, Vec West, Vec North, Vec South, PetscScalar LowerLeftxcord, PetscScalar LowerLeftycord, PetscScalar UpperRightxcord, PetscScalar UpperRightycord, PetscInt NumPoints)
{
    PetscErrorCode ierr;
    PetscScalar    v;



for (PetscInt j = 0; j < NumPoints; ++j)
{
  //x = -1 on the left, 2x^2-2 = 0
  double ypoint = 2*(double)(j)/(NumPoints - 1)-1;
  ierr = intervalShift(-1,  1, LowerLeftycord, UpperRightycord,  &ypoint);CHKERRQ(ierr);
  

  v = PetscSinReal(PETSC_PI*-1)*PetscSinReal(PETSC_PI*ypoint);
  ierr = VecSetValues(West, 1, &j, &v, ADD_VALUES);CHKERRQ(ierr); 


   v = PetscSinReal(PETSC_PI*1)*PetscSinReal(PETSC_PI*ypoint);
  ierr = VecSetValues(East, 1, &j, &v, ADD_VALUES);CHKERRQ(ierr); 

}

for (PetscInt j = 0; j < NumPoints; ++j)
{
    double xpoint = 2*(double)(j)/(NumPoints - 1)-1;
    ierr = intervalShift(-1,  1, LowerLeftycord, UpperRightycord,  &xpoint);CHKERRQ(ierr);
  
    //scans across then up
    
    v = PetscSinReal(PETSC_PI*xpoint)*PetscSinReal(PETSC_PI*-1);
    ierr = VecSetValues(South, 1, &j, &v, ADD_VALUES);CHKERRQ(ierr); 


     v = PetscSinReal(PETSC_PI*xpoint)*PetscSinReal(PETSC_PI*1);
    ierr = VecSetValues(North, 1, &j, &v, ADD_VALUES);CHKERRQ(ierr); 

}   
 
      PetscFunctionReturn(0);
}

PetscErrorCode FormTestFunctionBox2(Vec East, Vec West, Vec North, Vec South, PetscScalar LowerLeftxcord, PetscScalar LowerLeftycord, PetscScalar UpperRightxcord, PetscScalar UpperRightycord, PetscInt NumPoints)
{
    PetscErrorCode ierr;
    PetscScalar    v;



for (PetscInt j = 0; j < NumPoints; ++j)
{
  //x = -1 on the left, 2x^2-2 = 0
  double ypoint = 2*(double)(j)/(NumPoints - 1)-1;
  ierr = intervalShift(-1,  1, LowerLeftycord, UpperRightycord,  &ypoint);CHKERRQ(ierr);
  

  v = (2*(-1)*(-1)-2)*(2*ypoint*ypoint-2);
  ierr = VecSetValues(West, 1, &j, &v, ADD_VALUES);CHKERRQ(ierr); 


  v = (2-2)*(2*ypoint*ypoint-2);
  ierr = VecSetValues(East, 1, &j, &v, ADD_VALUES);CHKERRQ(ierr); 

}

for (PetscInt j = 0; j < NumPoints; ++j)
{
    double xpoint = 2*(double)(j)/(NumPoints - 1)-1;
    ierr = intervalShift(-1,  1, LowerLeftycord, UpperRightycord,  &xpoint);CHKERRQ(ierr);
  
    //scans across then up

    v = (2*(-1)*(-1)-2)*(2*xpoint*xpoint-2);
    ierr = VecSetValues(South, 1, &j, &v, ADD_VALUES);CHKERRQ(ierr); 


    v = (2-2)*(2*xpoint*xpoint-2);
    ierr = VecSetValues(North, 1, &j, &v, ADD_VALUES);CHKERRQ(ierr); 

}   
 
      PetscFunctionReturn(0);
}


PetscErrorCode FormTestFunctionInteriorBox1(Vec EastDer, Vec WestDer, Vec NorthDer, Vec SouthDer, PetscScalar LowerLeftxcord, PetscScalar LowerLeftycord, PetscScalar UpperRightxcord, PetscScalar UpperRightycord, PetscInt NumPoints)
{
    PetscErrorCode ierr;
    PetscScalar    v;


     
for (PetscInt j = 0; j < NumPoints; ++j)
{
    for (PetscInt i = 0; i < NumPoints; ++i)
    {
    //scans across then up
    double xpoint = -PetscCosReal(PETSC_PI*(double)i/(NumPoints - 1));
    double ypoint = -PetscCosReal(PETSC_PI*(double)j/(NumPoints - 1));

    ierr = intervalShift(-1,  1, LowerLeftxcord, UpperRightxcord,  &xpoint);CHKERRQ(ierr);
    ierr = intervalShift(-1,  1, LowerLeftycord, UpperRightycord,  &ypoint);CHKERRQ(ierr);
    
      //Vec WestDer
      if (i == 0)
      {
        v = -PETSC_PI*PETSC_PI*PetscSinReal(PETSC_PI*xpoint)*PetscSinReal(PETSC_PI*ypoint);
        ierr = VecSetValues(WestDer, 1, &j, &v, ADD_VALUES);CHKERRQ(ierr); 
        //LowerLeft
    
      }

      //VecEast
      if (i == NumPoints-1)
      {
        v = -PETSC_PI*PETSC_PI*PetscSinReal(PETSC_PI*xpoint)*PetscSinReal(PETSC_PI*ypoint);
        ierr = VecSetValues(EastDer, 1, &j, &v, ADD_VALUES);CHKERRQ(ierr); 
        //LowerRight
        
      }
      //VecSouth
      if (j == 0)
      {
        v = -PETSC_PI*PETSC_PI*PetscSinReal(PETSC_PI*xpoint)*PetscSinReal(PETSC_PI*ypoint);
        ierr = VecSetValues(SouthDer, 1, &i, &v, ADD_VALUES);CHKERRQ(ierr); 
        //LowerLeft
      }

      //VecNorth
      if (j == NumPoints-1)
      {
        v =  -PETSC_PI*PETSC_PI*PetscSinReal(PETSC_PI*xpoint)*PetscSinReal(PETSC_PI*ypoint);
        ierr = VecSetValues(NorthDer, 1, &i, &v, ADD_VALUES);CHKERRQ(ierr); 
        //LowerLeft
      }


    }

}   
 
      PetscFunctionReturn(0);
}

PetscErrorCode FormTestFunctionInteriorBox2(Vec EastDer, Vec WestDer, Vec NorthDer, Vec SouthDer, PetscScalar LowerLeftxcord, PetscScalar LowerLeftycord, PetscScalar UpperRightxcord, PetscScalar UpperRightycord, PetscInt NumPoints)
{
    PetscErrorCode ierr;
    PetscScalar    v;


     
for (PetscInt j = 0; j < NumPoints; ++j)
{
    for (PetscInt i = 0; i < NumPoints; ++i)
    {
    //scans across then up
    double xpoint = -PetscCosReal(PETSC_PI*(double)i/(NumPoints - 1));
    double ypoint = -PetscCosReal(PETSC_PI*(double)j/(NumPoints - 1));

    ierr = intervalShift(-1,  1, LowerLeftxcord, UpperRightxcord,  &xpoint);CHKERRQ(ierr);
    ierr = intervalShift(-1,  1, LowerLeftycord, UpperRightycord,  &ypoint);CHKERRQ(ierr);
    
      //Vec WestDer
      if (i == 0)
      {
        v = 0;
        ierr = VecSetValues(WestDer, 1, &j, &v, ADD_VALUES);CHKERRQ(ierr); 
        //LowerLeft
    
      }

      //VecEast
      if (i == NumPoints-1)
      {
        v =0;
        ierr = VecSetValues(EastDer, 1, &j, &v, ADD_VALUES);CHKERRQ(ierr); 
        //LowerRight
        
      }
      //VecSouth
      if (j == 0)
      {
        v = 0;
        ierr = VecSetValues(SouthDer, 1, &i, &v, ADD_VALUES);CHKERRQ(ierr); 
        //LowerLeft
      }

      //VecNorth
      if (j == NumPoints-1)
      {
        v =  0;
        ierr = VecSetValues(NorthDer, 1, &i, &v, ADD_VALUES);CHKERRQ(ierr); 
        //LowerLeft
      }


    }

}   
 
      PetscFunctionReturn(0);
}



PetscErrorCode FormTestFunctionInterior1(Vec input, PetscScalar LowerLeftxcord, PetscScalar LowerLeftycord, PetscScalar UpperRightxcord, PetscScalar UpperRightycord, PetscInt NumPoints)
{
    PetscErrorCode ierr;
    PetscScalar    v;


     
    for (PetscInt j = 0; j < NumPoints-2; ++j)
    {
    double ypoint = -PetscCosReal(PETSC_PI*(double)(j+1)/(NumPoints - 1));
    ierr = intervalShift(-1,  1, LowerLeftycord, UpperRightycord,  &ypoint);CHKERRQ(ierr);
        for (PetscInt i = 0; i < NumPoints-2; ++i)
        {
        double xpoint = -PetscCosReal(PETSC_PI*(double)(i+1)/(NumPoints - 1));

        ierr = intervalShift(-1,  1, LowerLeftxcord, UpperRightxcord,  &xpoint);CHKERRQ(ierr);


        PetscInt row = i + j*(NumPoints-2);
          
          
            //dxx*u(y) + dyy*u(x)
          v = -PETSC_PI*PETSC_PI*(PetscSinReal(PETSC_PI*xpoint)*PetscSinReal(PETSC_PI*ypoint)+PetscSinReal(PETSC_PI*ypoint)*PetscSinReal(PETSC_PI*xpoint));
          
          ierr = VecSetValues(input, 1, &row, &v, INSERT_VALUES);CHKERRQ(ierr);    
        }

    }   
     
          PetscFunctionReturn(0);
}

PetscErrorCode FormTestFunctionInterior2(Vec input, PetscScalar LowerLeftxcord, PetscScalar LowerLeftycord, PetscScalar UpperRightxcord, PetscScalar UpperRightycord, PetscInt NumPoints)
{
    PetscErrorCode ierr;
    PetscScalar    v;
    
    // PetscScalar yshiftder =  (UpperRightycord-LowerLeftycord)*(UpperRightycord-LowerLeftycord)/(4);
    // PetscScalar xshiftder =  (UpperRightxcord-LowerLeftxcord)*(UpperRightxcord-LowerLeftxcord)/(4);

     
    for (PetscInt j = 0; j < NumPoints-2; ++j)
    {
    double ypoint = -PetscCosReal(PETSC_PI*(double)(j+1)/(NumPoints - 1));
    ierr = intervalShift(-1,  1, LowerLeftycord, UpperRightycord,  &ypoint);CHKERRQ(ierr);

        for (PetscInt i = 0; i < NumPoints-2; ++i)
        {
        double xpoint = -PetscCosReal(PETSC_PI*(double)(i+1)/(NumPoints - 1));

        ierr = intervalShift(-1,  1, LowerLeftxcord, UpperRightxcord,  &xpoint);CHKERRQ(ierr);

        PetscInt row = i + j*(NumPoints-2);
          
          //Let M(x) map [-1, 1] to [a, b] where [a,b] is the width of the patch
          //L(x) map [-1, 1] to [c, d] the height of the patch
          //M is linear so M' is constant (b-a)/(1 - -1) = (b-a)/2
          //Laplacian = dxxu(M(x))*u(M(y)) + dyyu(M(x))*u(x) = (b-a)^2/4 u''(M(x)) + (c-d)^2/4 u''(M(y))

          v = 4*(2*xpoint*xpoint-2) +4*(2*ypoint*ypoint-2);

          
          ierr = VecSetValues(input, 1, &row, &v, INSERT_VALUES);CHKERRQ(ierr);    
        }

    }   
     
          PetscFunctionReturn(0);
}

PetscErrorCode FormTestFunctionResBox1(Vec EastDer, Vec WestDer, Vec NorthDer, Vec SouthDer, PetscScalar LowerLeftxcord, PetscScalar LowerLeftycord, PetscScalar UpperRightxcord, PetscScalar UpperRightycord, PetscInt NumPoints)
{
    PetscErrorCode ierr;
    PetscScalar    v;


//calculates dxx u(x, L) or dyy u(L, y) etc
     
for (PetscInt j = 0; j < NumPoints; ++j)
{
    for (PetscInt i = 0; i < NumPoints; ++i)
    {
    //scans across then up
    double xpoint = 2*(double)i/(NumPoints - 1)-1;
    double ypoint = 2*(double)j/(NumPoints - 1)-1;

    ierr = intervalShift(-1,  1, LowerLeftxcord, UpperRightxcord,  &xpoint);CHKERRQ(ierr);
    ierr = intervalShift(-1,  1, LowerLeftycord, UpperRightycord,  &ypoint);CHKERRQ(ierr);
    
      //Vec WestDer
      if (i == 0)
      {
        v = 0;
        ierr = VecSetValues(WestDer, 1, &j, &v, ADD_VALUES);CHKERRQ(ierr); 
        //LowerLeft
    
      }

      if (i == NumPoints-1)
      {
        v =0;
        ierr = VecSetValues(EastDer, 1, &j, &v, ADD_VALUES);CHKERRQ(ierr); 
        //LowerRight
        
      }
      //VecSouth
      if (j == 0)
      {
        v = 0;
        ierr = VecSetValues(SouthDer, 1, &i, &v, ADD_VALUES);CHKERRQ(ierr); 
        //LowerLeft
      }

      //VecNorth
      if (j == NumPoints-1)
      {
        v =  0;
        ierr = VecSetValues(NorthDer, 1, &i, &v, ADD_VALUES);CHKERRQ(ierr); 
        //LowerLeft
      }


    }

}   
 
      PetscFunctionReturn(0);
}

PetscErrorCode FormTestFunctionResBox2(Vec EastDer, Vec WestDer, Vec NorthDer, Vec SouthDer, PetscScalar LowerLeftxcord, PetscScalar LowerLeftycord, PetscScalar UpperRightxcord, PetscScalar UpperRightycord, PetscInt NumPoints)
{
    PetscErrorCode ierr;
    PetscScalar    v;


     
for (PetscInt j = 0; j < NumPoints; ++j)
{
    for (PetscInt i = 0; i < NumPoints; ++i)
    {
    //scans across then up
    double xpoint = 2*(double)i/(NumPoints - 1)-1;
    double ypoint = 2*(double)j/(NumPoints - 1)-1;

    ierr = intervalShift(-1,  1, LowerLeftxcord, UpperRightxcord,  &xpoint);CHKERRQ(ierr);
    ierr = intervalShift(-1,  1, LowerLeftycord, UpperRightycord,  &ypoint);CHKERRQ(ierr);
    
      //Vec WestDer
      if (i == 0)
      {
        v = 0;
        ierr = VecSetValues(WestDer, 1, &j, &v, ADD_VALUES);CHKERRQ(ierr); 
        //LowerLeft
    
      }

      //VecEast
      if (i == NumPoints-1)
      {
        v =0;
        ierr = VecSetValues(EastDer, 1, &j, &v, ADD_VALUES);CHKERRQ(ierr); 
        //LowerRight
        
      }
      //VecSouth
      if (j == 0)
      {
        v = 0;
        ierr = VecSetValues(SouthDer, 1, &i, &v, ADD_VALUES);CHKERRQ(ierr); 
        //LowerLeft
      }

      //VecNorth
      if (j == NumPoints-1)
      {
        v =  0;
        ierr = VecSetValues(NorthDer, 1, &i, &v, ADD_VALUES);CHKERRQ(ierr); 
        //LowerLeft
      }


    }

}   
 
      PetscFunctionReturn(0);
}

PetscErrorCode FormSolMatrix(Mat A, PetscScalar LowerLeftxcord, PetscScalar LowerLeftycord, PetscScalar UpperRightxcord, PetscScalar UpperRightycord, PetscInt NumPoints)
{
  PetscErrorCode ierr;
  for (PetscInt j = 0; j < NumPoints-2; ++j)
  {
      //generate each row
    for (PetscInt i = 0; i < NumPoints-2; ++i)
    {
    PetscInt row = i + j*(NumPoints-2);
    // //generalize this to any set of points later
    // double xpoint = -PetscCosReal(PETSC_PI*(double)i/(NumPoints - 1));
    // double ypoint = -PetscCosReal(PETSC_PI*(double)j/(NumPoints - 1));

    double xpoint = 2*(double)(i+1)/(NumPoints - 1)-1;
    double ypoint = 2*(double)(j+1)/(NumPoints - 1)-1;
    double *chebtestx = Chebpoly(xpoint, NumPoints);
    double *chebtesty = Chebpoly(ypoint, NumPoints);
    // ierr = intervalShift(-1,  1, LowerLeftxcord, UpperRightxcord,  &xpoint);CHKERRQ(ierr);
    // ierr = intervalShift(-1,  1, LowerLeftycord, UpperRightycord,  &ypoint);CHKERRQ(ierr);
  
      for (PetscInt jcol = 0; jcol < NumPoints-2; ++jcol)
      {
              for (PetscInt icol = 0; icol < NumPoints-2; ++icol)
              {
                  PetscInt col = icol + jcol*(NumPoints-2);
                  //if (condition) == border point
                
                if ((icol+2)%2 == 0 && (jcol+2)%2 == 0)
                {
                //recombination of bases that has homogenous boundaries
                PetscScalar v = (chebtestx[icol+2]-1)*(chebtesty[jcol+2]-1);
                ierr = MatSetValues(A, 1, &row, 1, &col, &v, INSERT_VALUES);CHKERRQ(ierr);
                }
                if ((icol+2)%2 == 0 && (jcol+2)%2 == 1)
                {
                //recombination of bases that has homogenous boundaries
                PetscScalar v = (chebtestx[icol+2]-1)*(chebtesty[jcol+2]-ypoint);
                ierr = MatSetValues(A, 1, &row, 1, &col, &v, INSERT_VALUES);CHKERRQ(ierr);
                }
                if ((icol+2)%2 == 1 && (jcol+2)%2 == 0)
                {
                //recombination of bases that has homogenous boundaries
                PetscScalar v = (chebtestx[icol+2]-xpoint)*(chebtesty[jcol+2]-1);
                ierr = MatSetValues(A, 1, &row, 1, &col, &v, INSERT_VALUES);CHKERRQ(ierr);
                }
                if ((icol+2)%2 == 1 && (jcol+2)%2 == 1)
                {
                //recombination of bases that has homogenous boundaries
                PetscScalar v = (chebtestx[icol+2]-xpoint)*(chebtesty[jcol+2]-ypoint);
                ierr = MatSetValues(A, 1, &row, 1, &col, &v, INSERT_VALUES);CHKERRQ(ierr);
                } 


                  
              
              }
        }
      }
  }
  PetscFunctionReturn(0);
}


PetscErrorCode SynthesizeBoxCheb(Vec interior, Vec box, PetscInt NumPoints)
{
  PetscErrorCode ierr;
  VecScatter scat;
  IS to;
  PetscInt *indices;
  PetscInt idx = 0;

  PetscMalloc((NumPoints-2)*(NumPoints-2)*sizeof(PetscInt),&indices);

  for (PetscInt jcol = 0; jcol < NumPoints; ++jcol)
  {
          for (PetscInt icol = 0; icol < NumPoints; ++icol)
          {
              PetscInt col = icol + jcol*(NumPoints);
              if(jcol != 0 && jcol != NumPoints-1 && icol != 0 && icol != NumPoints -1)
              {
              indices[idx] = col;
              ++idx;
              //printf("idx: %d col: %d \n", idx, col);
              }

          }
  }

  ierr = ISCreateGeneral(PETSC_COMM_SELF,(NumPoints-2)*(NumPoints-2),indices,PETSC_COPY_VALUES,&to);CHKERRQ(ierr);

  ierr = VecScatterCreate(interior, NULL,  box, to, &scat);CHKERRQ(ierr);
  ierr = VecScatterBegin(scat,interior,box,ADD_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
  ierr = VecScatterEnd(scat, interior,box, ADD_VALUES,SCATTER_FORWARD);CHKERRQ(ierr);
  ierr = VecScatterDestroy(&scat);CHKERRQ(ierr);
  ierr = ISDestroy(&to);
  ierr = PetscFree(indices);CHKERRQ(ierr);


  PetscFunctionReturn(0);
}

PetscErrorCode FormExactTestFunction1(Vec input, PetscScalar LowerLeftxcord, PetscScalar LowerLeftycord, PetscScalar UpperRightxcord, PetscScalar UpperRightycord, PetscInt NumPoints)
{
    PetscErrorCode ierr;
    PetscScalar    v;


for (PetscInt j = 0; j < NumPoints; ++j)
{
double ypoint = 2*(double)j/(NumPoints - 1)-1;
ierr = intervalShift(-1,  1, LowerLeftycord, UpperRightycord,  &ypoint);CHKERRQ(ierr);

    for (PetscInt i = 0; i < NumPoints; ++i)
    {
    double xpoint = 2*(double)i/(NumPoints - 1)-1;
    PetscInt row = i + j*NumPoints;

      ierr = intervalShift(-1,  1, LowerLeftxcord, UpperRightxcord,  &xpoint);CHKERRQ(ierr);
    
      
 
    v =  PetscSinReal(PETSC_PI*xpoint)*PetscSinReal(PETSC_PI*ypoint);
      

      

   
    
    ierr = VecSetValues(input, 1, &row, &v, INSERT_VALUES);CHKERRQ(ierr);    
    }

}   
 
      PetscFunctionReturn(0);
}

PetscErrorCode FormExactTestFunction2(Vec input, PetscScalar LowerLeftxcord, PetscScalar LowerLeftycord, PetscScalar UpperRightxcord, PetscScalar UpperRightycord, PetscInt NumPoints)
{
    PetscErrorCode ierr;
    PetscScalar    v;


for (PetscInt j = 0; j < NumPoints; ++j)
{
double ypoint = 2*(double)j/(NumPoints - 1)-1;
ierr = intervalShift(-1,  1, LowerLeftycord, UpperRightycord,  &ypoint);CHKERRQ(ierr);

    for (PetscInt i = 0; i < NumPoints; ++i)
    {
    double xpoint = 2*(double)i/(NumPoints - 1)-1;
    PetscInt row = i + j*NumPoints;

      ierr = intervalShift(-1,  1, LowerLeftxcord, UpperRightxcord,  &xpoint);CHKERRQ(ierr);

      
 
    v =  (2*xpoint*xpoint-2)*(2*ypoint*ypoint-2);

      

    //PetscPrintf(PETSC_COMM_SELF, "inserting point: [%f] for x,y = (%f, %f)\n", v, xpoint, ypoint);
    
    ierr = VecSetValues(input, 1, &row, &v, INSERT_VALUES);CHKERRQ(ierr);    


    }

}   
 
      PetscFunctionReturn(0);
}



//given coefficients on whole grid, get points along boundary
//we take at equispaced points because this is used to construct our equispaced approximation, not the bases for our solution
PetscErrorCode getfirstPatch(Vec Coefficients, PetscScalar LowerLeftxcord, PetscScalar LowerLeftycord, PetscScalar UpperRightxcord, PetscScalar UpperRightycord, PetscInt NumPoints, Vec East, Vec West, Vec North, Vec South)
{
  PetscErrorCode ierr;
  PetscScalar v;
  PetscScalar *coeff;
  
  ierr = VecGetArray(Coefficients,&coeff);CHKERRQ(ierr);
  for (PetscInt j = 0; j < NumPoints; ++j)
  {
      double ypoint = 2*(double)j/(NumPoints - 1)-1;
      ierr = intervalShift(-1,  1, LowerLeftycord, UpperRightycord,  &ypoint);CHKERRQ(ierr);

      for (PetscInt i = 0; i < NumPoints; ++i)
      {
      //scans across then up     
      double xpoint = 2*(double)i/(NumPoints - 1)-1;
      ierr = intervalShift(-1,  1, LowerLeftxcord, UpperRightxcord,  &xpoint);CHKERRQ(ierr);

      double *chebtestx = Chebpoly(xpoint, NumPoints);
      double *chebtesty = Chebpoly(ypoint, NumPoints);
      v = 0;
            for (PetscInt jcol = 0; jcol < NumPoints-2; ++jcol)
            {
                    for (PetscInt icol = 0; icol < NumPoints-2; ++icol)
                    {

                      PetscInt col = icol + jcol*(NumPoints-2);

                      if ((icol+2)%2 == 0 && (jcol+2)%2 == 0)
                      {
                      //recombination of bases that has homogenous boundaries
                      v = v + (chebtestx[icol+2]-1)*(chebtesty[jcol+2]-1)*coeff[col];                      
                      }
                      if ((icol+2)%2 == 0 && (jcol+2)%2 == 1)
                      {
                      //recombination of bases that has homogenous boundaries
                      v = v + (chebtestx[icol+2]-1)*(chebtesty[jcol+2]-ypoint)*coeff[col];
                      }
                      if ((icol+2)%2 == 1 && (jcol+2)%2 == 0)
                      {
                      //recombination of bases that has homogenous boundaries
                      v = v + (chebtestx[icol+2]-xpoint)*(chebtesty[jcol+2]-1)*coeff[col];
                      }
                      if ((icol+2)%2 == 1 && (jcol+2)%2 == 1)
                      {
                      //recombination of bases that has homogenous boundaries
                      v = v + (chebtestx[icol+2]-xpoint)*(chebtesty[jcol+2]-ypoint)*coeff[col];
                      } 
                    }
          }
      
  
        if (j == 0)
        {
          ierr = VecSetValues(South, 1, &i, &v, INSERT_VALUES);CHKERRQ(ierr);    
        }
        if (j == NumPoints-1)
        {
          ierr = VecSetValues(North, 1, &i, &v, INSERT_VALUES);CHKERRQ(ierr);    
        } 
        if (i == 0)
        {
          ierr = VecSetValues(West, 1, &j, &v, INSERT_VALUES);CHKERRQ(ierr);    
        }
        if (i == NumPoints-1)
        {
          ierr = VecSetValues(East, 1, &j, &v, INSERT_VALUES);CHKERRQ(ierr);  
        }  
      }
    } 
  

  PetscFunctionReturn(0);
}

PetscErrorCode FormExactResFunction1(Vec input, PetscScalar LowerLeftxcord, PetscScalar LowerLeftycord, PetscScalar UpperRightxcord, PetscScalar UpperRightycord, PetscInt NumPoints )
{
    PetscErrorCode ierr;
    PetscScalar    v;


for (PetscInt j = 0; j < NumPoints-2; ++j)
{
double ypoint = 2*(double)(j+1)/(NumPoints - 1)-1;
ierr = intervalShift(-1,  1, LowerLeftycord, UpperRightycord,  &ypoint);CHKERRQ(ierr);
    for (PetscInt i = 0; i < NumPoints-2; ++i)
    {
    double xpoint = 2*(double)(i+1)/(NumPoints - 1)-1;
    ierr = intervalShift(-1,  1, LowerLeftxcord, UpperRightxcord,  &xpoint);CHKERRQ(ierr);

    PetscInt row = i + j*(NumPoints-2);
    

       v = -PETSC_PI*PETSC_PI*(PetscSinReal(PETSC_PI*xpoint)*PetscSinReal(PETSC_PI*ypoint)+PetscSinReal(PETSC_PI*ypoint)*PetscSinReal(PETSC_PI*xpoint));
          
      

   
    
    ierr = VecSetValues(input, 1, &row, &v, INSERT_VALUES);CHKERRQ(ierr);    
    }

}   
 
      PetscFunctionReturn(0);
}

PetscErrorCode FormExactResFunction2(Vec input, PetscScalar LowerLeftxcord, PetscScalar LowerLeftycord, PetscScalar UpperRightxcord, PetscScalar UpperRightycord, PetscInt NumPoints )
{
    PetscErrorCode ierr;
    PetscScalar    v;


for (PetscInt j = 0; j < NumPoints-2; ++j)
{
double ypoint = 2*(double)(j+1)/(NumPoints - 1)-1;
ierr = intervalShift(-1,  1, LowerLeftycord, UpperRightycord,  &ypoint);CHKERRQ(ierr);
    for (PetscInt i = 0; i < NumPoints-2; ++i)
    {
    double xpoint = 2*(double)(i+1)/(NumPoints - 1)-1;
    ierr = intervalShift(-1,  1, LowerLeftxcord, UpperRightxcord,  &xpoint);CHKERRQ(ierr);

    PetscInt row = i + j*(NumPoints-2);
    

      v = 4*(2*ypoint*ypoint-2)+4*(2*xpoint*xpoint-2);

      

   
    
    ierr = VecSetValues(input, 1, &row, &v, INSERT_VALUES);CHKERRQ(ierr);    
    }

}   
 
      PetscFunctionReturn(0);
}


PetscErrorCode getfirstPatchDer(Vec Coefficients, PetscScalar LowerLeftxcord, PetscScalar LowerLeftycord, PetscScalar UpperRightxcord, PetscScalar UpperRightycord, PetscInt NumPoints, Vec EastDer, Vec WestDer, Vec NorthDer, Vec SouthDer)
{
  PetscErrorCode ierr;
  PetscScalar v;
  PetscScalar *coeff;
  
  ierr = VecGetArray(Coefficients,&coeff);CHKERRQ(ierr);
  for (PetscInt j = 0; j < NumPoints; ++j)
  {
      for (PetscInt i = 0; i < NumPoints; ++i)
      {
      //scans across then up
      double xpoint = -PetscCosReal(PETSC_PI*(double)i/(NumPoints - 1));
      double ypoint = -PetscCosReal(PETSC_PI*(double)j/(NumPoints - 1));
      ierr = intervalShift(-1,  1, LowerLeftxcord, UpperRightxcord,  &xpoint);CHKERRQ(ierr);
      ierr = intervalShift(-1,  1, LowerLeftycord, UpperRightycord,  &ypoint);

      double *chebtestx = Chebpoly(xpoint, NumPoints);
      double *chebtesty = Chebpoly(ypoint, NumPoints);
      double *chebderivx = Chebderiv(xpoint, NumPoints, chebtestx);
      double *chebderivy = Chebderiv(ypoint, NumPoints, chebtesty);
      double *chebsecondderivx = Chebsecondderiv(xpoint, NumPoints, chebderivx);
      double *chebsecondderivy = Chebsecondderiv(ypoint, NumPoints, chebderivy);
  

      
  
        if (j == 0)
        {
          v = 0;
          for (PetscInt jcol = 0; jcol < NumPoints-2; ++jcol)
            {
                    for (PetscInt icol = 0; icol < NumPoints-2; ++icol)
                    {

                      PetscInt col = icol + jcol*(NumPoints-2);

                      if ((icol+2)%2 == 0 && (jcol+2)%2 == 0)
                      {
                      //recombination of bases that has homogenous boundaries
                      v = v + chebsecondderivx[icol+2]*(chebtesty[jcol+2]-1)*coeff[col];
                      
                      }
                      if ((icol+2)%2 == 0 && (jcol+2)%2 == 1)
                      {
                      //recombination of bases that has homogenous boundaries
                      v = v + chebsecondderivx[icol+2]*(chebtesty[jcol+2]-ypoint)*coeff[col];
                     
                      }
                      if ((icol+2)%2 == 1 && (jcol+2)%2 == 0)
                      {
                      //recombination of bases that has homogenous boundaries
                      v = v + chebsecondderivx[icol+2]*(chebtesty[jcol+2]-1)*coeff[col];

                      }
                      if ((icol+2)%2 == 1 && (jcol+2)%2 == 1)
                      {
                      //recombination of bases that has homogenous boundaries
                      v = v + chebsecondderivx[icol+2]*(chebtesty[jcol+2]-ypoint)*coeff[col];
                      }
                    }
            }

          ierr = VecSetValues(SouthDer, 1, &i, &v, INSERT_VALUES);CHKERRQ(ierr);    
        }



        if (j == NumPoints-1)
        {
            v = 0;
            for (PetscInt jcol = 0; jcol < NumPoints-2; ++jcol)
            {
                    for (PetscInt icol = 0; icol < NumPoints-2; ++icol)
                    {

                      PetscInt col = icol + jcol*(NumPoints-2);

                      if ((icol+2)%2 == 0 && (jcol+2)%2 == 0)
                      {
                      //recombination of bases that has homogenous boundaries
                      v = v + chebsecondderivx[icol+2]*(chebtesty[jcol+2]-1)*coeff[col];
                      
                      }
                      if ((icol+2)%2 == 0 && (jcol+2)%2 == 1)
                      {
                      //recombination of bases that has homogenous boundaries
                      v = v + chebsecondderivx[icol+2]*(chebtesty[jcol+2]-ypoint)*coeff[col];
                     
                      }
                      if ((icol+2)%2 == 1 && (jcol+2)%2 == 0)
                      {
                      //recombination of bases that has homogenous boundaries
                      v = v + chebsecondderivx[icol+2]*(chebtesty[jcol+2]-1)*coeff[col];

                      }
                      if ((icol+2)%2 == 1 && (jcol+2)%2 == 1)
                      {
                      //recombination of bases that has homogenous boundaries
                      v = v + chebsecondderivx[icol+2]*(chebtesty[jcol+2]-ypoint)*coeff[col];
                      }
                    }
          }
          ierr = VecSetValues(NorthDer, 1, &i, &v, INSERT_VALUES);CHKERRQ(ierr);    
        } 


        if (i == 0)
        {
            v = 0;
            for (PetscInt jcol = 0; jcol < NumPoints-2; ++jcol)
            {
                    for (PetscInt icol = 0; icol < NumPoints-2; ++icol)
                    {

                      PetscInt col = icol + jcol*(NumPoints-2);

                      if ((icol+2)%2 == 0 && (jcol+2)%2 == 0)
                      {
                      //recombination of bases that has homogenous boundaries
                      v = v + chebsecondderivy[icol+2]*(chebtestx[jcol+2]-1)*coeff[col];
                      
                      }
                      if ((icol+2)%2 == 0 && (jcol+2)%2 == 1)
                      {
                      //recombination of bases that has homogenous boundaries
                      v = v + chebsecondderivy[icol+2]*(chebtestx[jcol+2]-xpoint)*coeff[col];
                     
                      }
                      if ((icol+2)%2 == 1 && (jcol+2)%2 == 0)
                      {
                      //recombination of bases that has homogenous boundaries
                      v = v + chebsecondderivy[icol+2]*(chebtestx[jcol+2]-1)*coeff[col];

                      }
                      if ((icol+2)%2 == 1 && (jcol+2)%2 == 1)
                      {
                      //recombination of bases that has homogenous boundaries
                      v = v + chebsecondderivy[icol+2]*(chebtestx[jcol+2]-xpoint)*coeff[col];
                      }
                    }
          }
          ierr = VecSetValues(WestDer, 1, &j, &v, INSERT_VALUES);CHKERRQ(ierr);    
        } 



        if (i == NumPoints-1)
        {
            v = 0;
            for (PetscInt jcol = 0; jcol < NumPoints-2; ++jcol)
            {
                    for (PetscInt icol = 0; icol < NumPoints-2; ++icol)
                    {

                      PetscInt col = icol + jcol*(NumPoints-2);

                      if ((icol+2)%2 == 0 && (jcol+2)%2 == 0)
                      {
                      //recombination of bases that has homogenous boundaries
                      v = v + chebsecondderivy[jcol+2]*(chebtestx[icol+2]-1)*coeff[col];
                      
                      }
                      if ((icol+2)%2 == 0 && (jcol+2)%2 == 1)
                      {
                      //recombination of bases that has homogenous boundaries
                      v = v + chebsecondderivy[jcol+2]*(chebtestx[icol+2]-1)*coeff[col];
                     
                      }
                      if ((icol+2)%2 == 1 && (jcol+2)%2 == 0)
                      {
                      //recombination of bases that has homogenous boundaries
                      v = v + chebsecondderivy[jcol+2]*(chebtestx[icol+2]-xpoint)*coeff[col];

                      }
                      if ((icol+2)%2 == 1 && (jcol+2)%2 == 1)
                      {
                      //recombination of bases that has homogenous boundaries
                      v = v + chebsecondderivy[jcol+2]*(chebtestx[icol+2]-xpoint)*coeff[col];
                      }
                    }
          }
          ierr = VecSetValues(EastDer, 1, &j, &v, INSERT_VALUES);CHKERRQ(ierr);  
        }  
      }
    } 

    //Here we used to scale derivatives because points from big box to patch via mapping M:square -> patch are d_xx f(M(x)) = M''*(f''(M(x)))
    //Now we just encode it in interiorchebmatrix 
    // VecScale(EastDer, PetscPowScalarReal((LowerLeftxcord-UpperRightxcord)/(2.0), 2));
    // VecScale(WestDer, PetscPowScalarReal((LowerLeftxcord-UpperRightxcord)/(2.0), 2));  
    // VecScale(NorthDer, PetscPowScalarReal((LowerLeftycord-UpperRightycord)/(2.0), 2));
    // VecScale(SouthDer, PetscPowScalarReal((LowerLeftycord-UpperRightycord)/(2.0), 2));

  PetscFunctionReturn(0);
}

PetscErrorCode getfirstPatchRes(Vec Coefficients, PetscScalar LowerLeftxcord, PetscScalar LowerLeftycord, PetscScalar UpperRightxcord, PetscScalar UpperRightycord, PetscInt NumPoints, Vec EastDer, Vec WestDer, Vec NorthDer, Vec SouthDer)
{
  PetscErrorCode ierr;
  PetscScalar v;
  PetscScalar *coeff;
  
  ierr = VecGetArray(Coefficients,&coeff);CHKERRQ(ierr);
  for (PetscInt j = 0; j < NumPoints; ++j)
  {
      for (PetscInt i = 0; i < NumPoints; ++i)
      {
      //scans across then up
      double xpoint = 2*(double)i/(NumPoints-1) - 1;
      double ypoint = 2*(double)j/(NumPoints-1) - 1;
      ierr = intervalShift(-1,  1, LowerLeftxcord, UpperRightxcord,  &xpoint);CHKERRQ(ierr);
      ierr = intervalShift(-1,  1, LowerLeftycord, UpperRightycord,  &ypoint);

      double *chebtestx = Chebpoly(xpoint, NumPoints);
      double *chebtesty = Chebpoly(ypoint, NumPoints);
      double *chebderivx = Chebderiv(xpoint, NumPoints, chebtestx);
      double *chebderivy = Chebderiv(ypoint, NumPoints, chebtesty);
      double *chebsecondderivx = Chebsecondderiv(xpoint, NumPoints, chebderivx);
      double *chebsecondderivy = Chebsecondderiv(ypoint, NumPoints, chebderivy);
  

      
  
        if (j == 0)
        {
          v = 0;
          for (PetscInt jcol = 0; jcol < NumPoints-2; ++jcol)
            {
                    for (PetscInt icol = 0; icol < NumPoints-2; ++icol)
                    {

                      PetscInt col = icol + jcol*(NumPoints-2);

                      if ((icol+2)%2 == 0 && (jcol+2)%2 == 0)
                      {
                      //recombination of bases that has homogenous boundaries
                      v = v + chebsecondderivx[icol+2]*(chebtesty[jcol+2]-1)*coeff[col];
                      
                      }
                      if ((icol+2)%2 == 0 && (jcol+2)%2 == 1)
                      {
                      //recombination of bases that has homogenous boundaries
                      v = v + chebsecondderivx[icol+2]*(chebtesty[jcol+2]-ypoint)*coeff[col];
                     
                      }
                      if ((icol+2)%2 == 1 && (jcol+2)%2 == 0)
                      {
                      //recombination of bases that has homogenous boundaries
                      v = v + chebsecondderivx[icol+2]*(chebtesty[jcol+2]-1)*coeff[col];

                      }
                      if ((icol+2)%2 == 1 && (jcol+2)%2 == 1)
                      {
                      //recombination of bases that has homogenous boundaries
                      v = v + chebsecondderivx[icol+2]*(chebtesty[jcol+2]-ypoint)*coeff[col];
                      }
                    }
            }

          ierr = VecSetValues(SouthDer, 1, &i, &v, INSERT_VALUES);CHKERRQ(ierr);    
        }



        if (j == NumPoints-1)
        {
            v = 0;
            for (PetscInt jcol = 0; jcol < NumPoints-2; ++jcol)
            {
                    for (PetscInt icol = 0; icol < NumPoints-2; ++icol)
                    {

                      PetscInt col = icol + jcol*(NumPoints-2);

                      if ((icol+2)%2 == 0 && (jcol+2)%2 == 0)
                      {
                      //recombination of bases that has homogenous boundaries
                      v = v + chebsecondderivx[icol+2]*(chebtesty[jcol+2]-1)*coeff[col];
                      
                      }
                      if ((icol+2)%2 == 0 && (jcol+2)%2 == 1)
                      {
                      //recombination of bases that has homogenous boundaries
                      v = v + chebsecondderivx[icol+2]*(chebtesty[jcol+2]-ypoint)*coeff[col];
                     
                      }
                      if ((icol+2)%2 == 1 && (jcol+2)%2 == 0)
                      {
                      //recombination of bases that has homogenous boundaries
                      v = v + chebsecondderivx[icol+2]*(chebtesty[jcol+2]-1)*coeff[col];

                      }
                      if ((icol+2)%2 == 1 && (jcol+2)%2 == 1)
                      {
                      //recombination of bases that has homogenous boundaries
                      v = v + chebsecondderivx[icol+2]*(chebtesty[jcol+2]-ypoint)*coeff[col];
                      }
                    }
          }
          ierr = VecSetValues(NorthDer, 1, &i, &v, INSERT_VALUES);CHKERRQ(ierr);    
        } 


        if (i == 0)
        {
            v = 0;
            for (PetscInt jcol = 0; jcol < NumPoints-2; ++jcol)
            {
                    for (PetscInt icol = 0; icol < NumPoints-2; ++icol)
                    {

                      PetscInt col = icol + jcol*(NumPoints-2);

                      if ((icol+2)%2 == 0 && (jcol+2)%2 == 0)
                      {
                      //recombination of bases that has homogenous boundaries
                      v = v + chebsecondderivy[icol+2]*(chebtestx[jcol+2]-1)*coeff[col];
                      
                      }
                      if ((icol+2)%2 == 0 && (jcol+2)%2 == 1)
                      {
                      //recombination of bases that has homogenous boundaries
                      v = v + chebsecondderivy[icol+2]*(chebtestx[jcol+2]-xpoint)*coeff[col];
                     
                      }
                      if ((icol+2)%2 == 1 && (jcol+2)%2 == 0)
                      {
                      //recombination of bases that has homogenous boundaries
                      v = v + chebsecondderivy[icol+2]*(chebtestx[jcol+2]-1)*coeff[col];

                      }
                      if ((icol+2)%2 == 1 && (jcol+2)%2 == 1)
                      {
                      //recombination of bases that has homogenous boundaries
                      v = v + chebsecondderivy[icol+2]*(chebtestx[jcol+2]-xpoint)*coeff[col];
                      }
                    }
          }
          ierr = VecSetValues(WestDer, 1, &j, &v, INSERT_VALUES);CHKERRQ(ierr);    
        } 



        if (i == NumPoints-1)
        {
            v = 0;
            for (PetscInt jcol = 0; jcol < NumPoints-2; ++jcol)
            {
                    for (PetscInt icol = 0; icol < NumPoints-2; ++icol)
                    {

                      PetscInt col = icol + jcol*(NumPoints-2);

                      if ((icol+2)%2 == 0 && (jcol+2)%2 == 0)
                      {
                      //recombination of bases that has homogenous boundaries
                      v = v + chebsecondderivy[jcol+2]*(chebtestx[icol+2]-1)*coeff[col];
                      
                      }
                      if ((icol+2)%2 == 0 && (jcol+2)%2 == 1)
                      {
                      //recombination of bases that has homogenous boundaries
                      v = v + chebsecondderivy[jcol+2]*(chebtestx[icol+2]-1)*coeff[col];
                     
                      }
                      if ((icol+2)%2 == 1 && (jcol+2)%2 == 0)
                      {
                      //recombination of bases that has homogenous boundaries
                      v = v + chebsecondderivy[jcol+2]*(chebtestx[icol+2]-xpoint)*coeff[col];

                      }
                      if ((icol+2)%2 == 1 && (jcol+2)%2 == 1)
                      {
                      //recombination of bases that has homogenous boundaries
                      v = v + chebsecondderivy[jcol+2]*(chebtestx[icol+2]-xpoint)*coeff[col];
                      }
                    }
          }
          ierr = VecSetValues(EastDer, 1, &j, &v, INSERT_VALUES);CHKERRQ(ierr);  
        }  
      }
    } 

    //Here we used to scale derivatives because points from big box to patch via mapping M:square -> patch are d_xx f(M(x)) = M''*(f''(M(x)))
    //Now we just encode it in interiorchebmatrix 
    // VecScale(EastDer, PetscPowScalarReal((LowerLeftxcord-UpperRightxcord)/(2.0), 2));
    // VecScale(WestDer, PetscPowScalarReal((LowerLeftxcord-UpperRightxcord)/(2.0), 2));  
    // VecScale(NorthDer, PetscPowScalarReal((LowerLeftycord-UpperRightycord)/(2.0), 2));
    // VecScale(SouthDer, PetscPowScalarReal((LowerLeftycord-UpperRightycord)/(2.0), 2));

  PetscFunctionReturn(0);
}

int main (int argc,char **args)
{
  
  Mat            derivmat, approxmat, resmat;
  //must have at least two points
  PetscInt       NumPoints = 5;
  Vec            conditions, coefficients, approx, East, West, North, South, EastDer, WestDer, NorthDer, SouthDer; 
  Vec            Box, interiorbox, exact, error, res, resbox, exactres, EastRes, WestRes, NorthRes, SouthRes;
  PetscErrorCode ierr;
  PetscScalar norm;
  PetscScalar LowerLeftxcord = -1;
  PetscScalar LowerLeftycord = -1;
  PetscScalar UpperRightxcord = 1;
  PetscScalar UpperRightycord = 1;
  KSP        ksp;
  PC         pc;

  PetscInitialize(&argc,&args,(char*)0,help);



  ierr       = PetscOptionsGetInt(NULL,NULL,"-N",&NumPoints,NULL);CHKERRQ(ierr);


  /* create the matrix */
  ierr = MatCreateSeqDense(PETSC_COMM_WORLD, (NumPoints-2)*(NumPoints-2), (NumPoints-2)*(NumPoints-2), NULL, &derivmat);CHKERRQ(ierr);
  ierr = FormInteriorChebMatrix(derivmat, LowerLeftxcord, LowerLeftycord,  UpperRightxcord,  UpperRightycord, NumPoints);CHKERRQ(ierr);
  ierr = MatAssemblyBegin(derivmat,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(derivmat,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

  //Set BC vectors
  ierr = VecCreate(PETSC_COMM_WORLD,&North);CHKERRQ(ierr);
  ierr = VecSetSizes(North,PETSC_DECIDE,NumPoints);CHKERRQ(ierr);
  ierr = VecSetFromOptions(North);CHKERRQ(ierr);

  ierr = VecCreate(PETSC_COMM_WORLD,&South);CHKERRQ(ierr);
  ierr = VecSetSizes(South,PETSC_DECIDE,NumPoints);CHKERRQ(ierr);
  ierr = VecSetFromOptions(South);CHKERRQ(ierr);

  ierr = VecCreate(PETSC_COMM_WORLD,&East);CHKERRQ(ierr);
  ierr = VecSetSizes(East,PETSC_DECIDE,NumPoints);CHKERRQ(ierr);
  ierr = VecSetFromOptions(East);CHKERRQ(ierr);

  ierr = VecCreate(PETSC_COMM_WORLD,&West);CHKERRQ(ierr);
  ierr = VecSetSizes(West,PETSC_DECIDE,NumPoints);CHKERRQ(ierr);
  ierr = VecSetFromOptions(West);CHKERRQ(ierr);

  ierr = VecCreate(PETSC_COMM_WORLD,&Box);CHKERRQ(ierr);
  ierr = VecSetSizes(Box,PETSC_DECIDE,NumPoints*NumPoints);CHKERRQ(ierr);
  ierr = VecSetFromOptions(Box);CHKERRQ(ierr);

  ierr = VecCreate(PETSC_COMM_WORLD,&NorthDer);CHKERRQ(ierr);
  ierr = VecSetSizes(NorthDer,PETSC_DECIDE,NumPoints);CHKERRQ(ierr);
  ierr = VecSetFromOptions(NorthDer);CHKERRQ(ierr);

  ierr = VecCreate(PETSC_COMM_WORLD,&SouthDer);CHKERRQ(ierr);
  ierr = VecSetSizes(SouthDer,PETSC_DECIDE,NumPoints);CHKERRQ(ierr);
  ierr = VecSetFromOptions(SouthDer);CHKERRQ(ierr);

  ierr = VecCreate(PETSC_COMM_WORLD,&EastDer);CHKERRQ(ierr);
  ierr = VecSetSizes(EastDer,PETSC_DECIDE,NumPoints);CHKERRQ(ierr);
  ierr = VecSetFromOptions(EastDer);CHKERRQ(ierr);

  ierr = VecCreate(PETSC_COMM_WORLD,&WestDer);CHKERRQ(ierr);
  ierr = VecSetSizes(WestDer,PETSC_DECIDE,NumPoints);CHKERRQ(ierr);
  ierr = VecSetFromOptions(WestDer);CHKERRQ(ierr);

  ierr = VecCreate(PETSC_COMM_WORLD,&NorthRes);CHKERRQ(ierr);
  ierr = VecSetSizes(NorthRes,PETSC_DECIDE,NumPoints);CHKERRQ(ierr);
  ierr = VecSetFromOptions(NorthRes);CHKERRQ(ierr);

  ierr = VecCreate(PETSC_COMM_WORLD,&SouthRes);CHKERRQ(ierr);
  ierr = VecSetSizes(SouthRes,PETSC_DECIDE,NumPoints);CHKERRQ(ierr);
  ierr = VecSetFromOptions(SouthRes);CHKERRQ(ierr);

  ierr = VecCreate(PETSC_COMM_WORLD,&EastRes);CHKERRQ(ierr);
  ierr = VecSetSizes(EastRes,PETSC_DECIDE,NumPoints);CHKERRQ(ierr);
  ierr = VecSetFromOptions(EastRes);CHKERRQ(ierr);

  ierr = VecCreate(PETSC_COMM_WORLD,&WestRes);CHKERRQ(ierr);
  ierr = VecSetSizes(WestRes,PETSC_DECIDE,NumPoints);CHKERRQ(ierr);
  ierr = VecSetFromOptions(WestRes);CHKERRQ(ierr);


  ierr = VecCreate(PETSC_COMM_WORLD,&conditions);CHKERRQ(ierr);
  ierr = VecSetSizes(conditions,PETSC_DECIDE,(NumPoints-2)*(NumPoints-2));CHKERRQ(ierr);
  ierr = VecSetFromOptions(conditions);CHKERRQ(ierr);

  ierr = VecCreate(PETSC_COMM_WORLD,&interiorbox);CHKERRQ(ierr);
  ierr = VecSetSizes(interiorbox,PETSC_DECIDE,(NumPoints-2)*(NumPoints-2));CHKERRQ(ierr);
  ierr = VecSetFromOptions(interiorbox);CHKERRQ(ierr);

  ierr = VecCreate(PETSC_COMM_WORLD,&exact);CHKERRQ(ierr);
  ierr = VecSetSizes(exact,PETSC_DECIDE,NumPoints*NumPoints);CHKERRQ(ierr);
  ierr = VecSetFromOptions(exact);CHKERRQ(ierr);


  ierr = VecCreate(PETSC_COMM_WORLD,&exactres);CHKERRQ(ierr);
  ierr = VecSetSizes(exactres,PETSC_DECIDE,(NumPoints-2)*(NumPoints-2));CHKERRQ(ierr);
  ierr = VecSetFromOptions(exactres);CHKERRQ(ierr);


  ierr = FormTestFunctionInterior1(conditions, LowerLeftxcord, LowerLeftycord,  UpperRightxcord,  UpperRightycord, NumPoints);CHKERRQ(ierr);
  ierr = FormTestFunctionInteriorBox1(EastDer, WestDer, NorthDer, SouthDer, LowerLeftxcord, LowerLeftycord,  UpperRightxcord,  UpperRightycord, NumPoints);CHKERRQ(ierr);
  ierr = FormTestFunctionBox1(East,  West,  North,  South,  LowerLeftxcord, LowerLeftycord,  UpperRightxcord,  UpperRightycord, NumPoints); CHKERRQ(ierr);
  ierr = FormExactTestFunction1(exact, LowerLeftxcord, LowerLeftycord,  UpperRightxcord,  UpperRightycord, NumPoints);CHKERRQ(ierr);
  
  ierr = FormInteriorBox(interiorbox, EastDer,  WestDer,  NorthDer,  SouthDer,  LowerLeftxcord, LowerLeftycord,  UpperRightxcord,  UpperRightycord, NumPoints);CHKERRQ(ierr);
  ierr = FormBox(Box, East, West, North, South, LowerLeftxcord, LowerLeftycord,  UpperRightxcord,  UpperRightycord,  NumPoints); CHKERRQ(ierr);

    
    //Put it in formcheb matrix function


  ierr = MatCreateSeqDense(PETSC_COMM_WORLD, (NumPoints-2)*(NumPoints-2), (NumPoints-2)*(NumPoints-2), NULL, &approxmat);CHKERRQ(ierr);
  ierr = FormSolMatrix(approxmat, LowerLeftxcord, LowerLeftycord,  UpperRightxcord,  UpperRightycord,  NumPoints);CHKERRQ(ierr);
    
    //Put it in formcheb matrix function
  ierr = MatAssemblyBegin(approxmat,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(approxmat,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);



 

  ierr = VecAXPY(conditions, -1.0, interiorbox);CHKERRQ(ierr);


  ierr = VecCreate(PETSC_COMM_WORLD,&coefficients);CHKERRQ(ierr);
  ierr = VecSetSizes(coefficients,PETSC_DECIDE,(NumPoints-2)*(NumPoints-2));CHKERRQ(ierr);
  ierr = VecSetFromOptions(coefficients);CHKERRQ(ierr);

  ierr = VecCreate(PETSC_COMM_WORLD,&approx);CHKERRQ(ierr);
  ierr = VecSetSizes(approx,PETSC_DECIDE,(NumPoints-2)*(NumPoints-2));CHKERRQ(ierr);
  ierr = VecSetFromOptions(approx);CHKERRQ(ierr);




  KSPCreate(PETSC_COMM_WORLD,&ksp);


  ierr = KSPSetOperators(ksp,derivmat,derivmat); CHKERRQ(ierr);
  ierr = KSPGetPC(ksp,&pc); CHKERRQ(ierr);
  ierr = PCSetType(pc,PCLU); CHKERRQ(ierr);
  ierr = KSPSetTolerances(ksp,1.e-18,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT);CHKERRQ(ierr);

  ierr = KSPSetFromOptions(ksp);CHKERRQ(ierr);

  ierr = KSPSolve(ksp,conditions,coefficients); CHKERRQ(ierr);
  ierr = MatMult(approxmat, coefficients, approx);CHKERRQ(ierr);

  ierr = SynthesizeBoxCheb(approx, Box, NumPoints);CHKERRQ(ierr);



 // need to change points for function interior1, function interiorbox, functionbox1, interiorbox1, box, approxmat
 // make FormTestFunctionInteriorBox1 FormTestFunctionBox1 add value, not insert
  {
  PetscScalar LowerLeftxcord = .25;
  PetscScalar LowerLeftycord = .25;
  PetscScalar UpperRightxcord = .5;
  PetscScalar UpperRightycord = .5;

  ierr = getfirstPatch(coefficients, LowerLeftxcord, LowerLeftycord,  UpperRightxcord,  UpperRightycord,  NumPoints, East,  West,  North,  South);CHKERRQ(ierr);
  ierr = getfirstPatchDer(coefficients, LowerLeftxcord, LowerLeftycord, UpperRightxcord, UpperRightycord, NumPoints, EastDer, WestDer, NorthDer, SouthDer);CHKERRQ(ierr);
  ierr = getfirstPatchRes(coefficients, LowerLeftxcord, LowerLeftycord, UpperRightxcord, UpperRightycord, NumPoints, EastRes, WestRes, NorthRes, SouthRes);CHKERRQ(ierr);
  
  ierr = FormInteriorChebMatrix(derivmat, LowerLeftxcord, LowerLeftycord, UpperRightxcord, UpperRightycord, NumPoints);CHKERRQ(ierr);
  ierr = MatAssemblyBegin(derivmat,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(derivmat,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = KSPSetOperators(ksp,derivmat,derivmat); CHKERRQ(ierr);



  ierr = FormExactResFunction1(exactres, LowerLeftxcord, LowerLeftycord,  UpperRightxcord,  UpperRightycord, NumPoints);CHKERRQ(ierr);
  ierr = FormTestFunctionInterior1(conditions, LowerLeftxcord, LowerLeftycord,  UpperRightxcord,  UpperRightycord, NumPoints);CHKERRQ(ierr);
  ierr = FormTestFunctionInteriorBox1(EastDer, WestDer, NorthDer, SouthDer, LowerLeftxcord, LowerLeftycord,  UpperRightxcord,  UpperRightycord, NumPoints);CHKERRQ(ierr); 
  ierr = FormTestFunctionBox1(East,  West,  North,  South, LowerLeftxcord, LowerLeftycord,  UpperRightxcord,  UpperRightycord,  NumPoints); CHKERRQ(ierr);
  ierr = FormTestFunctionResBox1(EastDer, WestDer, NorthDer, SouthDer, LowerLeftxcord, LowerLeftycord,  UpperRightxcord,  UpperRightycord, NumPoints);CHKERRQ(ierr); 
  ierr = FormExactTestFunction1(exact, LowerLeftxcord, LowerLeftycord,  UpperRightxcord,  UpperRightycord, NumPoints);CHKERRQ(ierr);

  
  ierr = FormInteriorBox(interiorbox, EastDer,  WestDer,  NorthDer,  SouthDer,  LowerLeftxcord, LowerLeftycord,  UpperRightxcord,  UpperRightycord, NumPoints);CHKERRQ(ierr);



  ierr = VecAXPY(conditions, -1.0, interiorbox);CHKERRQ(ierr);

  ierr = FormBox(Box, East, West, North, South, LowerLeftxcord, LowerLeftycord,  UpperRightxcord,  UpperRightycord, NumPoints); CHKERRQ(ierr);

  
  ierr = FormSolMatrix(approxmat, LowerLeftxcord, LowerLeftycord,  UpperRightxcord,  UpperRightycord, NumPoints);CHKERRQ(ierr);
  ierr = MatAssemblyBegin(approxmat,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(approxmat,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

  ierr = KSPSolve(ksp,conditions,coefficients); CHKERRQ(ierr);


  ierr = MatMult(approxmat, coefficients, approx);CHKERRQ(ierr);


  ierr = SynthesizeBoxCheb(approx, Box, NumPoints);CHKERRQ(ierr);

  ierr = VecCreate(PETSC_COMM_WORLD,&res);CHKERRQ(ierr);
  ierr = VecSetSizes(res,PETSC_DECIDE,(NumPoints-2)*(NumPoints-2));CHKERRQ(ierr);
  ierr = VecSetFromOptions(res);CHKERRQ(ierr);



  ierr = VecCreate(PETSC_COMM_WORLD,&resbox);CHKERRQ(ierr);
  ierr = VecSetSizes(resbox,PETSC_DECIDE,(NumPoints-2)*(NumPoints-2));CHKERRQ(ierr);
  ierr = VecSetFromOptions(resbox);CHKERRQ(ierr);
  ierr = FormInteriorResBox(resbox, EastRes, WestRes, NorthRes, SouthRes, LowerLeftxcord, LowerLeftycord, UpperRightxcord, UpperRightycord, NumPoints);CHKERRQ(ierr);
  

  

  ierr = MatCreateSeqDense(PETSC_COMM_WORLD, (NumPoints-2)*(NumPoints-2), (NumPoints-2)*(NumPoints-2), NULL, &resmat);CHKERRQ(ierr);
  ierr = FormResMatrix(resmat, LowerLeftxcord, LowerLeftycord, UpperRightxcord, UpperRightycord, NumPoints);CHKERRQ(ierr);
  ierr = MatAssemblyBegin(resmat,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(resmat,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatMult(resmat, coefficients, res);


  // ierr = VecView(res, PETSC_VIEWER_STDOUT_SELF);
  // ierr = VecView(resbox, PETSC_VIEWER_STDOUT_SELF);

  ierr = VecAXPY(res, 1.0, resbox);CHKERRQ(ierr);
  }




  ierr = VecCreate(PETSC_COMM_WORLD,&error);CHKERRQ(ierr);
  ierr = VecSetSizes(error,PETSC_DECIDE,NumPoints*NumPoints);CHKERRQ(ierr);
  ierr = VecSetFromOptions(error);CHKERRQ(ierr);

  ierr = VecWAXPY(error, -1, Box, exact);CHKERRQ(ierr);
  
  ierr = VecNorm(error,NORM_2,&norm);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Norm of error vector %g\n",(double)norm/(NumPoints*NumPoints));CHKERRQ(ierr);

  

  ierr = VecAXPY(res, -1.0, exactres);CHKERRQ(ierr);
  ierr = VecNorm(res,NORM_2,&norm);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Norm of res vector %g\n",(double)norm/((NumPoints-2)*NumPoints-2));CHKERRQ(ierr);

  KSPDestroy(&ksp); 
  VecDestroy(&coefficients);
  VecDestroy(&error);
  VecDestroy(&conditions);  
  VecDestroy(&approx);
  VecDestroy(&Box);
  VecDestroy(&interiorbox);
  VecDestroy(&North);
  VecDestroy(&East);
  VecDestroy(&West);
  VecDestroy(&South);
  VecDestroy(&NorthDer);
  VecDestroy(&EastDer);
  VecDestroy(&WestDer);
  VecDestroy(&SouthDer);
  VecDestroy(&NorthRes);
  VecDestroy(&EastRes);
  VecDestroy(&WestRes);
  VecDestroy(&SouthRes);
  VecDestroy(&exact);
  VecDestroy(&exactres);
  VecDestroy(&res);
  VecDestroy(&resbox);

  MatDestroy(&derivmat);
  MatDestroy(&resmat);
  MatDestroy(&approxmat);


  // PetscScalar point = 0;
  // intervalShift(-1,  1, -.5, .5,  &point);
  // printf("shifted 1 from [0, 1] to [0, 2]: %f", point);

PetscFinalize();
return 0;
  
}

