#include <fftw3.h>
#include <complex.h>
#include <petscmat.h>
#include <math.h>

// int main(int argc, char **args)
// {
//     int N = 5;
//     fftw_complex *in, *out;
//     fftw_plan p;

//     fftw_complex input[5] = {{1, 0}, {1, 0}, {1, 0}, {1, 0}, {1, 0}};
//     in = input;
//     //...
//     out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
//     p = fftw_plan_dft_1d(N, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
//     //...
//     fftw_execute(p); /* repeat as needed */
//     //...
//     for (int i = 0; i < 4; i++ ) {
//       printf("*(out + %d) :"  "%f + i%f \n", i, out[i][0], out[i][1]);
//    }
//     fftw_destroy_plan(p);


//     fftw_free(out);
//     return 0;
// }

void setTestInput1(double *in, const int N)
{
  


  for (int j = 0; j < N; ++j)
  {
   *(in+j) = PetscSinReal(PETSC_PI*(j+1)/(N+1));
}

  for (int i = 0; i < N; ++i ) {
      printf("*(in + %d) :"  "%f \n", i, in[i]);
   }
}


void setTestInput2(double *in, const int N)
{
  


  for (int j = 0; j < N; ++j)
  {
   *(in+j) = pow((double)(j+1)/(N+1),2);
}

  for (int i = 0; i < N; ++i ) {
      printf("*(in + %d) :"  "%f \n", i, in[i]);
   }
}

int main(int argc, char **args)
{
    const int N = 10;
    double *in, *out;
    fftw_plan p, pinv;

    in = (double*) fftw_malloc(sizeof(double) * N);
    setTestInput1(in, N);
    //fftw output returns integral of function times sin(j*pi*x)*(N+1)
    out = (double*) fftw_malloc(sizeof(double) * N);
    p = fftw_plan_r2r_1d(N, in, out, FFTW_RODFT00, FFTW_ESTIMATE);
    //...
    fftw_execute(p); /* repeat as needed */
    //...
    //to renormalize, must divided by 2(N+1)
    for (int i = 0; i < N; ++i ) {
      printf("*(out + %d) :"  "%f \n", i, out[i]);
   }
  
    pinv = fftw_plan_r2r_1d(N, out, in, FFTW_RODFT00, FFTW_ESTIMATE);
    fftw_execute(pinv);
    for (int i = 0; i < N; ++i ) {
      printf("*(in + %d) :"  "%f \n", i, in[i]);
   }

    fftw_destroy_plan(p);
    fftw_destroy_plan(pinv);


    fftw_free(out);
    return 0;
}


//http://www.fftw.org/doc/Real_002dto_002dReal-Transforms.html#index-r2r-1
//http://www.fftw.org/doc/Real_002dto_002dReal-Transform-Kinds.html#Real_002dto_002dReal-Transform-Kinds
