#! /usr/bin/env python
import os
import subprocess

error = [ ]
samplesize = []



for k in range ( 5 ) :
    Nx = 10*2**k + 1
    options = ['-N ', str(Nx)]
    error.append(os.system('./testfft '+' '.join(options)))
    samplesize.append(Nx)
print zip(error, samplesize)



from pylab import legend, plot, loglog, show, title, xlabel, ylabel, savefig
plot(samplesize, error, '--k')
legend(['fft accuracy'])
title('fft')
xlabel('Problem Size $N$')
ylabel('Error (N)')
show()

#implement line search for error

