#! /usr/bin/env python
import os
import subprocess

error = [ ]
samplesize = []
highreserror = []
reserror = []
work = []
compare = []



# for k in range ( 1 ) :
#     Nx = 2**k + 1
#     options = ['-N ', str(Nx)]
#     error.append(os.system('./boundaryvalues '+' '.join(options)))
#     samplesize.append(Nx)
# print zip(error, samplesize)



for k in range ( 5) :
	Nx = 7*k + 6
	cmd = './chebsolve2d -N ' + str(Nx)
	out = subprocess.check_output(cmd, shell=True) 
	splitstring = out.split("\n")
	for linenumber, lines in enumerate(splitstring):
		start = (lines.find("error vector"))
		if start > 0:
				print(float(lines[start + len("error vector "):]))
				error.append(float(lines[start + len("error vector "):]))
				compare.append(1.0/Nx)
				samplesize.append(Nx)
				print(linenumber, lines)

		start = (lines.find("res vector"))
		if start > 0:
				print(float(lines[start + len("res vector "):]))
				reserror.append(float(lines[start + len("res vector "):]))
				work.append(Nx**6)
				print(linenumber, lines)

from pylab import legend, plot, loglog, show, title, xlabel, ylabel, savefig
#plot(samplesize, error, '--k')
loglog(samplesize, error, '--k', samplesize, compare, 'r--')
legend(['cheby accuracy'])
title('cheby')
xlabel('Problem Size $N$')
ylabel('Error (N)')
show()

#plot(work, reserror, '--k')
loglog(work, reserror, '--k')
legend(['cheby precision'])
title('cheby')
xlabel('Problem Size $N$')
ylabel('Error (N)')
show()

# plot(samplesize, highreserror, '--k')
# legend(['fft accuracy'])
# title('fft')
# xlabel('Problem Size $N$')
# ylabel('High Resolution Error (N)')
# show()
#loglog(sizes, times)
#title('SNES ex5')
#xlabel('Problem Size $N$')
#ylabel('Time (s)')
#show()


