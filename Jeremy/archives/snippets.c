
    //printf("The integral of the error function in half of total region is: %f\n", integral);
    //printf("Region needed is %f \n", BufferNeeded(currentpatch, test, eps, Num_Quad_Points));
    //printf("Region is (%f, %f)x(%f, %f) \n", currentpatch->LLxcoord, currentpatch->URxcoord, currentpatch->LLycoord, currentpatch->URycoord);
    PlotPatchPortion(coarse, -1, 1, -1, 1, resolution, "fourierheatmapcoarse.png");
    PlotErrorPortion(coarse, -1, 1, -1, 1, resolution, "fouriererrormapcoarse.png", test);
    //PlotPatchPortion(coarse, -1, 0, -1, 0, resolution, "fourierheatpatchcoarse.png");
    //PlotErrorPortion(coarse, -1, 0, -1, 0, resolution, "fouriererrorpatchcoarse.png", test);
    PlotPatch(coarse->lowerleft, resolution, "LLmapfourier.png");
    PlotError(coarse->lowerleft, resolution, "LLmaperrorfourier.png", test);
    
    PatchError(coarse, test, Num_Quad_Points, &integral);
    printf("The integral of the error in coarse is: %f\n", integral);
    
    PatchError(coarse->lowerleft, test, Num_Quad_Points, &integral);
    printf("The integral of the error in LL is: %f\n", integral);


PlotPatchPortion(coarse, -1, 1, -1, 1, resolution, "chebheatmapcoarse.png");
PlotErrorPortion(coarse, -1, 1, -1, 1, resolution, "cheberrormapcoarse.png", test);
//PlotPatchPortion(coarse, -1, 0, -1, 0, resolution, "chebheatpatchcoarse.png");
//PlotErrorPortion(coarse, -1, 0, -1, 0, resolution, "cheberrorpatchcoarse.png", test);
PlotPatch(coarse->lowerleft, resolution, "LLmapcheb.png");
PlotError(coarse->lowerleft, resolution, "LLmaperrorcheb.png", test);
    
PetscScalar integral = 0;
PatchError(coarse, test, Num_Quad_Points, &integral);
printf("The integral of the error in coarse is: %f\n", integral);

PatchError(coarse->lowerleft, test, Num_Quad_Points, &integral);
printf("The integral of the error in LL is: %f\n", integral);

    
