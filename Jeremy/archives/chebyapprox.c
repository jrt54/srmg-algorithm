#include <petscmat.h>
#include <stdio.h>
#include <petscsys.h>
#include <petscdm.h>
#include <petscdmda.h>
#include <petscsnes.h>
#include <petscmatlab.h>
#include <fftw3.h>
static char help[] = "Solving Laplace via Chebyshev";

double *Chebpoly(double point, int degN)
{       

       
		double *chebfunceval = malloc(sizeof(double) * (degN+1)); 

        for (int i=0; i < degN+1; ++i) {
                if (i == 0)
                {
                	chebfunceval[i] = 1;
                }
                else if (i == 1)
                {
                	chebfunceval[i] = point;
                }
                else
                {
                	chebfunceval[i] = 2*point*chebfunceval[i-1] - chebfunceval[i-2];

        		}
        }

        return chebfunceval;
}

double *Chebderiv(double point, int degN, double *chebfunceval)
{       

       
		double *chebderiv = malloc(sizeof(double) * (degN+1)); 

        for (int i=0; i < degN + 1; ++i) {
                if (i == 0)
                {
                	chebderiv[i] = 0;
                }
                else if (i == 1)
                {
                	chebderiv[i] = 1;
                }
                else if (i==2)
                {
                	chebderiv[i] = 4*point;
                }
                else
                //Tk' = (-k*x*Tk(x) + k*T_(k-1)(x))/(1-x^2)
                {
                	chebderiv[i] = i*(2*chebfunceval[i-1] + chebderiv[i-2]/(i-2));

        		}
        }

        return chebderiv;
}


double *Chebsecondderiv(double point, int degN, double *chebderiv)
{       

       
		double *chebsecondderiv = malloc(sizeof(double) * (degN+1)); 

        for (int i=0; i < degN + 1; ++i) {
                if (i == 0)
                {
                	chebsecondderiv[i] = 0;
                }
                else if (i == 1)
                {
                	chebsecondderiv[i] = 0;
                }
                else if (i==2)
                {
                	chebsecondderiv[i] = 4;
                }
                else
                //Tk'; = (-k*x*Tk;(x) + k*T_(k-1)''(x))/(1-x^2)
                {
                	chebsecondderiv[i] = i*(2*chebderiv[i-1] + chebsecondderiv[i-2]/(i-2));

        		}
        }

        return chebsecondderiv;
}

//PetscErrorCode FormChebMatrix(Mat A, Vec Points, PetscInt NumBases, PetscInt NumPoints)
PetscErrorCode FormChebMatrix(Mat A, PetscInt NumBases, PetscInt NumPoints)
{
  PetscErrorCode ierr;
	for (PetscInt i = 0; i < NumPoints; ++i)
	{
	//generalize this to any set of points later
	double point = PetscCosReal(PETSC_PI*(double)i/(NumPoints - 1));
	double *chebtest = Chebpoly(point, NumBases);
	double *chebderiv = Chebderiv(point, NumBases, chebtest);
	double *chebsecondderiv = Chebsecondderiv(point, NumBases, chebderiv);
		for (PetscInt j = 0; j < NumPoints; ++j)
		{
			if (i == 0 || i == NumPoints - 1) 
			{
			PetscScalar v = chebtest[j];
			ierr = MatSetValues(A, 1, &i, 1, &j, &v, INSERT_VALUES);CHKERRQ(ierr);
			}
			else
			{
			//definition of derivative in terms of U
			PetscScalar v = chebsecondderiv[j];
			ierr = MatSetValues(A, 1, &i, 1, &j, &v, INSERT_VALUES);CHKERRQ(ierr);
			}
		}
	}
	PetscFunctionReturn(0);
}

//PetscErrorCode FormTestFunction1(Vec input, PetscInt NumPoints, Points[])
PetscErrorCode FormTestFunction1(Vec input, PetscInt NumPoints)
  {
    PetscErrorCode ierr;
    PetscScalar    v;
    PetscInt j;

     

    for (j = 0; j < NumPoints; j++)
    {

    double  point = PetscCosReal(PETSC_PI*(double)j/(NumPoints - 1));
      if (j == 0 || j == NumPoints - 1)
      {
        v = point;
      }
      //boundary function is x evaluated at Cheby points
      else
      {
        v = 0;
      }
      ierr = VecSetValues(input, 1, &j, &v, INSERT_VALUES);CHKERRQ(ierr);    
    }

    
 
      PetscFunctionReturn(0);
}

PetscErrorCode FormTestFunction2(Vec input, PetscInt NumPoints)
{
    PetscErrorCode ierr;
    PetscScalar    v;
    PetscInt j;

         

    for (j = 0; j < NumPoints; j++)
    {
      double point = PetscCosReal(PETSC_PI*(double)j/(NumPoints - 1));
      if (j == 0 || j == NumPoints - 1)
      {
        v = PetscSinReal(PETSC_PI*point);
      }
      else 
      {
        v = -PETSC_PI*PETSC_PI*PetscSinReal(PETSC_PI*point);
      }  



      ierr = VecSetValues(input, 1, &j, &v, INSERT_VALUES);CHKERRQ(ierr);    
    }

    
 
      PetscFunctionReturn(0);
}

PetscErrorCode FormTestFunction3(Vec input, PetscInt NumPoints)
  {
    PetscErrorCode ierr;
    PetscScalar    v;
    PetscInt j;

     

    for (j = 0; j < NumPoints; j++)
    {

    double  point = PetscCosReal(PETSC_PI*(double)j/(NumPoints - 1));
      if (j == 0 || j == NumPoints - 1)
      {
        v = point*point*point*point*point;
      }
      //boundary function is x evaluated at Cheby points
      else
      {
        v = 20*point*point*point;
      }
      ierr = VecSetValues(input, 1, &j, &v, INSERT_VALUES);CHKERRQ(ierr);    
    }

    
 
      PetscFunctionReturn(0);
}

PetscErrorCode FormExactTestFunction1(Vec input, PetscInt NumPoints)
{
    PetscErrorCode ierr;
    PetscScalar    v;
    PetscInt j;

     

    for (j = 0; j < NumPoints; j++)
    {
      double point = 2*(double)j/(NumPoints - 1)-1;
      v = point;
      

      ierr = VecSetValues(input, 1, &j, &v, INSERT_VALUES);CHKERRQ(ierr);    
    }

    
 
      PetscFunctionReturn(0);
}



PetscErrorCode FormExactTestFunction2(Vec input, PetscInt NumPoints)
{
    PetscErrorCode ierr;
    PetscScalar    v;
    PetscInt j;

     

    for (j = 0; j < NumPoints; j++)
    {
      double point = 2*(double)j/(NumPoints - 1) - 1;
      v = PetscSinReal(PETSC_PI*point);
      


      ierr = VecSetValues(input, 1, &j, &v, INSERT_VALUES);CHKERRQ(ierr);    
    }
    
 
      PetscFunctionReturn(0);
}

PetscErrorCode FormExactTestFunction3(Vec input, PetscInt NumPoints)
{
    PetscErrorCode ierr;
    PetscScalar    v;
    PetscInt j;

     

    for (j = 0; j < NumPoints; j++)
    {
      double point = 2*(double)j/(NumPoints - 1)-1;
      v = point*point*point*point*point;
      

      ierr = VecSetValues(input, 1, &j, &v, INSERT_VALUES);CHKERRQ(ierr);    
    }

    
 
      PetscFunctionReturn(0);
}




PetscErrorCode FormSol(Vec Sol, Vec Coefficients, PetscInt NumPoints)
{

const PetscScalar *coeffspace;
PetscErrorCode ierr;
PetscScalar v;

ierr = VecGetArrayRead(Coefficients, &coeffspace);CHKERRQ(ierr);
for (PetscInt j = 0; j < NumPoints; ++j)
{
    //generalize this to any set of points later
    double point = 2*(double)j/(NumPoints - 1)-1;
    double *chebtest = Chebpoly(point, NumPoints);
    v = 0;
    for (PetscInt i = 0; i<NumPoints; ++i)
    {
    v += chebtest[i]*coeffspace[i] ; 
    }
    ierr = VecSetValues(Sol, 1, &j, &v, INSERT_VALUES);CHKERRQ(ierr); 
}

    PetscFunctionReturn(0);

}

int main (int argc,char **args)
{
	PetscReal      norm;
	Mat            mat;
	//must have at least two points
 	PetscInt       NumBases, NumPoints = 15;
 	Vec            conditions, coefficients, solution, exact;
 	PetscErrorCode ierr;
 	KSP 		   ksp;
 	PC 			   pc;

  
	PetscInitialize(&argc,&args,(char*)0,help);
  ierr = PetscOptionsGetInt(NULL,NULL,"-N",&NumPoints,NULL);CHKERRQ(ierr);
  NumBases = NumPoints;
	/* create the matrix */
	ierr = MatCreateSeqDense(PETSC_COMM_WORLD, NumPoints, NumPoints, NULL, &mat);CHKERRQ(ierr);


	ierr = FormChebMatrix(mat, NumBases, NumPoints);CHKERRQ(ierr);

    
  //Put it in formcheb matrix function
	ierr = MatAssemblyBegin(mat,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
 	ierr = MatAssemblyEnd(mat,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

	//ierr = MatView(mat, PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr);

	ierr = VecCreate(PETSC_COMM_WORLD,&conditions);CHKERRQ(ierr);
  ierr = VecSetSizes(conditions,PETSC_DECIDE,NumPoints);CHKERRQ(ierr);
  ierr = VecSetFromOptions(conditions);CHKERRQ(ierr);
  ierr = FormTestFunction2(conditions, NumPoints);CHKERRQ(ierr);

  ierr = VecCreate(PETSC_COMM_WORLD,&coefficients);CHKERRQ(ierr);
  ierr = VecSetSizes(coefficients,PETSC_DECIDE,NumPoints);CHKERRQ(ierr);
  ierr = VecSetFromOptions(coefficients);CHKERRQ(ierr);

  //ksp set from options, pc type lu
  	

  KSPCreate(PETSC_COMM_WORLD,&ksp);


  KSPSetOperators(ksp,mat,mat);
  KSPGetPC(ksp,&pc);
  PCSetType(pc,PCLU);
  ierr = KSPSetTolerances(ksp,1.e-5,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT);CHKERRQ(ierr);

  KSPSetFromOptions(ksp);

  KSPSolve(ksp,conditions,coefficients);


  ierr = VecCreate(PETSC_COMM_WORLD,&solution);CHKERRQ(ierr);
  ierr = VecSetSizes(solution,PETSC_DECIDE,NumPoints);CHKERRQ(ierr);
  ierr = VecSetFromOptions(solution);CHKERRQ(ierr);
  ierr = FormSol(solution, coefficients, NumPoints);CHKERRQ(ierr);

  ierr = VecCreate(PETSC_COMM_WORLD,&exact);CHKERRQ(ierr);
  ierr = VecSetSizes(exact,PETSC_DECIDE,NumPoints);CHKERRQ(ierr);
  ierr = VecSetFromOptions(exact);CHKERRQ(ierr);
  ierr = FormExactTestFunction2(exact, NumPoints);CHKERRQ(ierr);


  // ierr = VecView(conditions, PETSC_VIEWER_STDOUT_WORLD); CHKERRQ(ierr);
  //ierr = VecView(coefficients, PETSC_VIEWER_STDOUT_WORLD); CHKERRQ(ierr);
   ierr = VecView(solution, PETSC_VIEWER_STDOUT_WORLD); CHKERRQ(ierr);
  // ierr = VecView(exact, PETSC_VIEWER_STDOUT_WORLD); CHKERRQ(ierr);

  ierr = VecAXPY(solution, -1.0, exact);CHKERRQ(ierr);
  ierr = VecNorm(solution,NORM_2,&norm);CHKERRQ(ierr);
  //ierr = VecView(solution, PETSC_VIEWER_STDOUT_WORLD); CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Norm of error vector %g\n",(double)norm);CHKERRQ(ierr);

  KSPDestroy(&ksp); 
  VecDestroy(&coefficients);
  VecDestroy(&conditions);  
  MatDestroy(&mat);
  VecDestroy(&exact);
  VecDestroy(&solution);



PetscFinalize();
return 0;
  
}