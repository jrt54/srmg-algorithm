#include <petscmat.h>
#include <stdio.h>
#include <petscsys.h>
#include <petscdm.h>
#include <petscdmda.h>
#include <petscsnes.h>
#include <petscmatlab.h>
#include <fftw3.h>
static char help[] = "Solving Laplace via Chebyshev";

double *Chebpoly(double point, int degN)
{       

       
		double *chebfunceval = malloc(sizeof(double) * (degN+1)); 

        for (int i=0; i < degN+1; ++i) {
                if (i == 0)
                {
                	chebfunceval[i] = 1;
                }
                else if (i == 1)
                {
                	chebfunceval[i] = point;
                }
                else
                {
                	chebfunceval[i] = 2*point*chebfunceval[i-1] - chebfunceval[i-2];

        		}
        }

        return chebfunceval;
}

double *Chebderiv(double point, int degN, double *chebfunceval)
{       

       
		double *chebderiv = malloc(sizeof(double) * (degN+1)); 

        for (int i=0; i < degN + 1; ++i) {
                if (i == 0)
                {
                	chebderiv[i] = 0;
                }
                else if (i == 1)
                {
                	chebderiv[i] = 1;
                }
                else if (i==2)
                {
                	chebderiv[i] = 4*point;
                }
                else
                //Tk' = (-k*x*Tk(x) + k*T_(k-1)(x))/(1-x^2)
                {
                	chebderiv[i] = i*(2*chebfunceval[i-1] + chebderiv[i-2]/(i-2));

        		}
        }

        return chebderiv;
}


double *Chebsecondderiv(double point, int degN, double *chebderiv)
{       

       
		double *chebsecondderiv = malloc(sizeof(double) * (degN+1)); 

        for (int i=0; i < degN + 1; ++i) {
                if (i == 0)
                {
                	chebsecondderiv[i] = 0;
                }
                else if (i == 1)
                {
                	chebsecondderiv[i] = 0;
                }
                else if (i==2)
                {
                	chebsecondderiv[i] = 4;
                }
                else
                //Tk'; = (-k*x*Tk;(x) + k*T_(k-1)''(x))/(1-x^2)
                {
                	chebsecondderiv[i] = i*(2*chebderiv[i-1] + chebsecondderiv[i-2]/(i-2));

        		}
        }

        return chebsecondderiv;
}

//PetscErrorCode FormChebMatrix(Mat A, Vec Points, PetscInt NumBases, PetscInt NumPoints)
PetscErrorCode FormChebMatrix(Mat A, PetscInt NumPoints)
{
PetscErrorCode ierr;
for (PetscInt j = 0; j < NumPoints; ++j)
{
    //generate each row
	for (PetscInt i = 0; i < NumPoints; ++i)
	{
  PetscInt row = i + j*NumPoints;
	// //generalize this to any set of points later
  double xpoint = -PetscCosReal(PETSC_PI*(double)i/(NumPoints - 1));
  double ypoint = -PetscCosReal(PETSC_PI*(double)j/(NumPoints - 1));

	// double xpoint = 2*(double)i/(NumPoints - 1)-1;
 //  double ypoint = 2*(double)j/(NumPoints - 1)-1;
	double *chebtestx = Chebpoly(xpoint, NumPoints);
  double *chebtesty = Chebpoly(ypoint, NumPoints);
	double *chebderivx = Chebderiv(xpoint, NumPoints, chebtestx);
  double *chebderivy = Chebderiv(ypoint, NumPoints, chebtesty);
	double *chebsecondderivx = Chebsecondderiv(xpoint, NumPoints, chebderivx);
	double *chebsecondderivy = Chebsecondderiv(ypoint, NumPoints, chebderivy);
    for (PetscInt jcol = 0; jcol < NumPoints; ++jcol)
		{
            for (PetscInt icol = 0; icol < NumPoints; ++icol)
            {
                PetscInt col = icol + jcol*NumPoints;
                //if (condition) == border point
                if (i == 0 || i == NumPoints-1 || j == 0 || j == NumPoints-1)
                {
                PetscScalar v = chebtestx[icol]*chebtesty[jcol];
          			ierr = MatSetValues(A, 1, &row, 1, &col, &v, INSERT_VALUES);CHKERRQ(ierr);
          			}
            
      			    else
          			{
          			//definition of second derivative in terms of U
          			PetscScalar v = chebsecondderivy[jcol]*chebtestx[icol] + chebsecondderivx[icol]*chebtesty[jcol] ;
          			ierr = MatSetValues(A, 1, &row, 1, &col, &v, INSERT_VALUES);CHKERRQ(ierr);
          			}
		    }
	    }
    }
}
	PetscFunctionReturn(0);
}

PetscErrorCode FormChebResMatrix(Mat A, PetscInt NumPoints)
{
PetscErrorCode ierr;
for (PetscInt j = 0; j < NumPoints; ++j)
{
    //generate each row
  for (PetscInt i = 0; i < NumPoints; ++i)
  {
  PetscInt row = i + j*NumPoints;
  // //generalize this to any set of points later
  double xpoint = 2*(double)i/(NumPoints - 1)-1;
  double ypoint = 2*(double)j/(NumPoints - 1)-1;
  

  // double xpoint = 2*(double)i/(NumPoints - 1)-1;
 //  double ypoint = 2*(double)j/(NumPoints - 1)-1;
  double *chebtestx = Chebpoly(xpoint, NumPoints);
  double *chebtesty = Chebpoly(ypoint, NumPoints);
  double *chebderivx = Chebderiv(xpoint, NumPoints, chebtestx);
  double *chebderivy = Chebderiv(ypoint, NumPoints, chebtesty);
  double *chebsecondderivx = Chebsecondderiv(xpoint, NumPoints, chebderivx);
  double *chebsecondderivy = Chebsecondderiv(ypoint, NumPoints, chebderivy);
    for (PetscInt jcol = 0; jcol < NumPoints; ++jcol)
    {
            for (PetscInt icol = 0; icol < NumPoints; ++icol)
            {
                PetscInt col = icol + jcol*NumPoints;
                //if (condition) == border point
                if (i == 0 || i == NumPoints-1 || j == 0 || j == NumPoints-1)
                {
                PetscScalar v = 0;
                ierr = MatSetValues(A, 1, &row, 1, &col, &v, INSERT_VALUES);CHKERRQ(ierr);
                }
            
                else
                {
                //definition of second derivative in terms of U
                PetscScalar v = chebsecondderivy[jcol]*chebtestx[icol] + chebsecondderivx[icol]*chebtesty[jcol] ;
                ierr = MatSetValues(A, 1, &row, 1, &col, &v, INSERT_VALUES);CHKERRQ(ierr);
                }
        }
      }
    }
}
  PetscFunctionReturn(0);
}


//PetscErrorCode FormTestFunction1(Vec input, PetscInt NumPoints, Points[])
PetscErrorCode FormTestFunction1(Vec input, PetscInt NumPoints)
{
    PetscErrorCode ierr;
    PetscScalar    v;


     
for (PetscInt j = 0; j < NumPoints; ++j)
{
    for (PetscInt i = 0; i < NumPoints; ++i)
    {

    PetscInt row = i + j*NumPoints;
      if (i == 0 || i == NumPoints-1 || j == 0 || j == NumPoints-1)
      {
        v = 1;
      }
      //boundary function is 1
      else
      {
        v = 0;
      }
      ierr = VecSetValues(input, 1, &row, &v, INSERT_VALUES);CHKERRQ(ierr);    
    }

}   
 
      PetscFunctionReturn(0);
}

PetscErrorCode FormTestFunction2(Vec input, PetscInt NumPoints)
{
    PetscErrorCode ierr;
    PetscScalar    v;


     
    for (PetscInt j = 0; j < NumPoints; ++j)
    {
    double ypoint = -PetscCosReal(PETSC_PI*(double)j/(NumPoints - 1));
        for (PetscInt i = 0; i < NumPoints; ++i)
        {
        double xpoint = -PetscCosReal(PETSC_PI*(double)i/(NumPoints - 1));

        PetscInt row = i + j*NumPoints;
          if (i == 0 || i == NumPoints-1 || j == 0 || j == NumPoints-1)
          {
            v = PetscSinReal(PETSC_PI*xpoint)*PetscSinReal(PETSC_PI*ypoint);
          }
          //boundary function is sin\pix*sin\pi*y
          else
          {
            //dxx*u(y) + dyy*u(x)
            v = -PETSC_PI*PETSC_PI*(PetscSinReal(PETSC_PI*xpoint)*PetscSinReal(PETSC_PI*ypoint)+PetscSinReal(PETSC_PI*ypoint)*PetscSinReal(PETSC_PI*xpoint));
          }
          ierr = VecSetValues(input, 1, &row, &v, INSERT_VALUES);CHKERRQ(ierr);    
        }

    }   
     
          PetscFunctionReturn(0);
}

PetscErrorCode FormExactTestFunction1(Vec input, PetscInt NumPoints)
{
    PetscErrorCode ierr;
    PetscScalar    v;


     
for (PetscInt j = 0; j < NumPoints; ++j)
{

    for (PetscInt i = 0; i < NumPoints; ++i)
    {
    PetscInt row = i + j*NumPoints;
    
    v = 1;
   
    
    ierr = VecSetValues(input, 1, &row, &v, INSERT_VALUES);CHKERRQ(ierr);    
    }

}   
 
      PetscFunctionReturn(0);
}


PetscErrorCode FormExactTestFunction2(Vec input, PetscInt NumPoints)
{
    PetscErrorCode ierr;
    PetscScalar    v;


for (PetscInt j = 0; j < NumPoints; ++j)
{
double ypoint = 2*(double)j/(NumPoints - 1)-1;
    for (PetscInt i = 0; i < NumPoints; ++i)
    {
    double xpoint = 2*(double)i/(NumPoints - 1)-1;
    PetscInt row = i + j*NumPoints;
    
      
 
    v =  PetscSinReal(PETSC_PI*xpoint)*PetscSinReal(PETSC_PI*ypoint);
      

      

   
    
    ierr = VecSetValues(input, 1, &row, &v, INSERT_VALUES);CHKERRQ(ierr);    
    }

}   
 
      PetscFunctionReturn(0);
}

PetscErrorCode FormResFunction2(Vec input, PetscInt NumPoints)
{
    PetscErrorCode ierr;
    PetscScalar    v;


for (PetscInt j = 0; j < NumPoints; ++j)
{
double ypoint = 2*(double)j/(NumPoints - 1)-1;
    for (PetscInt i = 0; i < NumPoints; ++i)
    {
    double xpoint = 2*(double)i/(NumPoints - 1)-1;
    PetscInt row = i + j*NumPoints;
    
      
    if (i == 0 || i == NumPoints-1 || j == 0 || j == NumPoints-1)
    {
      v = 0;
    }
    else
    {
      v = -PETSC_PI*PETSC_PI*(PetscSinReal(PETSC_PI*xpoint)*PetscSinReal(PETSC_PI*ypoint)+PetscSinReal(PETSC_PI*ypoint)*PetscSinReal(PETSC_PI*xpoint));
    }  

      

   
    
    ierr = VecSetValues(input, 1, &row, &v, INSERT_VALUES);CHKERRQ(ierr);    
    }

}   
 
      PetscFunctionReturn(0);
}

//solution is x*y
PetscErrorCode FormTestFunction3(Vec input, PetscInt NumPoints)
{
    PetscErrorCode ierr;
    PetscScalar    v;


     
for (PetscInt j = 0; j < NumPoints; ++j)
{
    for (PetscInt i = 0; i < NumPoints; ++i)
    {
    //scans across then up
    double xpoint = PetscCosReal(PETSC_PI*(double)i/(NumPoints - 1));;
    double ypoint = PetscCosReal(PETSC_PI*(double)j/(NumPoints - 1));;
    PetscInt row = i + j*NumPoints;

      if (i == NumPoints-1 || j == NumPoints - 1 || i == 0 || j == 0)
      {
        v = xpoint*ypoint;
      }

      else
      {
        v = 0;
      }
      ierr = VecSetValues(input, 1, &row, &v, INSERT_VALUES);CHKERRQ(ierr);    
    }

}   
 
      PetscFunctionReturn(0);
}



PetscErrorCode FormExactTestFunction3(Vec input, PetscInt NumPoints)
{
    PetscErrorCode ierr;
    PetscScalar    v;


for (PetscInt j = 0; j < NumPoints; ++j)
{
double ypoint = -PetscCosReal(PETSC_PI*(double)j/(NumPoints - 1));
    for (PetscInt i = 0; i < NumPoints; ++i)
    {
    double xpoint = -PetscCosReal(PETSC_PI*(double)i/(NumPoints - 1));
    PetscInt row = i + j*NumPoints;
    
      
 
    v =  xpoint*ypoint;
      

      

   
    
    ierr = VecSetValues(input, 1, &row, &v, INSERT_VALUES);CHKERRQ(ierr);    
    }

}   
 
      PetscFunctionReturn(0);
}

//solution is x*y
PetscErrorCode FormTestFunction4(Vec input, PetscInt NumPoints)
{
    PetscErrorCode ierr;
    PetscScalar    v;


     
for (PetscInt j = 0; j < NumPoints; ++j)
{
    for (PetscInt i = 0; i < NumPoints; ++i)
    {
    //scans across then up
    double xpoint = -PetscCosReal(PETSC_PI*(double)i/(NumPoints - 1));;
    double ypoint = -PetscCosReal(PETSC_PI*(double)j/(NumPoints - 1));;
    PetscInt row = i + j*NumPoints;

      if (i == NumPoints-1 || j == NumPoints - 1 || i == 0 || j == 0)
      {
        v =(32*PetscPowReal(xpoint,6)-48*PetscPowReal(xpoint,4)+18*PetscPowReal(xpoint,2)-1)*ypoint;
      }

      else
      {
        v =(32*6*5*PetscPowReal(xpoint,4)-48*4*3*PetscPowReal(xpoint,2)+18*2)*ypoint; 
      }
      ierr = VecSetValues(input, 1, &row, &v, INSERT_VALUES);CHKERRQ(ierr);    
    }

}   
 
      PetscFunctionReturn(0);
}



PetscErrorCode FormExactTestFunction4(Vec input, PetscInt NumPoints)
{
    PetscErrorCode ierr;
    PetscScalar    v;


for (PetscInt j = 0; j < NumPoints; ++j)
{
double ypoint = 2*(double)j/(NumPoints - 1)-1;
    for (PetscInt i = 0; i < NumPoints; ++i)
    {
    double xpoint = 2*(double)i/(NumPoints - 1)-1;
    PetscInt row = i + j*NumPoints;
    
      
 
    v =  (32*PetscPowReal(xpoint,6)-48*PetscPowReal(xpoint,4)+18*PetscPowReal(xpoint,2)-1)*ypoint;
      

      

   
    
    ierr = VecSetValues(input, 1, &row, &v, INSERT_VALUES);CHKERRQ(ierr);    
    }

}   
 
      PetscFunctionReturn(0);
}



PetscErrorCode FormSolMatrix(Mat A, PetscInt NumPoints)
{
  PetscErrorCode ierr;
  for (PetscInt j = 0; j < NumPoints; ++j)
  {
      //generate each row
    for (PetscInt i = 0; i < NumPoints; ++i)
    {
    PetscInt row = i + j*NumPoints;
    // //generalize this to any set of points later
    // double xpoint = -PetscCosReal(PETSC_PI*(double)i/(NumPoints - 1));
    // double ypoint = -PetscCosReal(PETSC_PI*(double)j/(NumPoints - 1));

    double xpoint = 2*(double)i/(NumPoints - 1)-1;
    double ypoint = 2*(double)j/(NumPoints - 1)-1;
    double *chebtestx = Chebpoly(xpoint, NumPoints);
    double *chebtesty = Chebpoly(ypoint, NumPoints);
  
      for (PetscInt jcol = 0; jcol < NumPoints; ++jcol)
      {
              for (PetscInt icol = 0; icol < NumPoints; ++icol)
              {
                  PetscInt col = icol + jcol*NumPoints;

                
                  
                  PetscScalar v = chebtestx[icol]*chebtesty[jcol];
                  ierr = MatSetValues(A, 1, &row, 1, &col, &v, INSERT_VALUES);CHKERRQ(ierr);
                  
              
              }
        }
      }
  }
  PetscFunctionReturn(0);
}


int main (int argc,char **args)
{
	
	Mat            mat, approxmat, resmat;
	//must have at least two points
 	PetscInt       NumPoints = 3;
 	Vec            conditions, coefficients, approx, exact, res, exactres;
 	PetscErrorCode ierr;
 	KSP 		   ksp;
 	PC 			   pc;
  PetscReal    norm;

	PetscInitialize(&argc,&args,(char*)0,help);
  ierr = PetscOptionsGetInt(NULL,NULL,"-N",&NumPoints,NULL);CHKERRQ(ierr);
	/* create the matrix */
	ierr = MatCreateSeqDense(PETSC_COMM_WORLD, NumPoints*NumPoints, NumPoints*NumPoints, NULL, &mat);CHKERRQ(ierr);


	ierr = FormChebMatrix(mat, NumPoints);CHKERRQ(ierr);
    
    //Put it in formcheb matrix function
	ierr = MatAssemblyBegin(mat,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
 	ierr = MatAssemblyEnd(mat,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);


	//ierr = MatView(mat, PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr);

  ierr = MatCreateSeqDense(PETSC_COMM_WORLD, NumPoints*NumPoints, NumPoints*NumPoints, NULL, &approxmat);CHKERRQ(ierr);


  ierr = FormSolMatrix(approxmat, NumPoints);CHKERRQ(ierr);
    
    //Put it in formcheb matrix function
  ierr = MatAssemblyBegin(approxmat,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(approxmat,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

  //ierr = MatView(approxmat, PETSC_VIEWER_STDOUT_SELF);CHKERRQ(ierr);

  /* create the matrix */
  ierr = MatCreateSeqDense(PETSC_COMM_WORLD, NumPoints*NumPoints, NumPoints*NumPoints, NULL, &resmat);CHKERRQ(ierr);


  ierr = FormChebResMatrix(resmat, NumPoints);CHKERRQ(ierr);
    
    //Put it in formcheb matrix function
  ierr = MatAssemblyBegin(resmat,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(resmat,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

	ierr = VecCreate(PETSC_COMM_WORLD,&conditions);CHKERRQ(ierr);
  ierr = VecSetSizes(conditions,PETSC_DECIDE,NumPoints*NumPoints);CHKERRQ(ierr);
  ierr = VecSetFromOptions(conditions);CHKERRQ(ierr);
  ierr = FormTestFunction2(conditions, NumPoints);CHKERRQ(ierr);

  ierr = VecCreate(PETSC_COMM_WORLD,&coefficients);CHKERRQ(ierr);
  ierr = VecSetSizes(coefficients,PETSC_DECIDE,NumPoints*NumPoints);CHKERRQ(ierr);
  ierr = VecSetFromOptions(coefficients);CHKERRQ(ierr);

  ierr = VecCreate(PETSC_COMM_WORLD,&approx);CHKERRQ(ierr);
  ierr = VecSetSizes(approx,PETSC_DECIDE,NumPoints*NumPoints);CHKERRQ(ierr);
  ierr = VecSetFromOptions(approx);CHKERRQ(ierr);

  ierr = VecCreate(PETSC_COMM_WORLD,&exact);CHKERRQ(ierr);
  ierr = VecSetSizes(exact,PETSC_DECIDE,NumPoints*NumPoints);CHKERRQ(ierr);
  ierr = VecSetFromOptions(exact);CHKERRQ(ierr);
  ierr = FormExactTestFunction2(exact, NumPoints);CHKERRQ(ierr);

  ierr = VecCreate(PETSC_COMM_WORLD,&res);CHKERRQ(ierr);
  ierr = VecSetSizes(res,PETSC_DECIDE,NumPoints*NumPoints);CHKERRQ(ierr);
  ierr = VecSetFromOptions(res);CHKERRQ(ierr);

  ierr = VecCreate(PETSC_COMM_WORLD,&exactres);CHKERRQ(ierr);
  ierr = VecSetSizes(exactres,PETSC_DECIDE,NumPoints*NumPoints);CHKERRQ(ierr);
  ierr = VecSetFromOptions(exactres);CHKERRQ(ierr);
  ierr = FormResFunction2(exactres, NumPoints);CHKERRQ(ierr);




  	//ksp set from options, pc type lu
  	

  //ierr = VecView(conditions, PETSC_VIEWER_STDOUT_WORLD); CHKERRQ(ierr);

  KSPCreate(PETSC_COMM_WORLD,&ksp);


  ierr = KSPSetOperators(ksp,mat,mat); CHKERRQ(ierr);
  ierr = KSPGetPC(ksp,&pc); CHKERRQ(ierr);
  ierr = PCSetType(pc,PCLU); CHKERRQ(ierr);
  ierr = KSPSetTolerances(ksp,1.e-18,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT);CHKERRQ(ierr);

  KSPSetFromOptions(ksp);

  KSPSolve(ksp,conditions,coefficients);
  MatMult(approxmat, coefficients, approx);

  MatMult(resmat, coefficients, res);


  ierr = VecView(coefficients, PETSC_VIEWER_STDOUT_WORLD); CHKERRQ(ierr);
  //ierr = VecView(res, PETSC_VIEWER_STDOUT_WORLD); CHKERRQ(ierr);
  //ierr = VecView(exactres, PETSC_VIEWER_STDOUT_WORLD); CHKERRQ(ierr);

  ierr = VecAXPY(approx, -1.0, exact);CHKERRQ(ierr);
  ierr = VecNorm(approx,NORM_2,&norm);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Norm of error vector %g\n",(double)norm/(NumPoints));CHKERRQ(ierr);

  ierr = VecAXPY(res, -1.0, exactres);CHKERRQ(ierr);
  ierr = VecNorm(res,NORM_2,&norm);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"Norm of res vector %g\n",(double)norm/(NumPoints));CHKERRQ(ierr);

  KSPDestroy(&ksp);

  VecDestroy(&coefficients);
  VecDestroy(&conditions);  
  VecDestroy(&res);
  VecDestroy(&exactres);
  VecDestroy(&approx);
  VecDestroy(&exact);
  MatDestroy(&mat);
  MatDestroy(&approxmat);
  MatDestroy(&resmat);



PetscFinalize();
return 0;
  
}