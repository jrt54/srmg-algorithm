Below is a description of the functions used to perform SRMG


//Maps the interval [a, b] to [c, d]
PetscErrorCode intervalShift(PetscScalar a, PetscScalar b, PetscScalar c, PetscScalar d, PetscScalar *point);

//Generates an array of values P_n(point) corresponding to chebyshev polynomials
double *Chebpoly(double point, int degN);


//Generates an array of values P'_n(point) corresponding to chebyshev polynomials
double *Chebderiv(double point, int degN, double *chebfunceval);


//Generates an array of values P''_n(point) corresponding to chebyshev polynomials
double *Chebsecondderiv(double point, int degN, double *chebderiv);

//Generate the value of function satisfying boundary conditions at (x, y)
PetscScalar BoxSoln(struct patch* mypatch, PetscScalar xpoint, PetscScalar ypoint);

//Generate the value of the Laplacian of the function satisfying boundary conditions at (x, y)
PetscScalar LaplaceBoxSoln(struct patch* mypatch, PetscScalar xpoint, PetscScalar ypoint);


//Generate f-Laplace(Box)
PetscErrorCode FormConditions(Vec conditions, struct patch* mypatch);


//Form the matrix of P''(x,y) at chebyshev points
PetscErrorCode FormInteriorChebMatrix(Mat A, struct patch* mypatch);

//Get the coefficients making up the solution to the PDE and store it in the struct
PetscErrorCode SolveCoeff(struct patch* mypatch, KSP ksp);

//Get the value of the approximation to the PDE (after SolveCoeff is called)
PetscScalar PatchSolution(struct patch* mypatch, PetscScalar xpoint, PetscScalar ypoint);

//Construct, set boundaries, and solve 4 subpatches with a given buffer size
PetscErrorCode constructSubPatches(struct patch* mypatch, KSP ksp, PetscScalar buffersize);

//Plot the patch using GNUplot
PetscErrorCode PlotPatch(struct patch* mypatch, int resolution, char *FILENAME);
PetscErrorCode PlotPatchPortion(struct patch* mypatch, PetscScalar xmin, PetscScalar xmax, PetscScalar ymin, PetscScalar ymax, int resolution, char *FILENAME);
PetscErrorCode PlotError(struct patch* mypatch, int resolution, char *FILENAME,PetscScalar(*test)(PetscScalar, PetscScalar)); 
PetscErrorCode PlotErrorPortion(struct patch* mypatch, PetscScalar xmin, PetscScalar xmax, PetscScalar ymin, PetscScalar ymax, int resolution, char *FILENAME, PetscScalar(*test)(PetscScalar, PetscScalar)); 

//Get the sine coefficients out (divided by 2)
PetscErrorCode SineCoeff(double *in, double *out, int NumCoeff);

//Exhaustively search all patches for high magnitude error
PetscErrorCode TestPatches(struct patch* mypatch,  PetscScalar test(PetscScalar, PetscScalar), PetscScalar tol, PetscInt Num_Quad_Points);

//Get the fourier coefficients of each boundary and store them in the struct 
PetscErrorCode ConstructFourier(struct patch* mypatch)
