#include "SRMG.h"
#include <stdio.h>
#include <time.h>
#include <fftw3.h>
#include <petscsys.h>
#include <petscdmda.h>
#include <petscsnes.h>
#include <petscmatlab.h>
#include <petsctime.h>
#include <petscmat.h>
#include <petscdm.h>
#include <petscdmda.h>
//#include "SRMG.h"
//#include "utils.h"
#include <fftw3.h>

static char help[] = "Solving Laplace via Chebyshev";

//maps [a, b] to [c, d]
PetscErrorCode intervalShift(PetscScalar a, PetscScalar b, PetscScalar c, PetscScalar d, PetscScalar *point)
{


  *point = (*point - a)/(b-a)*(d-c) + c;
  PetscFunctionReturn(0);
}

double *Chebpoly(double point, int degN)
{       
PetscLogEvent Chebpolystuff;
PetscLogEventRegister("Chebpolystuff",0,&Chebpolystuff);
PetscLogEventBegin(Chebpolystuff,0,0,0,0);




   
  double *chebfunceval = malloc(sizeof(double) * (degN+1)); 

    for (int i=0; i < degN+1; ++i) {
            if (i == 0)
            {
            	chebfunceval[i] = 1;
            }
            else if (i == 1)
            {
            	chebfunceval[i] = point;
            }
            else
            {
            	chebfunceval[i] = 2*point*chebfunceval[i-1] - chebfunceval[i-2];

    		}
    }
PetscLogEventEnd(Chebpolystuff,0,0,0,0);

        return chebfunceval;
}

double *Chebderiv(double point, int degN, double *chebfunceval)
{       
PetscLogEvent Chebpolystuff;
PetscLogEventRegister("Chebpolystuff",0,&Chebpolystuff);
PetscLogEventBegin(Chebpolystuff,0,0,0,0);

double *chebderiv = malloc(sizeof(double) * (degN+1)); 

        for (int i=0; i < degN + 1; ++i) {
                if (i == 0)
                {
                	chebderiv[i] = 0;
                }
                else if (i == 1)
                {
                	chebderiv[i] = 1;
                }
                else if (i==2)
                {
                	chebderiv[i] = 4*point;
                }
                else
                //Tk' = (-k*x*Tk(x) + k*T_(k-1)(x))/(1-x^2)
                {
                	chebderiv[i] = i*(2*chebfunceval[i-1] + chebderiv[i-2]/(i-2));

        		}
        }
PetscLogEventEnd(Chebpolystuff,0,0,0,0);


        return chebderiv;
}


double *Chebsecondderiv(double point, int degN, double *chebderiv)
{       
PetscLogEvent Chebpolystuff;
PetscLogEventRegister("Chebpolystuff",0,&Chebpolystuff);
PetscLogEventBegin(Chebpolystuff,0,0,0,0);


       
		double *chebsecondderiv = malloc(sizeof(double) * (degN+1)); 

        for (int i=0; i < degN + 1; ++i) {
                if (i == 0)
                {
                	chebsecondderiv[i] = 0;
                }
                else if (i == 1)
                {
                	chebsecondderiv[i] = 0;
                }
                else if (i==2)
                {
                	chebsecondderiv[i] = 4;
                }
                else
                //Tk'; = (-k*x*Tk;(x) + k*T_(k-1)''(x))/(1-x^2)
                {
                	chebsecondderiv[i] = i*(2*chebderiv[i-1] + chebsecondderiv[i-2]/(i-2));

        		}
        }

PetscLogEventEnd(Chebpolystuff,0,0,0,0);
 
        return chebsecondderiv;
}




PetscScalar PatchSolution(struct patch* mypatch, PetscScalar xpoint, PetscScalar ypoint)
{
	PetscScalar *coeff;
	VecGetArray(mypatch->coefficients,&coeff);
	PetscInt NumPoints = mypatch->NumPoints;
	PetscScalar relativexpoint = xpoint;
	PetscScalar relativeypoint = ypoint;
	intervalShift(mypatch->LLxcoord, mypatch->URxcoord, -1, 1,  &relativexpoint);
	intervalShift(mypatch->LLycoord, mypatch->URycoord, -1, 1,  &relativeypoint);
	double *chebtestx = Chebpoly(relativexpoint, NumPoints);
	double *chebtesty = Chebpoly(relativeypoint, NumPoints);
    PetscScalar v = 0;




//sum up the recombined cheby bases times their corresponding coefficients. 
//probably need to $shift here from true x, y to chebtest(shiftinto [-1, 1] of x, y)
    for (PetscInt jcol = 0; jcol < NumPoints-2; ++jcol)
    {
        for (PetscInt icol = 0; icol < NumPoints-2; ++icol)
        {

	            PetscInt col = icol + jcol*(NumPoints-2);
				
	            if ((icol+2)%2 == 0 && (jcol+2)%2 == 0)
	            {
	            //recombination of bases that has homogenous boundaries
	            v = v + (chebtestx[icol+2]-1)*(chebtesty[jcol+2]-1)*coeff[col];                      
	            }
				if ((icol+2)%2 == 0 && (jcol+2)%2 == 1)
				{
				//recombination of bases that has homogenous boundaries
				v = v + (chebtestx[icol+2]-1)*(chebtesty[jcol+2]-relativeypoint)*coeff[col];
				}
				if ((icol+2)%2 == 1 && (jcol+2)%2 == 0)
				{
				v = v + (chebtestx[icol+2]-relativexpoint)*(chebtesty[jcol+2]-1)*coeff[col];
	            }
	            if ((icol+2)%2 == 1 && (jcol+2)%2 == 1)
	            {
	            //recombination of bases that has homogenous boundaries
	            v = v + (chebtestx[icol+2]-relativexpoint)*(chebtesty[jcol+2]-relativeypoint)*coeff[col];
	            } 
	            

        }

    }
    VecRestoreArray(mypatch->coefficients, &coeff);

return v + BoxSoln(mypatch, xpoint, ypoint);

}

PetscErrorCode SolveCoeff(struct patch* mypatch, KSP ksp)
{
    PetscInt NumPoints = mypatch->NumPoints;
    PC         pc;
    PetscErrorCode ierr; 
    Vec coefficients, conditions;
    Mat derivmat;

    ierr = VecCreate(PETSC_COMM_WORLD,&coefficients);
    ierr = VecSetSizes(coefficients,PETSC_DECIDE,(NumPoints-2)*(NumPoints-2));
    ierr = VecSetFromOptions(coefficients);

    ierr = VecCreate(PETSC_COMM_WORLD,&conditions);CHKERRQ(ierr);
    ierr = VecSetSizes(conditions,PETSC_DECIDE,(NumPoints-2)*(NumPoints-2));CHKERRQ(ierr);
    ierr = VecSetFromOptions(conditions);CHKERRQ(ierr);
    ierr = FormConditions(conditions, mypatch); CHKERRQ(ierr);


    ierr = MatCreateSeqDense(PETSC_COMM_WORLD, (NumPoints-2)*(NumPoints-2), (NumPoints-2)*(NumPoints-2), NULL, &derivmat);CHKERRQ(ierr);
    ierr = FormInteriorChebMatrix(derivmat, mypatch);CHKERRQ(ierr);
    ierr = MatAssemblyBegin(derivmat,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
    ierr = MatAssemblyEnd(derivmat,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

    ierr = KSPReset(ksp);CHKERRQ(ierr);
    ierr = KSPSetOperators(ksp,derivmat,derivmat); CHKERRQ(ierr);
    ierr = KSPGetPC(ksp,&pc); CHKERRQ(ierr);
    ierr = PCSetType(pc,PCLU); CHKERRQ(ierr);
    ierr = KSPSetTolerances(ksp,1.e-18,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT);CHKERRQ(ierr);
    ierr = KSPSetFromOptions(ksp);CHKERRQ(ierr);
    ierr = KSPSolve(ksp,conditions,coefficients); CHKERRQ(ierr);
 

    VecDuplicate(coefficients,&mypatch -> coefficients);
    VecCopy(coefficients, mypatch -> coefficients);
    VecDestroy(&coefficients);
    VecDestroy(&conditions);
    MatDestroy(&derivmat);

    PetscFunctionReturn(0);
}
PetscScalar PatchLaplace(struct patch *mypatch, PetscScalar xpoint, PetscScalar ypoint)
{
  PetscScalar *coeff;
  PetscErrorCode ierr;
  VecGetArray(mypatch->coefficients,&coeff);
  PetscInt NumPoints = mypatch->NumPoints;
  PetscScalar v = 0;

  PetscScalar relativexpoint = xpoint;
  PetscScalar relativeypoint = ypoint;
  intervalShift(mypatch->LLxcoord, mypatch->URxcoord, -1, 1,  &relativexpoint);
  intervalShift(mypatch->LLycoord, mypatch->URycoord, -1, 1,  &relativeypoint);


  PetscScalar yshiftder =  4/((mypatch->URycoord-mypatch->LLycoord)*(mypatch->URycoord-mypatch->LLycoord));
  PetscScalar xshiftder =  4/((mypatch->URxcoord-mypatch->LLxcoord)*(mypatch->URxcoord-mypatch->LLxcoord));
  //PetscPrintf(PETSC_COMM_SELF, "scaling (x, y): %f, %f", xshiftder, yshiftder);

  double *chebtestx = Chebpoly(relativexpoint, NumPoints);
  double *chebtesty = Chebpoly(relativeypoint, NumPoints);
  double *chebderivx = Chebderiv(relativexpoint, NumPoints, chebtestx);
  double *chebderivy = Chebderiv(relativeypoint, NumPoints, chebtesty);
  double *chebsecondderivx = Chebsecondderiv(relativexpoint, NumPoints, chebderivx);
  double *chebsecondderivy = Chebsecondderiv(relativeypoint, NumPoints, chebderivy);


    for (PetscInt jcol = 0; jcol < NumPoints-2; ++jcol)
    {
            for (PetscInt icol = 0; icol < NumPoints-2; ++icol)
            {
                PetscInt col = icol + jcol*(NumPoints-2);
                if ((icol+2)%2 == 0 && (jcol+2)%2 == 0)
                {
                //recombination of bases that has homogenous boundaries
                v = v + coeff[col]* (yshiftder*chebsecondderivy[jcol+2]*(chebtestx[icol+2]-1) + xshiftder*chebsecondderivx[icol+2]*(chebtesty[jcol+2]-1));
                //ierr = MatSetValues(A, 1, &row, 1, &col, &v, INSERT_VALUES);CHKERRQ(ierr);
                }
                if ((icol+2)%2 == 0 && (jcol+2)%2 == 1)
                {
                //recombination of bases that has homogenous boundaries
                v = v + coeff[col]*(yshiftder*chebsecondderivy[jcol+2]*(chebtestx[icol+2]-1) + xshiftder*chebsecondderivx[icol+2]*(chebtesty[jcol+2]-relativeypoint));
                }
                if ((icol+2)%2 == 1 && (jcol+2)%2 == 0)
                {
                //recombination of bases that has homogenous boundaries
                v = v + coeff[col]*(yshiftder*chebsecondderivy[jcol+2]*(chebtestx[icol+2]-relativexpoint) + xshiftder*chebsecondderivx[icol+2]*(chebtesty[jcol+2]-1));
                }
                if ((icol+2)%2 == 1 && (jcol+2)%2 == 1)
                {
                //recombination of bases that has homogenous boundaries
                v = v + coeff[col]*(yshiftder*chebsecondderivy[jcol+2]*(chebtestx[icol+2]-relativexpoint) + xshiftder*chebsecondderivx[icol+2]*(chebtesty[jcol+2]-relativeypoint));
                }
       		}
    }
   
  VecRestoreArray(mypatch->coefficients, &coeff);
return LaplaceBoxSoln(mypatch, xpoint, ypoint)  + v;

}

PetscErrorCode FormConditions(Vec conditions, struct patch* mypatch)
{
    PetscScalar LowerLeftxcord = mypatch->LLxcoord;
    PetscScalar LowerLeftycord = mypatch->LLycoord;
    PetscScalar UpperRightycord = mypatch->URycoord;
    PetscScalar UpperRightxcord = mypatch->URxcoord;
    
    PetscInt NumPoints = mypatch->NumPoints;
    PetscErrorCode ierr;
    PetscScalar    v;
    

     
    for (PetscInt j = 0; j < NumPoints-2; ++j){
    double ypoint = -PetscCosReal(PETSC_PI*(double)(2*j+1)/(2*(NumPoints - 2)));
    ierr = intervalShift(-1,  1, LowerLeftycord, UpperRightycord,  &ypoint);CHKERRQ(ierr);

        for (PetscInt i = 0; i < NumPoints-2; ++i){
        PetscInt row = i + j*(NumPoints-2);
        
        double xpoint = -PetscCosReal(PETSC_PI*(double)(2*i+1)/(2*(NumPoints - 2)));
        ierr = intervalShift(-1,  1, LowerLeftxcord, UpperRightxcord,  &xpoint);CHKERRQ(ierr);

        v = mypatch->forcing(xpoint, ypoint) - LaplaceBoxSoln(mypatch, xpoint, ypoint);

        ierr = VecSetValues(conditions, 1, &row, &v, INSERT_VALUES);CHKERRQ(ierr);    
        }

    }  
PetscFunctionReturn(0);
}

PetscErrorCode PatchErrorinRegion(struct patch *mypatch, PetscScalar test(PetscScalar, PetscScalar), PetscInt Num_Quad_Points, PetscScalar* integral, PetscScalar xmin,  PetscScalar xmax, PetscScalar ymin, PetscScalar ymax)
{
  PetscReal x[Num_Quad_Points];
  PetscReal wx[Num_Quad_Points];
  PetscReal y[Num_Quad_Points];
  PetscReal wy[Num_Quad_Points];

  PetscDTGaussQuadrature(Num_Quad_Points, xmin, xmax, x, wx);
  PetscDTGaussQuadrature(Num_Quad_Points, ymin, ymax, y, wy);

  (*integral) = 0;
   

  for (PetscInt i = 0; i < Num_Quad_Points; i++)
  {

    for (PetscInt j = 0; j < Num_Quad_Points; j++)
    {


    *integral = *integral + PetscAbsReal(PatchSolution(mypatch, x[i], y[j])-test(x[i], y[j]))*wx[i]*wy[j];


    }
  }


PetscFunctionReturn(0);

}

PetscErrorCode PatchErrorInterior(struct patch *mypatch, PetscScalar test(PetscScalar, PetscScalar), PetscInt Num_Quad_Points, PetscScalar* integral)
{

PetscScalar xmin = mypatch->interiorLLxcoord;
PetscScalar ymin = mypatch->interiorLLycoord;
PetscScalar xmax = mypatch->interiorURxcoord; 
PetscScalar ymax = mypatch->interiorURycoord;

PatchErrorinRegion(mypatch, test, Num_Quad_Points, integral, xmin, xmax, ymin, ymax);

PetscFunctionReturn(0);
}


PetscErrorCode PatchError(struct patch *mypatch, PetscScalar test(PetscScalar, PetscScalar), PetscInt Num_Quad_Points, PetscScalar* integral)
{
  PetscReal x[Num_Quad_Points];
  PetscReal wx[Num_Quad_Points];
  PetscReal y[Num_Quad_Points];
  PetscReal wy[Num_Quad_Points];

  PetscDTGaussQuadrature(Num_Quad_Points, mypatch->LLxcoord, mypatch->URxcoord, x, wx);
  PetscDTGaussQuadrature(Num_Quad_Points, mypatch->LLycoord, mypatch->URycoord, y, wy);

  (*integral) = 0;
   

  for (PetscInt i = 0; i < Num_Quad_Points; i++)
  {

    for (PetscInt j = 0; j < Num_Quad_Points; j++)
    {

    *integral = *integral + PetscAbsReal(PatchSolution(mypatch, x[i], y[j])-test(x[i], y[j]))*wx[i]*wy[j];


    }
  }

PetscFunctionReturn(0);

}

PetscErrorCode PatchResinRegion(struct patch *mypatch, PetscInt Num_Quad_Points, PetscScalar* integral, PetscScalar xmin,  PetscScalar xmax, PetscScalar ymin, PetscScalar ymax)
{
  PetscReal x[Num_Quad_Points];
  PetscReal wx[Num_Quad_Points];
  PetscReal y[Num_Quad_Points];
  PetscReal wy[Num_Quad_Points];

  PetscDTGaussQuadrature(Num_Quad_Points, xmin, xmax, x, wx);
  PetscDTGaussQuadrature(Num_Quad_Points, ymin, ymax, y, wy);

  (*integral) = 0;
   

  for (PetscInt i = 0; i < Num_Quad_Points; i++)
  {

    for (PetscInt j = 0; j < Num_Quad_Points; j++)
    {


    *integral = *integral + PetscAbsReal(PatchLaplace(mypatch, x[i], y[j])-mypatch->forcing(x[i], y[j]))*wx[i]*wy[j];


    }
  }


PetscFunctionReturn(0);

}

PetscErrorCode PatchResInterior(struct patch *mypatch, PetscInt Num_Quad_Points, PetscScalar* integral)
{

PetscScalar xmin = mypatch->interiorLLxcoord;
PetscScalar ymin = mypatch->interiorLLycoord;
PetscScalar xmax = mypatch->interiorURxcoord; 
PetscScalar ymax = mypatch->interiorURycoord;

PatchResinRegion(mypatch, Num_Quad_Points, integral, xmin, xmax, ymin, ymax);

PetscFunctionReturn(0);
}

PetscErrorCode PatchRes(struct patch *mypatch, PetscScalar test(PetscScalar, PetscScalar), PetscInt Num_Quad_Points, PetscScalar* integral)
{
  PetscReal x[Num_Quad_Points];
  PetscReal wx[Num_Quad_Points];
  PetscReal y[Num_Quad_Points];
  PetscReal wy[Num_Quad_Points];

  PetscDTGaussQuadrature(Num_Quad_Points, mypatch->LLxcoord, mypatch->URxcoord, x, wx);
  PetscDTGaussQuadrature(Num_Quad_Points, mypatch->LLycoord, mypatch->URycoord, y, wy);

  (*integral) = 0;
   

  for (PetscInt i = 0; i < Num_Quad_Points; i++)
  {

    for (PetscInt j = 0; j < Num_Quad_Points; j++)
    {
//fprintf(temp, "%lf %lf %lf \n", xvals[i], yvals[j], PetscAbsReal(PatchLaplace(mypatch, xvals[i], yvals[j])-mypatch->forcing(xvals[i], yvals[j])) ); //Write the data to a temporary file

    *integral = *integral + PetscAbsReal(PatchLaplace(mypatch, x[i], y[j])-mypatch->forcing(x[i], y[j]))*wx[i]*wy[j];


    }
  }

PetscFunctionReturn(0);

}


PetscErrorCode PatchErrorinBuffer(struct patch *mypatch, PetscScalar test(PetscScalar, PetscScalar), PetscInt N, PetscScalar buffer, PetscScalar* integral)
{
  PetscReal x[N];
  PetscReal wx[N];
  PetscReal y[N];
  PetscReal wy[N];

  PetscDTGaussQuadrature(N, mypatch->LLxcoord, mypatch->URxcoord, x, wx);
  PetscDTGaussQuadrature(N, mypatch->LLycoord, mypatch->URycoord, y, wy);

  (*integral) = 0;
   

  for (PetscInt i = 0; i < N; i++)
  {

    for (PetscInt j = 0; j < N; j++)
    {
    //only evaluates function if it is within some distance of one of the  sides of the region
    if(PetscMin(PetscAbsReal(x[i] - mypatch->LLxcoord), PetscAbsReal(x[i] - mypatch->URxcoord))<= buffer ||  
      PetscMin(PetscAbsReal(y[i] - mypatch->LLycoord), PetscAbsReal(y[i] - mypatch->URycoord)) <= buffer)
      {
      *integral = *integral + PetscAbsReal(PatchSolution(mypatch, x[i], y[j])-test(x[i], y[j]))*wx[i]*wy[j];
      }
    }
  }

PetscFunctionReturn(0);

}


PetscErrorCode FormInteriorChebMatrix(Mat A, struct patch* mypatch)
{
PetscErrorCode ierr;
PetscScalar LowerLeftxcord = mypatch->LLxcoord;
PetscScalar LowerLeftycord = mypatch->LLycoord;
PetscScalar UpperRightycord = mypatch->URycoord;
PetscScalar UpperRightxcord = mypatch->URxcoord;
PetscInt NumPoints = mypatch->NumPoints;


for (PetscInt j = 0; j < NumPoints-2; ++j)
{
        double ypoint = -PetscCosReal(PETSC_PI*(double)(2*j+1)/(2*(NumPoints - 2)));
	for (PetscInt i = 0; i < NumPoints-2; ++i)
        {
         PetscInt row = i + j*(NumPoints-2);

	//only taking interior points

        double xpoint = -PetscCosReal(PETSC_PI*(double)(2*i+1)/(2*(NumPoints - 2)));

  //We want the laplacian of bases phi(M^-1(x)) where M maps from square to patch
  //Invert this matrix to see what would yield equality at laplace(u) = f(x)
  //So each basis must be e.g. phi_xx(M^-1(x)) * [M^-1]'^2 = 4/(Rightx-Leftx)
  PetscScalar yshiftder =  4/((UpperRightycord-LowerLeftycord)*(UpperRightycord-LowerLeftycord));
  PetscScalar xshiftder =  4/((UpperRightxcord-LowerLeftxcord)*(UpperRightxcord-LowerLeftxcord));

  double *chebtestx = Chebpoly(xpoint, NumPoints);
  double *chebtesty = Chebpoly(ypoint, NumPoints);
  double *chebderivx = Chebderiv(xpoint, NumPoints, chebtestx);
  double *chebderivy = Chebderiv(ypoint, NumPoints, chebtesty);
  double *chebsecondderivx = Chebsecondderiv(xpoint, NumPoints, chebderivx);
  double *chebsecondderivy = Chebsecondderiv(ypoint, NumPoints, chebderivy);
    for (PetscInt jcol = 0; jcol < NumPoints-2; ++jcol)
    {
            for (PetscInt icol = 0; icol < NumPoints-2; ++icol)
            {
                PetscInt col = icol + jcol*(NumPoints-2);
                if ((icol+2)%2 == 0 && (jcol+2)%2 == 0)
          			{
          			//recombination of bases that has homogenous boundaries
          			PetscScalar v = yshiftder*chebsecondderivy[jcol+2]*(chebtestx[icol+2]-1) + xshiftder*chebsecondderivx[icol+2]*(chebtesty[jcol+2]-1);
          			ierr = MatSetValues(A, 1, &row, 1, &col, &v, INSERT_VALUES);CHKERRQ(ierr);
          			}
                if ((icol+2)%2 == 0 && (jcol+2)%2 == 1)
                {
                //recombination of bases that has homogenous boundaries
                PetscScalar v = yshiftder*chebsecondderivy[jcol+2]*(chebtestx[icol+2]-1) + xshiftder*chebsecondderivx[icol+2]*(chebtesty[jcol+2]-ypoint);
                ierr = MatSetValues(A, 1, &row, 1, &col, &v, INSERT_VALUES);CHKERRQ(ierr);
                }
                if ((icol+2)%2 == 1 && (jcol+2)%2 == 0)
                {
                //recombination of bases that has homogenous boundaries
                PetscScalar v = yshiftder*chebsecondderivy[jcol+2]*(chebtestx[icol+2]-xpoint) + xshiftder*chebsecondderivx[icol+2]*(chebtesty[jcol+2]-1);
                ierr = MatSetValues(A, 1, &row, 1, &col, &v, INSERT_VALUES);CHKERRQ(ierr);
                }
                if ((icol+2)%2 == 1 && (jcol+2)%2 == 1)
                {
                //recombination of bases that has homogenous boundaries
                PetscScalar v = yshiftder*chebsecondderivy[jcol+2]*(chebtestx[icol+2]-xpoint) + xshiftder*chebsecondderivx[icol+2]*(chebtesty[jcol+2]-ypoint);
                ierr = MatSetValues(A, 1, &row, 1, &col, &v, INSERT_VALUES);CHKERRQ(ierr);
                }
            }
    }

  free(chebtestx);
  free(chebtesty);
  free(chebderivx);
  free(chebderivy);
  free(chebsecondderivx);
  free(chebsecondderivy);

        }
}
PetscFunctionReturn(0);
}


PetscErrorCode PlotPatchPortion(struct patch* mypatch, PetscScalar xmin, PetscScalar xmax, PetscScalar ymin, PetscScalar ymax, int resolution, char *FILENAME)
{
    int NUM_POINTS=resolution;
    PetscScalar LLxcoord = xmin;    
    PetscScalar LLycoord = ymin;    
    PetscScalar URxcoord = xmax;    
    PetscScalar URycoord = ymax;     
    
    int NUM_COMMANDS=8;
    //char * commandsForGnuplot[] = {"set title \"Errorplot\"", "plot 'data.temp'"};
    char * commandsForGnuplot[] = {
					"set terminal png",
					"set xrange [-1:1]",
					"set yrange [-1:1]",
					"set pm3d interpolate 30,30",
					"set palette defined (0 'blue', 1 'red')", 
					"set view map",
//					"set title 'Patchplot'", 
					"set key off", 
    					"splot 'data.temp' using 1:2:3 with image \n"
				};
    double xvals[NUM_POINTS];
    for(int i=0; i< NUM_POINTS; ++i)
    {
    xvals[i] = xmin + i*(xmax-xmin)/(NUM_POINTS-1);    
    }	
    
    double yvals[NUM_POINTS];
    for(int i=0; i< NUM_POINTS; ++i)
    {
    yvals[i] = ymin + i*(ymax-ymin)/(NUM_POINTS-1);    
    }	

    //double xvals[NUM_POINTS] = {5.0 ,3.0, 1.0, 3.0, 5.0};
    //double yvals[NUM_POINTS] = {5.0 ,3.0, 1.0, 3.0, 5.0};
    //PetscScalar approx = PatchSolution(*currentpatch, x, y);
    //char *datafile = malloc(sizeof(char)*(strlen(FILENAME)+strlen(".temp")));
    //sprintf(datafile, "%s.tmp", FILENAME);
    //printf("filename %s \n", datafile);
    //FILE * temp = fopen(datafile, "w");
    FILE * temp = fopen("data.temp", "w");
    //Opens an interface that one can use to send commands as if they were typing into the
    //     gnuplot command line.  "The -persistent" keeps the plot open even after your
    //     C program terminates.
    //
    FILE * gnuplotPipe = popen ("gnuplot -persistent", "w");
    //FILE * gnuplotPipe = fopen ("gnuplot -persistent", "w");
    fprintf(gnuplotPipe, "set output \"%s\"\n", FILENAME);  
    for (int i=0; i < NUM_POINTS; ++i)
    for (int j=0; j < NUM_POINTS; ++j)
    //PetscScalar LLxcoord = mypatch->LLxcoord;    
    //PetscScalar LLycoord = mypatch->LLycoord;    
    //PetscScalar URxcoord = mypatch->URxcoord;    
    //PetscScalar URycoord = mypatch->URycoord;     V
    {
    fprintf(temp, "%lf %lf %lf \n", xvals[i], yvals[j], PatchSolution(mypatch, xvals[i], yvals[j])); //Write the data to a temporary file
    }
    
    
    for (int i=0; i < NUM_COMMANDS; i++)
    {
    fprintf(gnuplotPipe, "%s \n", commandsForGnuplot[i]); //Send commands to gnuplot one by one.
    }
    fclose(temp);
    pclose(gnuplotPipe);
    //fclose(gnuplotPipe);
    PetscFunctionReturn(0);

}

PetscErrorCode PlotPatch(struct patch* mypatch, int resolution, char *FILENAME)
{
PlotPatchPortion(mypatch, mypatch->LLxcoord, mypatch->URxcoord, mypatch->LLycoord, mypatch->URycoord, resolution, FILENAME);
PetscFunctionReturn(0);

}

PetscErrorCode PlotErrorPortion(struct patch* mypatch, PetscScalar xmin, PetscScalar xmax, PetscScalar ymin, PetscScalar ymax, int resolution, char *FILENAME, PetscScalar(*test)(PetscScalar, PetscScalar)) 
{
    int NUM_POINTS=resolution;
    PetscScalar LLxcoord = xmin;    
    PetscScalar LLycoord = ymin;    
    PetscScalar URxcoord = xmax;    
    PetscScalar URycoord = ymax;     
    
    int NUM_COMMANDS=9;
    //char * commandsForGnuplot[] = {"set title \"Errorplot\"", "plot 'data.temp'"};
    char * commandsForGnuplot[] = {
					"set terminal png",
					"set xrange [-1:1]",
					"set yrange [-1:1]",
					"set pm3d interpolate 30,30",
					"set view map",
					"set cbrange [0: 2]", 
					"set palette defined (0 'blue', 2 'red')", 
		//			"set title 'Patchplot'", 
					"set key off", 
    					"splot 'error.temp' using 1:2:3 with image"

				};
    double xvals[NUM_POINTS];
    for(int i=0; i< NUM_POINTS; ++i)
    {
    xvals[i] = LLxcoord + i*(URxcoord-LLxcoord)/(NUM_POINTS-1);    
    }	
    
    double yvals[NUM_POINTS];
    for(int i=0; i< NUM_POINTS; ++i)
    {
    yvals[i] = LLycoord + i*(URycoord-LLycoord)/(NUM_POINTS-1);    
    }	

    //double xvals[NUM_POINTS] = {5.0 ,3.0, 1.0, 3.0, 5.0};
    //double yvals[NUM_POINTS] = {5.0 ,3.0, 1.0, 3.0, 5.0};
    //PetscScalar approx = PatchSolution(*currentpatch, x, y);
    //char *datafile = malloc(sizeof(char)*(strlen(FILENAME)+strlen(".temp")));
    //sprintf(datafile, "%s.tmp", FILENAME);
    //printf("filename %s \n", datafile);
    //FILE * temp = fopen(datafile, "w");
    FILE * temp = fopen("error.temp", "w");
    //Opens an interface that one can use to send commands as if they were typing into the
    //     gnuplot command line.  "The -persistent" keeps the plot open even after your
    //     C program terminates.
    //
    FILE * gnuplotPipe = popen ("gnuplot -persistent", "w");
    //FILE * gnuplotPipe = fopen ("gnuplot -persistent", "w");
    fprintf(gnuplotPipe, "set output \"%s\"\n", FILENAME);  
    for (int i=0; i < NUM_POINTS; ++i)
    for (int j=0; j < NUM_POINTS; ++j)
    {
    fprintf(temp, "%lf %lf %lf \n", xvals[i], yvals[j], PetscAbsReal(PatchSolution(mypatch, xvals[i], yvals[j])-test(xvals[i], yvals[j])) ); //Write the data to a temporary file
    }
    
    
    for (int i=0; i < NUM_COMMANDS; i++)
    {
    fprintf(gnuplotPipe, "%s \n", commandsForGnuplot[i]); //Send commands to gnuplot one by one.
    }

    fclose(temp);
    //fclose(gnuplotPipe);
    pclose(gnuplotPipe);
    PetscFunctionReturn(0);

}

PetscErrorCode PlotError(struct patch* mypatch, int resolution, char *FILENAME,PetscScalar(*test)(PetscScalar, PetscScalar)) 
{
PlotErrorPortion(mypatch, mypatch->LLxcoord, mypatch->URxcoord, mypatch->LLycoord, mypatch->URycoord, resolution, FILENAME, test);
PetscFunctionReturn(0);
}

PetscErrorCode PlotResPortion(struct patch* mypatch, PetscScalar xmin, PetscScalar xmax, PetscScalar ymin, PetscScalar ymax, int resolution, char *FILENAME, PetscScalar(*test)(PetscScalar, PetscScalar)) 
{
    int NUM_POINTS=resolution;
    PetscScalar LLxcoord = xmin;    
    PetscScalar LLycoord = ymin;    
    PetscScalar URxcoord = xmax;    
    PetscScalar URycoord = ymax;     
    
    int NUM_COMMANDS=8;
    //char * commandsForGnuplot[] = {"set title \"Errorplot\"", "plot 'data.temp'"};
    char * commandsForGnuplot[] = {
					"set terminal png",
					"set xrange [-1:1]",
					"set yrange [-1:1]",
					"set pm3d interpolate 30,30",
					"set view map",
					"set palette defined (0 'blue', 1 'red')", 
					"set key off", 
					//"set title 'Patchplot'", 
    					"splot 'error.temp' using 1:2:3 with image \n"
				};
    double xvals[NUM_POINTS];
    for(int i=0; i< NUM_POINTS; ++i)
    {
    xvals[i] = LLxcoord + i*(URxcoord-LLxcoord)/(NUM_POINTS-1);    
    }	
    
    double yvals[NUM_POINTS];
    for(int i=0; i< NUM_POINTS; ++i)
    {
    yvals[i] = LLycoord + i*(URycoord-LLycoord)/(NUM_POINTS-1);    
    }	

    //double xvals[NUM_POINTS] = {5.0 ,3.0, 1.0, 3.0, 5.0};
    //double yvals[NUM_POINTS] = {5.0 ,3.0, 1.0, 3.0, 5.0};
    //PetscScalar approx = PatchSolution(*currentpatch, x, y);
    //char *datafile = malloc(sizeof(char)*(strlen(FILENAME)+strlen(".temp")));
    //sprintf(datafile, "%s.tmp", FILENAME);
    //printf("filename %s \n", datafile);
    //FILE * temp = fopen(datafile, "w");
    FILE * temp = fopen("error.temp", "w");
    //Opens an interface that one can use to send commands as if they were typing into the
    //     gnuplot command line.  "The -persistent" keeps the plot open even after your
    //     C program terminates.
    //
    FILE * gnuplotPipe = popen ("gnuplot -persistent", "w");
    //FILE * gnuplotPipe = fopen ("gnuplot -persistent", "w");
    fprintf(gnuplotPipe, "set output \"%s\"\n", FILENAME);  
    for (int i=0; i < NUM_POINTS; ++i)
    for (int j=0; j < NUM_POINTS; ++j)
    {
    fprintf(temp, "%lf %lf %lf \n", xvals[i], yvals[j], PetscAbsReal(PatchLaplace(mypatch, xvals[i], yvals[j])-mypatch->forcing(xvals[i], yvals[j])) ); //Write the data to a temporary file
    }
    
    
    for (int i=0; i < NUM_COMMANDS; i++)
    {
    fprintf(gnuplotPipe, "%s \n", commandsForGnuplot[i]); //Send commands to gnuplot one by one.
    }
    fclose(temp);
    //fclose(gnuplotPipe);
    pclose(gnuplotPipe);
    PetscFunctionReturn(0);

}

PetscErrorCode PlotRes(struct patch* mypatch, int resolution, char *FILENAME,PetscScalar(*test)(PetscScalar, PetscScalar)) 
{
PlotResPortion(mypatch, mypatch->LLxcoord, mypatch->URxcoord, mypatch->LLycoord, mypatch->URycoord, resolution, FILENAME, test);
PetscFunctionReturn(0);
}


PetscScalar BufferNeeded(struct patch *mypatch, PetscScalar test(PetscScalar, PetscScalar), PetscScalar machineerror, PetscInt N)
{
  //return a buffer size such that 90% of the error is inside of this buffer region
  PetscScalar totalerror, buffererror;
  PatchError(mypatch, test, N, &totalerror);
  PetscScalar bufferregion = 0;
  PatchErrorinBuffer(mypatch, test, N, bufferregion, &buffererror);
  while(buffererror < .9*totalerror - machineerror && bufferregion < PetscMax(mypatch->URxcoord - mypatch-> LLxcoord, mypatch->URycoord - mypatch-> LLycoord))
  {
    //add a little more to the buffer relative to the size of patch
    bufferregion = bufferregion + .05*PetscMin(mypatch->URxcoord - mypatch-> LLxcoord, mypatch->URycoord - mypatch-> LLycoord);
    PatchErrorinBuffer(mypatch, test, N, bufferregion, &buffererror);
  }

  printf("Error in buffer region %f: %f whereas whole patch has error: %f \n" , bufferregion, buffererror, totalerror);
  return bufferregion;
}

//Tests all patches for relatively hgih error (set by tolerance per unit area)
PetscErrorCode TestPatches(struct patch* mypatch,  PetscScalar test(PetscScalar, PetscScalar), PetscScalar tol, PetscInt Num_Quad_Points)
{
if(mypatch != NULL)
{
  PetscScalar integral;
  PatchError(mypatch, test, Num_Quad_Points, &integral);
  if(integral > tol*(mypatch->URxcoord - mypatch->LLxcoord)*(mypatch->URycoord -mypatch->LLycoord))
  {
  printf("There is large error for patch [%f, %f] x [%f, %f] with error: %f\n", mypatch->LLxcoord, mypatch->URxcoord, mypatch->LLycoord, mypatch->URycoord, integral);
  }

  
  struct patch* lowerleft = mypatch->lowerleft; 
  struct patch* lowerright = mypatch->lowerright; 
  struct patch* upperright = mypatch->upperright; 
  struct patch* upperleft = mypatch -> upperleft;

  
  TestPatches(lowerleft, test, tol, Num_Quad_Points);
  TestPatches(lowerright, test, tol, Num_Quad_Points);
  TestPatches(upperleft, test, tol, Num_Quad_Points);
  TestPatches(upperright, test, tol, Num_Quad_Points);
}



PetscFunctionReturn(0);
}






PetscErrorCode SegmentalRefinement(struct patch* mypatch, PetscInt NumLevels, KSP ksp, PetscScalar buffersize)//struct patch* lowerleft, struct patch* lowerright, struct patch* upperleft, struct patch* upperright)
{
  PetscErrorCode ierr; 
//mypatch->parent->LLycoord

  PetscInt L = NumLevels;


  PetscInt NumPoints = mypatch->NumPoints;



 
  if(L >= 1)
  {
  //get the remaining levels needed

  constructSubPatches(mypatch, ksp, buffersize);
  SegmentalRefinement(mypatch->lowerleft, L-1, ksp, buffersize);
  SegmentalRefinement(mypatch->lowerright, L-1, ksp,buffersize);
  SegmentalRefinement(mypatch->upperleft, L-1, ksp, buffersize);
  SegmentalRefinement(mypatch->upperright, L-1, ksp,buffersize);

  }

  PetscFunctionReturn(0);


}

//integral must come in set to 0
PetscErrorCode TotalError(struct patch *mypatch, PetscScalar test(PetscScalar, PetscScalar), PetscInt Num_Quad_Points, PetscScalar* integral, PetscInt curr_level, PetscInt total_level)
{

if(curr_level < total_level)
{
TotalError(mypatch->lowerleft, test, Num_Quad_Points, integral, curr_level+1, total_level);
TotalError(mypatch->lowerright, test, Num_Quad_Points, integral, curr_level+1, total_level);
TotalError(mypatch->upperleft, test, Num_Quad_Points, integral, curr_level+1, total_level);
TotalError(mypatch->upperright, test, Num_Quad_Points, integral, curr_level+1, total_level);
}

if(curr_level == total_level)
{
PetscScalar patchintegral;

PatchErrorInterior(mypatch, test, Num_Quad_Points, &patchintegral);

//printf("Adding to full integral from [%f %f] x [%f %f]: %f\n ", mypatch->LLxcoord, mypatch->URxcoord, mypatch->LLycoord, mypatch->URycoord, patchintegral );
//printf("Box Soln from [%f %f] x [%f %f]: %f\n ", mypatch->interiorLLxcoord, mypatch->interiorURxcoord, mypatch->interiorLLycoord, mypatch->interiorURycoord, BoxSoln(mypatch, .5*mypatch->interiorLLxcoord+.5*mypatch->interiorURxcoord, .5*mypatch->interiorLLycoord+.5*mypatch->interiorURycoord));

*integral += patchintegral;

}


PetscFunctionReturn(0);
}

PetscErrorCode TotalRes(struct patch *mypatch, PetscInt Num_Quad_Points, PetscScalar* integral, PetscInt curr_level, PetscInt total_level)
{

if(curr_level < total_level)
{
TotalRes(mypatch->lowerleft, Num_Quad_Points, integral, curr_level+1, total_level);
TotalRes(mypatch->lowerright, Num_Quad_Points, integral, curr_level+1, total_level);
TotalRes(mypatch->upperleft, Num_Quad_Points, integral, curr_level+1, total_level);
TotalRes(mypatch->upperright, Num_Quad_Points, integral, curr_level+1, total_level);
}

if(curr_level == total_level)
{
PetscScalar patchintegral;

PatchResInterior(mypatch,  Num_Quad_Points, &patchintegral);

//printf("Adding to full integral: %f\n ", patchintegral);

*integral += patchintegral;

}


PetscFunctionReturn(0);
}
